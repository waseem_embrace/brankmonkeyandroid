package com.embrace.it.beskrivapp.commons;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

import com.embrace.it.beskrivapp.MainApplication;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by waseem on 2/17/17.
 */

public class Utils {

    private static DisplayMetrics matrics = null;
    public static int getScreenWidth(){
        if(matrics == null) {
            Context context = MainApplication.getContext();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            matrics = new DisplayMetrics();
            Display display = wm.getDefaultDisplay();
            display.getRealMetrics(matrics);
        }
        return  matrics.widthPixels;
    }
    public static int getScreenHeight(){
        if(matrics == null) {
            Context context = MainApplication.getContext();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            matrics = new DisplayMetrics();
            Display display = wm.getDefaultDisplay();
            display.getRealMetrics(matrics);
        }
        return  matrics.heightPixels;
    }

    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }


    public static final int IO_BUFFER_SIZE = 8 * 1024;

    private Utils() {};

    public static boolean isExternalStorageRemovable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            return Environment.isExternalStorageRemovable();
        }
        return true;
    }

    public static File getExternalCacheDir(Context context) {
        if (hasExternalCacheDir()) {
            return context.getExternalCacheDir();
        }

        // Before Froyo we need to construct the external cache dir ourselves
        final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    public static boolean hasExternalCacheDir() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }


    static public Constants.ToolType getToolType(int value){
        for (Constants.ToolType t : Constants.ToolType.values()) {
            if(t.getNumericValue() == value)
                return t;
        }
        return Constants.ToolType.None;
    }


    /** Check if this device has a camera */
    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    static public float convertSpToPx(Context context,float scaledPixels) {
        return scaledPixels * context.getResources().getDisplayMetrics().scaledDensity;
    }

    static public int convertDpToPx(Context context,float dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / (float)DisplayMetrics.DENSITY_DEFAULT));
    }

    static public int convertDpToPx(float dp) {
        Context context = MainApplication.getContext();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / (float)DisplayMetrics.DENSITY_DEFAULT));
    }

    static public int convertPxToDp(Context context,float px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / (float)DisplayMetrics.DENSITY_DEFAULT));
    }
    static public int convertPxToDp(float px) {
        Context context = MainApplication.getContext();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / (float)DisplayMetrics.DENSITY_DEFAULT));
    }

    public static Typeface getFontTypeface(Context context, String fontName){
        return  Typeface.createFromAsset(context.getAssets(), "fonts/"+fontName);
    }

    public static String generateName(){
     return ""+System.currentTimeMillis();
    }

    public static String coordinateString(double latitude, double longitude) {
        long latSeconds = (long)(latitude * 3600);
        long latDegrees = latSeconds / 3600;
         latSeconds = Math.abs(latSeconds % 3600);
        long latMinutes = latSeconds / 60;
        latSeconds %= 60;
        long longSeconds = (long)(longitude * 3600);
        long longDegrees = longSeconds / 3600;
        longSeconds = Math.abs(longSeconds % 3600);
        long longMinutes = longSeconds / 60;
        longSeconds %= 60;
        return
                Math.abs(latDegrees)+"°"+
                latMinutes+"\'"+
                latSeconds+"\""+
                        (latDegrees >= 0 ? "N" : "S") +
                Math.abs(longDegrees)+"°"+
                longMinutes+"\'"+
                longSeconds+"\""+
                        (longDegrees >= 0 ? "E" : "W");
    }

    public static String getCurrentTimeString(){
        DateFormat date = new SimpleDateFormat("HH:mm:ss");
        Date currentLocalTime = new Date();

//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
//        Date currentLocalTime = cal.getTime();
//// you can get seconds by adding  "...:ss" to it
//        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));

        String localTime = date.format(currentLocalTime);
        return localTime;
    }

    public static String getCurrentDateString(){

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        String yearStr ="" + Math.abs(year % 100);


       String str = String.format("%02d",c.get(Calendar.DAY_OF_MONTH))+" / "+String.format("%02d",(c.get(Calendar.MONTH)))+" - "+ yearStr;
        return str;
    }
}