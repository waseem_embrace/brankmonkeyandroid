package com.embrace.it.beskrivapp.sticker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.text.Layout;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants.ToolType;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.sticker.toolframs.AngleSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.BracketSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.CountSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.DrawAreaSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.DrawSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.EditTextSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.IconSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.MarkingsSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.ShapeSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.SignSticker;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolDataFactory;

import java.util.List;

/**
 * Created by waseem on 2/17/17.
 */

public class StickerFactory {

    public static Sticker getSticker(Context context, ToolType toolType,Object arg) {

        Drawable drawable = null;
        ToolData data;
        switch (toolType) {
            case Crop_Tool:
                Sticker stickerCrop =  new MarkingsSticker(drawable);
                stickerCrop.setData(ToolDataFactory.getToolData(toolType));
                stickerCrop.setToolType(ToolType.Markings_Tool);
                return stickerCrop;

            case Brightness_Tool:

                Sticker brighnesSticker =  new MarkingsSticker(drawable);
                brighnesSticker.setData(ToolDataFactory.getToolData(toolType));
                brighnesSticker.setToolType(ToolType.Markings_Tool);
                return brighnesSticker;

            case Markings_Tool: //1,0

                drawable = ImageHelper.getdrawable(R.drawable.markoer_01);
                MarkingsSticker markingSticker = new MarkingsSticker(drawable);
                markingSticker.setData(ToolDataFactory.getToolData(toolType));
                markingSticker.setToolType(ToolType.Markings_Tool);
                return markingSticker;


            case Icon_Tool://1,1
                data = ToolDataFactory.getToolData(toolType);
                drawable = ImageHelper.getdrawable(data.getEditTools().get(0).getToolItemData().get(0));
                IconSticker iconSticker = new IconSticker(drawable);
                iconSticker.setData(data);
                iconSticker.setToolType(ToolType.Icon_Tool);
                return iconSticker;
            case Sign_Tool://1,2

                 data = ToolDataFactory.getToolData(toolType);
                drawable = ImageHelper.getdrawable(data.getEditTools().get(0).getToolItemData().get(0));
                SignSticker signSticker = new SignSticker(drawable);
                signSticker.setData(data);
                signSticker.setToolType(ToolType.Sign_Tool);
                return signSticker;
            case Draw_Tool://2,0
                List<PointF> points20 = (List<PointF>) arg;
                DrawSticker drawSticker = new DrawSticker(points20, ToolDataFactory.getToolData(toolType));
                drawSticker.setToolType(toolType);
                return drawSticker;

            case Draw_Area://2,1

                List<PointF> points21 = (List<PointF>) arg;
                DrawAreaSticker drawareaSticker = new DrawAreaSticker(points21, ToolDataFactory.getToolData(toolType));
                drawareaSticker.setToolType(toolType);
                return drawareaSticker;

            case Shape_Tool://2,2

                data = ToolDataFactory.getToolData(toolType);
                drawable = ImageHelper.getdrawable(data.getEditTools().get(0).getToolItemData().get(0));
                ShapeSticker shapeSticker = new ShapeSticker(drawable);
                shapeSticker.setData(data);
                shapeSticker.setToolType(toolType);
                return shapeSticker;

            case Brackets_Tool://3,0
                List<PointF> points = (List<PointF>) arg;
                BracketSticker bracketSticker = new BracketSticker(points, ToolDataFactory.getToolData(toolType));
                bracketSticker.setToolType(toolType);
                return bracketSticker;

            case Count_Tool://3,1

                List<PointF> points31 = (List<PointF>) arg;
                CountSticker countSticker = new CountSticker(points31, ToolDataFactory.getToolData(toolType));
                countSticker.setToolType(toolType);
                return countSticker;
            case Angle_Tool://3,2
                List<PointF> points32 = (List<PointF>) arg;
                AngleSticker angleSticker = new AngleSticker(points32, ToolDataFactory.getToolData(toolType));
                angleSticker.setToolType(toolType);
                return angleSticker;

            case Text_Area_Tool://4,0
                EditTextSticker sticker = new EditTextSticker(context,"", ToolDataFactory.getToolData(toolType));
                sticker.setText("");
                sticker.setTextColor(Color.WHITE);
                sticker.setTextAlign(Layout.Alignment.ALIGN_NORMAL);
//                sticker.resizeText();
                return sticker;

            case Symbol_Tool://4,1

                drawable = ImageHelper.getdrawable(R.drawable.markoer_01);
                MarkingsSticker markingSticker1 = new MarkingsSticker(drawable);
                markingSticker1.setData(ToolDataFactory.getToolData(toolType));
                markingSticker1.setToolType(toolType);
                return markingSticker1;



            default:
               return null;
        }

    }
}
