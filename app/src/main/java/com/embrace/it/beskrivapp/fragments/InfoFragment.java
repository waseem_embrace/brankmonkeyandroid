package com.embrace.it.beskrivapp.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;


public class InfoFragment extends Fragment implements SimpleTouchListener.SimpleTouchListenerCallback{

    public interface InfoFragmentCallback {
        public void goBack();
    }

    public void setDelegate(InfoFragmentCallback delegate) {
        this.delegate = delegate;
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    ImageButton btnBack;
    InfoFragmentCallback delegate;
    TextView tvOrganization;
    TextView tvSupportedBy;
    TextView tvDesignedBy;
    SimpleTouchListener touchListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_info,container,false);
        btnBack = (ImageButton)view.findViewById(R.id.btn_back_info);
        tvOrganization = (TextView)view.findViewById(R.id.tv_info_organization);
        tvDesignedBy = (TextView)view.findViewById(R.id.tv_info_designedby);
        tvSupportedBy = (TextView)view.findViewById(R.id.tv_info_supportedby);
        touchListener = new SimpleTouchListener(container.getContext(),this);
        touchListener.setSwipeSensitivity(1);
        setFontsAndSizes(container.getContext());
        btnBack.setOnTouchListener(touchListener);
        view.setOnTouchListener(touchListener);
        view.setEnabled(true);
        return view;
    }

    void setFontsAndSizes(Context context){
       Typeface blackCn = Utils.getFontTypeface(context,Constants.FONT_FrutigerLTStd_BlackCn);
        Typeface roman = Utils.getFontTypeface(context,Constants.FONT_FrutigerLTStd_Roman);

        tvOrganization.setTypeface(blackCn);
        tvSupportedBy.setTypeface(roman);
        tvDesignedBy.setTypeface(roman);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }





//-------SimpleTouchListener Callbacks----------//
    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {

    }

    @Override
    public void click(View v,float x,float y) {
        if (v.equals(btnBack)){
            if (delegate != null){
                delegate.goBack();
            }
        }
    }

    @Override
    public void swipe(View v,int direction){
        switch(direction){
            case SimpleTouchListener.SWIPE_LEFT:
                break;
        }
    }
}
