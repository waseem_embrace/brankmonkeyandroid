package com.embrace.it.beskrivapp.database;

import io.realm.RealmObject;

/**
 * Created by waseem on 3/2/17.
 */

public class RealmString extends RealmObject {
    private String val;
    public RealmString(){
    }

    public RealmString(String value) {
        this.val = value;
    }

    public String getValue() {
        return val;
    }

    public void setValue(String value) {
        this.val = value;
    }


    @Override
    public String toString() {
        return val;
    }
}