package com.embrace.it.beskrivapp.components.tools;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.components.BaseButton;

/**
 * Created by wasimshigri on 1/25/17.
 */

public class ToolMenuSubButton extends BaseButton {

    public int itemLocation = 1;
    public int mainIndex;
    public int childIndex;

    public static ToolMenuSubButton getNewInstance(Context context) {
        ToolMenuSubButton item = new ToolMenuSubButton(context);
        item.setBackgroundColor(Color.parseColor("#00000000"));
        item.setImageResource(R.drawable.icon);
        item.setButtonType(Constants.ToolType.None);
        return item;
    }

    public ToolMenuSubButton(Context context) {
        super(context);
        setBackgroundColor(Color.parseColor("#00ffffff"));
        mainIndex = 0;
        childIndex = 0;
    }

    public ToolMenuSubButton(Context context, int mainIndex, int childIndex) {
        super(context);
        setBackgroundColor(Color.parseColor("#00ffffff"));
        this.mainIndex = mainIndex;
        this.childIndex = childIndex;
    }

    public ToolMenuSubButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolMenuSubButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
