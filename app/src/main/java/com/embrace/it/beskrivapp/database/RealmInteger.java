package com.embrace.it.beskrivapp.database;

import io.realm.RealmObject;

/**
 * Created by waseem on 3/2/17.
 */

public class RealmInteger extends RealmObject {
    private Integer val;
    public RealmInteger(){
    }

    public RealmInteger(Integer value) {
        this.val = value;
    }

    public Integer getValue() {
        return val;
    }

    public void setValue(Integer value) {
        this.val = value;
    }


    @Override
    public String toString() {
        return ""+val;
    }


    public int toInteger() {
        return val;
    }
}