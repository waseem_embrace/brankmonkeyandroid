package com.embrace.it.beskrivapp.sticker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.Constants.ToolType;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waseem on 2/21/17.
 */


public class DrawingView extends View implements SimpleTouchListener.SimpleTouchListenerCallback {

    public int width;
    public int height;

    Context context;
    private Paint circlePaint;
    private Paint circlePaintFilled;
    private Paint textPaint;
    private Paint linePaint;
    private Paint linePaintFill;
    ToolType tooltype;

    public ToolType getTooltype() {
        return tooltype;
    }

    public void setTooltype(ToolType tooltype) {
        this.tooltype = tooltype;
        switch (this.tooltype) {
            case Angle_Tool:
                drawingPoints = new ArrayList<PointF>();

                    float x = Utils.getScreenWidth()/5;
                    float y = (Utils.getScreenHeight()/5);
                    float width = (Utils.getScreenWidth()/5) * 3;
                   float height = (y * 1.5f);


                drawingPoints.add(new PointF(x, height));
                drawingPoints.add(new PointF(x, height*2));
                drawingPoints.add(new PointF(width, height*2));
//                drawingPoints.add(new PointF(100, 200));
//                drawingPoints.add(new PointF(100, 400));
//                drawingPoints.add(new PointF(300, 400));
                invalidate();
                break;
            case Count_Tool:
                textPaint.setTextSize(Utils.convertDpToPx(25));
                break;
        }
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public DrawingView(Context c) {
        super(c);
        init(c);
    }

    private void init(Context c) {
        context = c;
//        mPath = new Path();
//        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(Color.WHITE);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeJoin(Paint.Join.BEVEL);
        circlePaint.setStrokeWidth(Utils.convertDpToPx(2f));
        DashPathEffect dashPath = new DashPathEffect(new float[]{Utils.convertDpToPx(5), Utils.convertDpToPx(5)}, 1);
        circlePaint.setPathEffect(dashPath);

        circlePaintFilled = new Paint();
        circlePaintFilled.setAntiAlias(true);
        circlePaintFilled.setColor(Color.WHITE);
        circlePaintFilled.setStyle(Paint.Style.FILL);
        circlePaintFilled.setStrokeJoin(Paint.Join.BEVEL);
        circlePaintFilled.setStrokeWidth(Utils.convertDpToPx(2f));


        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setColor(Color.WHITE);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(Utils.convertDpToPx(4f));

        linePaintFill = new Paint();
        linePaintFill.setAntiAlias(true);
        linePaintFill.setColor(Color.WHITE);
        linePaintFill.setAlpha(200);
        linePaintFill.setStyle(Paint.Style.FILL);
        linePaintFill.setStrokeWidth(Utils.convertDpToPx(4f));

        textPaint = new Paint();
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(Utils.convertDpToPx(20));

        simpleTouch = new SimpleTouchListener(context, this);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
//        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
//        mCanvas = new Canvas(mBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCanvas(canvas);
//        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

//        canvas.drawPath(mPath, mPaint);

    }

    void drawCanvas(Canvas canvas) {
        PointF prevPoint = null;
        switch (tooltype) {

            case Draw_Tool://2,0
                for (int i = 0; i < drawingPoints.size(); i++) {
                    PointF p = drawingPoints.get(i);
//                    canvas.drawPath(circlePath, circlePaint);
                    canvas.drawCircle(p.x, p.y, circleRadius, circlePaint);
                    if (prevPoint != null) {
                        canvas.drawLine(prevPoint.x, prevPoint.y, p.x, p.y, linePaint);
                    }
                    prevPoint = p;

                    if (i == 0)
                        canvas.drawCircle(p.x, p.y, Utils.convertDpToPx(5), circlePaintFilled);
                }
                break;
            case Draw_Area://2,0
                Path path = new Path();
                for (int i = 0; i < drawingPoints.size(); i++) {
                    PointF p = drawingPoints.get(i);
//                    canvas.drawPath(circlePath, circlePaint);
                    canvas.drawCircle(p.x, p.y, circleRadius, circlePaint);
                    if (prevPoint != null) {
                        canvas.drawLine(prevPoint.x, prevPoint.y, p.x, p.y, linePaint);
                    }
                    prevPoint = p;

                    if (i == 0) {
                        canvas.drawCircle(p.x, p.y, circleRadius - Utils.convertDpToPx(5), circlePaint);
                        path.moveTo(p.x, p.y);
                    } else {
                        path.lineTo(p.x, p.y);
                    }
                }
                canvas.drawPath(path, linePaintFill);

                break;
            case Brackets_Tool: //3,0

                if (drawingPoints == null || drawingPoints.size() == 0) {
                } else if (drawingPoints.size() == 1) {
                    PointF p = drawingPoints.get(0);
                    canvas.drawCircle(p.x, p.y, circleRadius, circlePaint);
                } else {
                    PointF p1 = drawingPoints.get(0);
                    PointF p2 = drawingPoints.get(1);
                    canvas.drawCircle(p1.x, p1.y, circleRadius, circlePaint);
                    canvas.drawCircle(p2.x, p2.y, circleRadius, circlePaint);
                    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
                }

                break;

            case Count_Tool://3,1
                for (int i = 0; i < drawingPoints.size(); i++) {
                    PointF p = drawingPoints.get(i);
                    Rect bounds = new Rect();
                    String text = "" + (i + 1) + "";
                    textPaint.getTextBounds(text, 0, text.length(), bounds);
                    int height = bounds.height();
                    int width = bounds.width();
                    canvas.drawText(text, p.x - (width/2), p.y + (height/2), textPaint);
                    canvas.drawCircle(p.x, p.y, circleRadius, circlePaint);
                }
                break;

            case Angle_Tool://3,2

                for (int i = 0; i < drawingPoints.size(); i++) {
                    PointF p = drawingPoints.get(i);
//                    canvas.drawPath(circlePath, circlePaint);
                    canvas.drawCircle(p.x, p.y, circleRadius, circlePaint);
                    if (prevPoint != null) {
                        canvas.drawLine(prevPoint.x, prevPoint.y, p.x, p.y, linePaint);
                    }
                    prevPoint = p;
                }

                PointF p1 = drawingPoints.get(0);
                PointF p2 = drawingPoints.get(2);
                PointF centerP = drawingPoints.get(1);
                final RectF oval = new RectF();
                oval.set(centerP.x - circleRadius, centerP.y - circleRadius, centerP.x + circleRadius, centerP.y + circleRadius);
                int startAngle = (int) (180 / Math.PI * Math.atan2(p1.y - centerP.y, p1.x - centerP.x));
                double sweepAngle = StickerUtils.angleBetween(centerP, p1, p2);
                if (sweepAngle > 180) {
                    sweepAngle = -(360 - sweepAngle);
                } else if (sweepAngle < -180) {
                    sweepAngle = -(-360 - sweepAngle);
                }
                MyLog.e("", "sweepAngle:" + sweepAngle);
                Path myPath = new Path();
                myPath.arcTo(oval, startAngle, -(float) sweepAngle, true);
                canvas.drawArc(oval, startAngle, (int) sweepAngle, true, linePaint);
                PointF textPoint = new PointF((p1.x + p2.x + centerP.x) / 3, (p1.y + p2.y + centerP.y) / 3);
                canvas.drawText("" + Math.abs((int) sweepAngle) + "°", textPoint.x, textPoint.y, textPaint);
                break;
        }

    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
//        mPath.reset();
//        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
//            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;

//            circlePath.reset();
//            circlePath.addCircle(mX, mY, circleRadius, Path.Direction.CW);
        }
    }

    private void touch_up() {
//        mPath.lineTo(mX, mY);
//        circlePath.reset();
        // commit the path to our offscreen
//        mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw
//        mPath.reset();
    }

    SimpleTouchListener simpleTouch;
float xDown, yDown;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
         xDown = event.getX();
         yDown = event.getY();
        simpleTouch.onTouch(this, event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
//                touch_start(x, y);
                handleActionDown(xDown, yDown);
                break;
            case MotionEvent.ACTION_MOVE:
//                touch_move(x, y);
//                invalidate();
                handleActionMove(xDown, yDown);
                break;
            case MotionEvent.ACTION_UP:
//                touch_up();
                handleActionUp(xDown, yDown);
                break;
        }
        return true;
    }


    void handleActionDown(float x, float y) {
        switch (tooltype) {

            case Draw_Tool: //2,0
            case Draw_Area: //2,1
            case Brackets_Tool: //3,0
            case Count_Tool://3,1
            case Angle_Tool://3,2
                checkCircleTouch(x, y);
                break;
        }
    }

    void handleActionMove(float x, float y) {
        switch (tooltype) {

            case Draw_Tool: //2,0
            case Draw_Area: //2,1
            case Brackets_Tool: //3,0
            case Count_Tool://3,1
            case Angle_Tool://3,2
                moveCircle(x, y);
                break;
        }
    }

    void handleActionUp(float x, float y) {
        switch (tooltype) {
            case Brackets_Tool: //3,0
                break;
            case Count_Tool://3,1
                break;
            case Angle_Tool://3,2
                break;
        }
        invalidate();
    }

    List<PointF> drawingPoints = new ArrayList<PointF>();

    float circleRadius = Utils.convertDpToPx(25f);
    PointF touchedCircle;

    void addCircle(float x, float y, ToolType tooltype) {
        PointF pointNew = new PointF(x, y);
//        checkCircleTouch(x, y);
        if(tooltype != ToolType.Angle_Tool) {
            if (touchedCircle == null) {
                if (tooltype == ToolType.Brackets_Tool && drawingPoints.size() < 2) {
                    drawingPoints.add(pointNew);
                    invalidate();
                } else if (drawingPoints.size() < 26) {
                    drawingPoints.add(pointNew);
                    invalidate();
                }
            } else {

            }
            if(delegate != null) {
                delegate.drawingPointsUpdated(this,drawingPoints);
            }
        }
    }

    void checkCircleTouch(float x, float y) {
        PointF pointNew = new PointF(x, y);
        touchedCircle = null;

        for (int i = 0; i < drawingPoints.size(); i++) {
            PointF p = drawingPoints.get(i);
            if (StickerUtils.isInsideCircle(pointNew, p, circleRadius)) {
                touchedCircle = p;
                break;
            }
        }
    }

    void moveCircle(float x, float y) {
        if (touchedCircle != null) {
            touchedCircle.x = x;
            touchedCircle.y = y;
            invalidate();
        }
    }


    private DrawingViewCallback delegate;

    public interface DrawingViewCallback {
        void doneDrawing(View v);
        void drawingPointsUpdated(View v,List<PointF> drawingPoints);
    }

    public void setDelegate(DrawingViewCallback callback) {
        this.delegate = callback;
    }


    public List<PointF> getDrawingPoints() {
        return this.drawingPoints;
    }

    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {

    }

    @Override
    public void click(View v,float x,float y) {

        if (touchedCircle == null) {
            addCircle(xDown, yDown, tooltype);
        }else{

            if (tooltype == ToolType.Draw_Area && drawingPoints.indexOf(touchedCircle) == 0) {
                if (this.delegate != null) {
                    this.delegate.doneDrawing(this);
                }
            }
        }
    }

    @Override
    public void swipe(View v, int direction) {

    }
}