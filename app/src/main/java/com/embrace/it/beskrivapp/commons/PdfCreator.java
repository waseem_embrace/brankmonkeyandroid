package com.embrace.it.beskrivapp.commons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.database.DBController;
import com.embrace.it.beskrivapp.database.Project;
import com.embrace.it.beskrivapp.database.ProjectMainItem;
import com.embrace.it.beskrivapp.database.SubProject;
import com.embrace.it.beskrivapp.fragments.ProjectsFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by waseem on 4/5/17.
 */

public class PdfCreator {

    Project project;
    private static String DIRECTORY_PDF = "saved_pdf";
    public String createPdf(String projectId) {
    Context context = MainApplication.getContext();

        PdfDocument document = new PdfDocument();

        float width = 595.2f;
        float height = 841.8f;
        float margin = 40f;
        float leftSectionWidth = (width / 5) * 3;
        float rightSectionWidth = (width / 5) * 2;
        float fontSize = 6;
        String fontLight = Constants.FONT_FrutigerLTStd_Light;
        String fontBlack = Constants.FONT_FrutigerLTStd_Black;
        Typeface fontFrutigerLight = Utils.getFontTypeface(context,Constants.FONT_FrutigerLTStd_Light);
        project = DBController.getProject(projectId);


        boolean gps = false;
        boolean date = false;
        boolean time = false;
        String place = null;
        String who = null;
        String what = null;
        String other = null;
        if (project.getAlbum() != null) {
            gps = project.getAlbum().getGps();
            date = project.getAlbum().getDate();
            time = project.getAlbum().getTime();
            place = project.getAlbum().getWhere();
            who = project.getAlbum().getWho();
            what = project.getAlbum().getAlbumTitle();
            other = project.getAlbum().getOther();
        }

        stringsForPdf pdfStrings = new stringsForPdf();
        List<View> views = new ArrayList<View>();


        ViewGroup firstView = new RelativeLayout(context);
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        rlp.height = (int) height;
        rlp.width = (int) width;
        firstView.setLayoutParams(rlp);
        firstView.measure((int)width, (int)height);
        firstView.layout(0, 0, (int)width, (int)height);

        List<SubProject> subProejcts =  project.getSubProjects();

        TextView info = new TextView(context);
        RectF frameInfo = new RectF(leftSectionWidth + margin, margin, (leftSectionWidth + margin) + rightSectionWidth - (margin * 2), (margin) + 50);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)frameInfo.width(),(int)frameInfo.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        info.setLayoutParams(params);
        Rect rectFrame = new Rect();
        frameInfo.roundOut(rectFrame);
        info.measure((int)frameInfo.width(), (int)frameInfo.height());
        info.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        firstView.addView(info);
        info.setText(pdfStrings.infoString);
        info.setTypeface(Utils.getFontTypeface(context, fontBlack));
        info.setTextSize(fontSize * 1.5f);
        info.setTextColor(Color.parseColor("#000000"));
        info.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);


        TextView beskrivLabel = new TextView(context);
        RectF frameBeskriv = new RectF(frameInfo.left + (frameInfo.width()/2)+5, margin , frameInfo.left + (frameInfo.width()/2)+5 + frameInfo.width() ,frameInfo.bottom);
        params = new RelativeLayout.LayoutParams((int)frameBeskriv.width(),(int)frameBeskriv.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        beskrivLabel.setLayoutParams(params);
        frameBeskriv.roundOut(rectFrame);
        beskrivLabel.measure((int)rectFrame.width(), (int)rectFrame.height());
        beskrivLabel.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        beskrivLabel.setText(pdfStrings.wordMark);
        beskrivLabel.setTypeface(Utils.getFontTypeface(context, fontLight),Typeface.BOLD);
        beskrivLabel.setTextSize((fontSize * 1.5f));
        beskrivLabel.setTextColor(Color.parseColor("#000000"));
        beskrivLabel.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        firstView.addView(beskrivLabel);


        ImageView iView = new ImageView(context);
        RectF frameIv = new RectF(frameInfo.left, margin + frameBeskriv.bottom, frameInfo.right, margin + frameBeskriv.bottom + (height / 6) * 2);
//        Bitmap b = ImageHelper.readImage(subProejcts.get(0).getThumbnailPath(), false);
        Bitmap b = ImageHelper.getBitmapForPdf(subProejcts.get(0).getThumbnailPath());
        if (b != null) {
            iView.setImageBitmap(b);
        }
        iView.setScaleType(ImageView.ScaleType.FIT_XY);
        frameIv.roundOut(rectFrame);
        iView.measure((int)rectFrame.width(), (int)rectFrame.height());
        iView.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        firstView.addView(iView);

        ImageView logo = new ImageView(context);
        RectF frameLogo = new RectF(frameInfo.left, height - ((height / 6) + 20), frameInfo.right, height - ((height / 6) + 20) + (height / 6));
        params = new RelativeLayout.LayoutParams((int)frameLogo.width(),(int)frameLogo.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        logo.setLayoutParams(params);
        frameLogo.roundOut(rectFrame);
        logo.measure((int)rectFrame.width(), (int)rectFrame.height());
        logo.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        logo.setImageDrawable(ImageHelper.getdrawable(R.drawable.snm_logo_side1_3));
        logo.setScaleType(ImageView.ScaleType.FIT_XY);
        firstView.addView(logo);

        views.add(firstView);

        float spacing = 15;

        TextView whoLabel = new TextView(context);
        RectF frameWho = new RectF((margin * 1.5f), frameIv.top, (margin * 1.5f) + leftSectionWidth - margin, frameIv.top + 20);
        params = new RelativeLayout.LayoutParams((int)frameWho.width(),(int)frameWho.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        whoLabel.setLayoutParams(params);
        frameWho.roundOut(rectFrame);
        whoLabel.measure((int)rectFrame.width(), (int)rectFrame.height());
        whoLabel.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        whoLabel.setText(pdfStrings.whoLabel);
        whoLabel.setTypeface(Utils.getFontTypeface(context, fontBlack),Typeface.BOLD);
        whoLabel.setTextSize(fontSize);
        whoLabel.setTextColor(Color.parseColor("#000000"));
        firstView.addView(whoLabel);

        TextView whoValue = new TextView(context);
        RectF frameWhoValue = new RectF(frameWho.left, frameWho.bottom, frameWho.right, frameWho.bottom + frameWho.height());
        params = new RelativeLayout.LayoutParams((int)frameWhoValue.width(),(int)frameWhoValue.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        whoValue.setLayoutParams(params);
        frameWhoValue.roundOut(rectFrame);
        whoValue.measure((int)rectFrame.width(), (int)rectFrame.height());
        whoValue.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        whoValue.setText(who);
        whoValue.setTypeface(Utils.getFontTypeface(context, fontLight));
        whoValue.setTextSize(fontSize);
        whoValue.setTextColor(Color.parseColor("#000000"));
        firstView.addView(whoValue);

        TextView whatLabel = new TextView(context);
        RectF frameWhat = new RectF(margin, frameWhoValue.top + frameWhoValue.height() + spacing, frameWho.width() + margin, frameWhoValue.top + frameWhoValue.height() + spacing + 40);
        params = new RelativeLayout.LayoutParams((int)frameWhat.width(),(int)frameWhat.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        whatLabel.setLayoutParams(params);
        frameWhat.roundOut(rectFrame);
        whatLabel.measure((int)rectFrame.width(), (int)rectFrame.height());
        whatLabel.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        whatLabel.setText(pdfStrings.whatLabel);
        whatLabel.setTypeface(Utils.getFontTypeface(context, fontBlack));
        whatLabel.setTextSize(fontSize * 3);
        whatLabel.setTextColor(Color.parseColor("#000000"));
        firstView.addView(whatLabel);

        TextView whatValue = new TextView(context);
        RectF frameWhatValue = new RectF(frameWhat.left, frameWhat.bottom, frameWhat.right, frameWhat.bottom + frameWhat.height());
        params = new RelativeLayout.LayoutParams((int)frameWhatValue.width(),(int)frameWhatValue.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        whatValue.setLayoutParams(params);
        frameWhatValue.roundOut(rectFrame);
        whatValue.measure((int)rectFrame.width(), (int)rectFrame.height());
        whatValue.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        whatValue.setText(what);
        whatValue.setTypeface(Utils.getFontTypeface(context, fontLight));
        whatValue.setTextSize(fontSize * 3);
        whatValue.setTextColor(Color.parseColor("#000000"));
        firstView.addView(whatValue);


        TextView whereLabel = new TextView(context);
        RectF frameWhere = new RectF(frameWhoValue.left, frameWhatValue.bottom + spacing, frameWhat.width() + frameWhoValue.left, frameWhatValue.bottom + spacing + frameWhoValue.height());
        params = new RelativeLayout.LayoutParams((int)frameWhere.width(),(int)frameWhere.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        whereLabel.setLayoutParams(params);
        frameWhere.roundOut(rectFrame);
        whereLabel.measure((int)rectFrame.width(), (int)rectFrame.height());
        whereLabel.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        whereLabel.setText(pdfStrings.whereLabel);
        whereLabel.setTypeface(Utils.getFontTypeface(context, fontBlack));
        whereLabel.setTextSize(fontSize);
        whereLabel.setTextColor(Color.parseColor("#000000"));
        firstView.addView(whereLabel);

        TextView whereValue = new TextView(context);
        RectF frameWhereValue = new RectF(frameWhere.left, frameWhere.bottom, frameWhere.right, frameWhere.bottom + frameWhere.height());
        params = new RelativeLayout.LayoutParams((int)frameWhereValue.width(),(int)frameWhereValue.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        whereValue.setLayoutParams(params);
        whereValue.setText(place);
        frameWhereValue.roundOut(rectFrame);
        whereValue.measure((int)rectFrame.width(), (int)rectFrame.height());
        whereValue.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        whereValue.setTypeface(Utils.getFontTypeface(context, fontLight));
        whereValue.setTextSize(fontSize);
        whereValue.setTextColor(Color.parseColor("#000000"));
        firstView.addView(whereValue);

        TextView otherLabel = new TextView(context);
        RectF frameOther = new RectF(frameWhereValue.left, frameWhereValue.bottom + spacing, frameWhere.width() + frameWhereValue.left, frameWhereValue.bottom + spacing + frameWhereValue.height());
        params = new RelativeLayout.LayoutParams((int)frameOther.width(),(int)frameOther.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        otherLabel.setLayoutParams(params);
        frameOther.roundOut(rectFrame);
        otherLabel.measure((int)rectFrame.width(), (int)rectFrame.height());
        otherLabel.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        otherLabel.setText(pdfStrings.otherLabel);
        otherLabel.setTypeface(Utils.getFontTypeface(context, fontBlack),Typeface.BOLD);
        otherLabel.setTextSize(fontSize);
        otherLabel.setTextColor(Color.parseColor("#000000"));
        firstView.addView(otherLabel);

        TextView otherValue = new TextView(context);
        RectF frameOtherValue = new RectF(frameOther.left, frameOther.bottom, frameOther.right, frameOther.bottom + frameOther.height());
        params = new RelativeLayout.LayoutParams((int)frameOtherValue.width(),(int)frameOtherValue.height());
        params.leftMargin = (int)frameInfo.left;
        params.topMargin = (int)frameInfo.top;
        otherValue.setLayoutParams(params);
        frameOtherValue.roundOut(rectFrame);
        otherValue.measure((int)rectFrame.width(), (int)rectFrame.height());
        otherValue.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
        otherValue.setText(other);
        otherValue.setTypeface(Utils.getFontTypeface(context, fontLight));
        otherValue.setTextSize(fontSize);
        otherValue.setTextColor(Color.parseColor("#000000"));
        firstView.addView(otherValue);

        int index = -1;
        for (SubProject sub : project.getSubProjects()) {
            Bitmap image = ImageHelper.getBitmapForPdf(sub.getThumbnailPath());
//            Bitmap image = ImageHelper.readImage(sub.getThumbnailPath(), false);
            index++;

            ViewGroup newView = new RelativeLayout(context);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            rlp.height = (int) height;
            rlp.width = (int) width;
            newView.setLayoutParams(param);
            newView.measure((int)width, (int)height);
            newView.layout(0, 0, (int)width, (int)height);

            TextView information = new TextView(context);
            RectF framInformation = new RectF(leftSectionWidth + margin, margin, (leftSectionWidth + margin) + rightSectionWidth - (margin * 2), (margin) + 50);
            params = new RelativeLayout.LayoutParams((int)framInformation.width(),(int)framInformation.height());
            params.leftMargin = (int)framInformation.left;
            params.topMargin = (int)framInformation.top;
            information.setLayoutParams(params);
            rectFrame = new Rect();
            framInformation.roundOut(rectFrame);
            information.measure((int)rectFrame.width(), (int)rectFrame.height());
            information.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
            information.setText(pdfStrings.infoString);
            information.setTypeface(Utils.getFontTypeface(context, fontBlack));
            information.setTextSize(fontSize * 1.5f);
            information.setTextColor(Color.parseColor("#000000"));
            information.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            newView.addView(information);

            TextView beskriv = new TextView(context);
            RectF frambeskriv = new RectF(framInformation.left + (framInformation.width()/2)+5, margin , framInformation.left + (framInformation.width()/2)+5 + framInformation.width() ,framInformation.bottom);
            params = new RelativeLayout.LayoutParams((int)frambeskriv.width(),(int)frambeskriv.height());
            params.leftMargin = (int)framInformation.left;
            params.topMargin = (int)framInformation.top;
            beskriv.setLayoutParams(params);
            frambeskriv.roundOut(rectFrame);
            beskriv.measure((int)rectFrame.width(), (int)rectFrame.height());
            beskriv.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
            beskriv.setText(pdfStrings.wordMark);
            beskriv.setTypeface(Utils.getFontTypeface(context, fontLight));
            beskriv.setTextSize((fontSize * 1.5f));
            beskriv.setTextColor(Color.parseColor("#000000"));
            beskriv.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            newView.addView(beskriv);

            TextView picture = new TextView(context);
            RectF framePicture = new RectF(margin, frambeskriv.bottom + margin, margin + leftSectionWidth - margin, frambeskriv.bottom + margin + 20);
            params = new RelativeLayout.LayoutParams((int)framePicture.width(),(int)framePicture.height());
            params.leftMargin = (int)frameInfo.left;
            params.topMargin = (int)frameInfo.top;
            picture.setLayoutParams(params);
            framePicture.roundOut(rectFrame);
            picture.measure((int)rectFrame.width(), (int)rectFrame.height());
            picture.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
            picture.setText(pdfStrings.imageLabel);
            picture.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            picture.setTypeface(Utils.getFontTypeface(context, fontBlack));
            picture.setTextSize(fontSize);
            picture.setTextColor(Color.parseColor("#000000"));
            newView.addView(picture);


            TextView title = new TextView(context);
            RectF frameTitle = new RectF(margin, framePicture.bottom, margin + framePicture.width(), framePicture.bottom + framePicture.height());
            params = new RelativeLayout.LayoutParams((int)frameTitle.width(),(int)frameTitle.height());
            params.leftMargin = (int)frameInfo.left;
            params.topMargin = (int)frameInfo.top;
            title.setLayoutParams(params);
            frameTitle.roundOut(rectFrame);
            title.measure((int)rectFrame.width(), (int)rectFrame.height());
            title.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
            title.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            title.setTypeface(Utils.getFontTypeface(context, fontLight));
            title.setTextSize(fontSize);
            title.setText(sub.getProjectTitle()+" - " + (index+1) + pdfStrings.imageTitle);
            title.setTextColor(Color.parseColor("#000000"));
            newView.addView(title);


            ImageView iview = new ImageView(context);
            RectF frameiview = new RectF(margin, frameTitle.bottom + margin, frameTitle.width() + margin, frameTitle.bottom + margin + (height / 5) * 3);
            params = new RelativeLayout.LayoutParams((int)frameiview.width(),(int)frameiview.height());
            params.leftMargin = (int)frameInfo.left;
            params.topMargin = (int)frameInfo.top;
            iview.setLayoutParams(params);
            frameiview.roundOut(rectFrame);
            iview.measure((int)rectFrame.width(), (int)rectFrame.height());
            iview.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
            iview.setImageBitmap(image);
            iview.setScaleType(ImageView.ScaleType.FIT_XY);
            newView.addView(iview);

            ImageView logoView = new ImageView(context);
            RectF framelogoView = new RectF(margin / 2, height - 130, width - margin + (margin / 2), 130 + (height - 130));
            params = new RelativeLayout.LayoutParams((int)framelogoView.width(),(int)framelogoView.height());
            params.leftMargin = (int)frameInfo.left;
            params.topMargin = (int)frameInfo.top;
            logoView.setLayoutParams(params);
            framelogoView.roundOut(rectFrame);
            logoView.measure((int)rectFrame.width(), (int)rectFrame.height());
            logoView.layout(rectFrame.left,rectFrame.top,rectFrame.right,rectFrame.bottom);
            logoView.setImageDrawable(ImageHelper.getdrawable(R.drawable.snm_logo_side2_2));
            logoView.setScaleType(ImageView.ScaleType.FIT_XY);
            newView.addView(logoView);

            float labelHeight = 20;
             place = "at Home";


            if (time) {
                try {
                    String convertedTime = sub.getTime().toString();
                    if (convertedTime != null && convertedTime != "") {
                        TextView timeLabel = new TextView(context);
                        RectF frameTime = new RectF(framInformation.left, frameiview.bottom - labelHeight, framInformation.right, frameiview.bottom);
                        params = new RelativeLayout.LayoutParams((int) frameTime.width(), (int) frameTime.height());
                        params.leftMargin = (int) frameTime.left;
                        params.topMargin = (int) frameTime.top;
                        timeLabel.setLayoutParams(params);
                        frameTime.roundOut(rectFrame);
                        timeLabel.measure((int) rectFrame.width(), (int) rectFrame.height());
                        timeLabel.layout(rectFrame.left, rectFrame.top, rectFrame.right, rectFrame.bottom);
                        timeLabel.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        timeLabel.setTypeface(Utils.getFontTypeface(context, fontLight));
                        timeLabel.setTextSize(fontSize);
                        timeLabel.setTextColor(Color.parseColor("#000000"));
                        timeLabel.setText(place + ", " + convertedTime);
                        newView.addView(timeLabel);

                        TextView timeTitle = new TextView(context);
                        RectF framTimeTitle = new RectF(framInformation.left, frameTime.top - labelHeight, frameTime.right, frameTime.top);
                        params = new RelativeLayout.LayoutParams((int) framTimeTitle.width(), (int) framTimeTitle.height());
                        params.leftMargin = (int) framTimeTitle.left;
                        params.topMargin = (int) framTimeTitle.top;
                        timeTitle.setLayoutParams(params);
                        framTimeTitle.roundOut(rectFrame);
                        timeTitle.measure((int) rectFrame.width(), (int) rectFrame.height());
                        timeTitle.layout(rectFrame.left, rectFrame.top, rectFrame.right, rectFrame.bottom);
                        timeTitle.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        timeTitle.setTypeface(Utils.getFontTypeface(context, fontLight));
                        timeTitle.setTextSize(fontSize);
                        timeTitle.setText(pdfStrings.timeAndPlace);
                        timeTitle.setTextColor(Color.parseColor("#000000"));
                        newView.addView(timeTitle);
                    }
                }catch(Exception e){

                }
            }

            if (date) {
                try {
                String convertedDate = sub.getDate().toString();
                if ( convertedDate != null && convertedDate != "") {
                    TextView dateLabel = new TextView(context);
                    RectF frameDate = new RectF(framInformation.left, frameiview.bottom - (labelHeight * 4), framInformation.right, frameiview.bottom - (labelHeight * 3));
                    params = new RelativeLayout.LayoutParams((int) frameDate.width(), (int) frameDate.height());
                    params.leftMargin = (int) frameDate.left;
                    params.topMargin = (int) frameDate.top;
                    dateLabel.setLayoutParams(params);
                    frameDate.roundOut(rectFrame);
                    dateLabel.measure((int) rectFrame.width(), (int) rectFrame.height());
                    dateLabel.layout(rectFrame.left, rectFrame.top, rectFrame.right, rectFrame.bottom);
                    dateLabel.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    dateLabel.setTypeface(Utils.getFontTypeface(context, fontLight));
                    dateLabel.setTextSize(fontSize);
                    dateLabel.setTextColor(Color.parseColor("#000000"));
                    dateLabel.setText(convertedDate);
                    newView.addView(dateLabel);

                    TextView dateTitle = new TextView(context);
                    RectF framDateTitle = new RectF(framInformation.left, frameDate.top - labelHeight, frameDate.right, frameDate.top);
                    params = new RelativeLayout.LayoutParams((int) framDateTitle.width(), (int) framDateTitle.height());
                    params.leftMargin = (int) framDateTitle.left;
                    params.topMargin = (int) framDateTitle.top;
                    dateTitle.setLayoutParams(params);
                    framDateTitle.roundOut(rectFrame);
                    dateTitle.measure((int) rectFrame.width(), (int) rectFrame.height());
                    dateTitle.layout(rectFrame.left, rectFrame.top, rectFrame.right, rectFrame.bottom);
                    dateTitle.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    dateTitle.setTypeface(Utils.getFontTypeface(context, fontLight));
                    dateTitle.setTextSize(fontSize);
                    dateTitle.setText(pdfStrings.date);
                    dateTitle.setTextColor(Color.parseColor("#000000"));
                    newView.addView(dateTitle);
                }
                }catch(Exception e){

                }
            }


            if (gps) {
                try {
                String coords = sub.getGpsCoordinate().toString();
                if ( coords != null && coords != "") {
                    TextView gpsLabel = new TextView(context);
                    RectF frameGps = new RectF(framInformation.left, frameiview.bottom - (labelHeight * 7), framInformation.right, frameiview.bottom - (labelHeight * 6));
                    params = new RelativeLayout.LayoutParams((int) frameGps.width(), (int) frameGps.height());
                    params.leftMargin = (int) frameGps.left;
                    params.topMargin = (int) frameGps.top;
                    gpsLabel.setLayoutParams(params);
                    frameGps.roundOut(rectFrame);
                    gpsLabel.measure((int) rectFrame.width(), (int) rectFrame.height());
                    gpsLabel.layout(rectFrame.left, rectFrame.top, rectFrame.right, rectFrame.bottom);
                    gpsLabel.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    gpsLabel.setTypeface(Utils.getFontTypeface(context, fontLight));
                    gpsLabel.setTextSize(fontSize);
                    gpsLabel.setTextColor(Color.parseColor("#000000"));
                    gpsLabel.setText(coords);
                    newView.addView(gpsLabel);

                    TextView gpsTitle = new TextView(context);
                    RectF framGpsTitle = new RectF(framInformation.left, frameGps.top - labelHeight, frameGps.right, frameGps.top);
                    params = new RelativeLayout.LayoutParams((int) framGpsTitle.width(), (int) framGpsTitle.height());
                    params.leftMargin = (int) framGpsTitle.left;
                    params.topMargin = (int) framGpsTitle.top;
                    gpsTitle.setLayoutParams(params);
                    framGpsTitle.roundOut(rectFrame);
                    gpsTitle.measure((int) rectFrame.width(), (int) rectFrame.height());
                    gpsTitle.layout(rectFrame.left, rectFrame.top, rectFrame.right, rectFrame.bottom);
                    gpsTitle.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    gpsTitle.setTypeface(Utils.getFontTypeface(context, fontLight));
                    gpsTitle.setTextSize(fontSize);
                    gpsTitle.setText(pdfStrings.gps);
                    gpsTitle.setTextColor(Color.parseColor("#000000"));
                    newView.addView(gpsTitle);
                }
                }catch(Exception e){

                }
            }

            views.add(newView);
         }

         for (View v: views) {
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder((int)width,(int)height,index + 2).create();
             PdfDocument.Page page = document.startPage(pageInfo);
             v.draw(page.getCanvas());
             document.finishPage(page);

         }

       String path= savePDFToExternalStorage(document,project.getProjectId());
        document.close();
        return path;
    }

    public static String savePDFToExternalStorage(PdfDocument document,String name){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/"+ DIRECTORY_PDF );
        myDir.mkdirs();
        if (name.contains(".pdf")) {
        } else
            name = name + ".pdf";
        String fname = ""+ name;
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            document.writeTo(out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file.toString();
    }

    void addPage(PdfDocument document, int pageNumber) {
     /*   PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(100, 100, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        // draw something on the page
        View content = getContentView();
        content.draw(page.getCanvas());
        // finish the page
        document.finishPage(page);*/
    }


    class stringsForPdf {

        public String infoString = "BESKRIV";
        public String wordMark = "VERDEN";

        public String whoLabel = "HVEM:";
        public String whatLabel = "HVAD:";
        public String whereLabel = "HVOR:";
        public String otherLabel = "ANDET:";
        public String imageLabel = "BILLEDE:";

        // Formatted like {count} YourText. Example : "3. picture"
        public String imageTitle = ". billede";

        public String timeAndPlace = "Tid/sted:";
        public String date = "Dato:";
        public String gps = "GPS koordinater:";
    }
}