package com.embrace.it.beskrivapp.database;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by waseem on 3/2/17.
 */

public class RealmHelper {

    static public Rect getFrame(RealmList<RealmInteger> frame) {
        Rect r = new Rect();
        try {
            r.left = frame.get(0).toInteger();
            r.top = frame.get(1).toInteger();
            r.right = frame.get(2).toInteger();
            r.bottom = frame.get(3).toInteger();
        } catch (Exception e) {
            r = null;
        }
        return r;
    }

    static public RectF getFrameF(RealmList<RealmFloat> frame) {
        if(frame == null)
            return null;
        RectF r = new RectF();
        try {
            r.left = frame.get(0).toFloat();
            r.top = frame.get(1).toFloat();
            r.right = frame.get(2).toFloat();
            r.bottom = frame.get(3).toFloat();
        } catch (Exception e) {
            r = null;
        }
        return r;
    }

    static public int[] getArray(RealmList<RealmInteger> list) {
        int count = list.size();
        int[] arr = new int[count];
        for (int i = 0; i < count; i++) {
            RealmInteger v = list.get(i);
            arr[i] = v.toInteger();
        }
        return arr;
    }

    static public float[] getArrayF(RealmList<RealmFloat> list) {
        int count = list.size();
        float[] arr = new float[count];
        for (int i = 0; i < count; i++) {
            RealmFloat v = list.get(i);
            arr[i] = v.toFloat();
        }
        return arr;
    }

    static public List<PointF> getListPointF(RealmList<RealmPointF> list) {
        if(list == null)
            return null;
        int count = list.size();
        List<PointF> l = new ArrayList<PointF>();
        for (int i = 0; i < count; i++) {
            RealmPointF v = list.get(i);
            l.add(v.toPointF());
        }
        return l;
    }

}
