package com.embrace.it.beskrivapp.components.tools;

/**
 * Created by wasimshigri on 1/30/17.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Utils;


/**
 * Created by wasimshigri on 1/25/17.
 */

public class SliderBar extends ViewGroup {

    public SliderBar(Context context, int controlType, int progress) {
        super(context);
        this.progress = progress;
        this.controlType = controlType;
        initView(context);
    }

    public SliderBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SliderBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SliderBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            Rect mTmpChildRect = new Rect(l, t, r, b);
            child.layout(mTmpChildRect.left, mTmpChildRect.top,
                    mTmpChildRect.right, mTmpChildRect.bottom);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
//            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
        }

        setMeasuredDimension(widthSize, heightSize);
    }

    SeekBar seekbar;
    float parentWidth = 0;
    float parentHeight = 50;
    int controlType = 0;
    int progress = 50;

    private void initView(Context context) {
        View view = inflate(context, R.layout.sliderbar, null);
        seekbar = (SeekBar) view.findViewById(R.id.slider_seek_bar);
        seekbar.setProgress(progress);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (delegate != null)
                    delegate.progressChanged(progress, controlType);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        parentWidth = Utils.getScreenWidth();
        parentHeight = 50;
//        view.setBackgroundColor(Color.GREEN);
        seekbar.setBackgroundColor(Color.GREEN);

        addView(view);
    }


    public interface SliderBarCallback {
        void progressChanged(int progress, int controlType);
    }

    SliderBarCallback delegate;

    public void setSliderCallback(SliderBarCallback callback) {
        this.delegate = callback;
    }
}