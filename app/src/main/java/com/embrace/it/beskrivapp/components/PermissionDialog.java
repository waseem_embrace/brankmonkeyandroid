package com.embrace.it.beskrivapp.components;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;

import com.embrace.it.beskrivapp.R;

import java.io.Serializable;

/**
 * Created by waseem on 2/2/17.
 */

public class PermissionDialog  extends Fragment implements FragmentCompat.OnRequestPermissionsResultCallback {

    interface PermissionCallback extends Serializable {
        void onPermission(String requestedPermission, boolean success, String message); // Params are self-defined and added to suit your needs.
    }

    private static final String FRAGMENT_DIALOG = "dialog";
    String REQUEST_PERMISSION;
    int REQUEST_PERMISSION_CODE;


    PermissionCallback callback;

    public PermissionDialog(){

    }

    public void requestPermission(Fragment contextFragment, String permissionRequested, int requestedCode, String message, final PermissionCallback callback) {
        REQUEST_PERMISSION = permissionRequested;
        REQUEST_PERMISSION_CODE = requestedCode;
        this.callback = callback;

        if (FragmentCompat.shouldShowRequestPermissionRationale(contextFragment, permissionRequested)) {
            ConfirmationDialog d =new ConfirmationDialog();
            Bundle b = new Bundle();
            b.putString("permission",REQUEST_PERMISSION);
            b.putInt("code",REQUEST_PERMISSION_CODE);
            b.putSerializable("callback",callback);
            b.putSerializable("","");
            d.setArguments(b);
            d.show(contextFragment.getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(contextFragment, new String[]{permissionRequested},
                    requestedCode);
        }
        callback.onPermission(permissionRequested,true, message);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }
        if ((grantResults.length > 0) && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            callback.onPermission(REQUEST_PERMISSION,false,"");
        } else if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//                CameraFragment.ErrorDialog.newInstance(getString(R.string.request_permission))
//                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
                callback.onPermission(REQUEST_PERMISSION,false,"");
            }
        }
    }

    public static class ConfirmationDialog extends DialogFragment {

        PermissionCallback callback;
        int REQUEST_PERMISSION_CODE;
        String REQUEST_PERMISSION;
         Fragment parent = null;
        public ConfirmationDialog(){

        }

        @Override
        public void setArguments(Bundle args) {
            super.setArguments(args);
            REQUEST_PERMISSION = args.getString("permission");
            callback = (PermissionCallback)args.get("callback");
            REQUEST_PERMISSION_CODE = args.getInt("code");
            parent = (Fragment)args.get("parent");
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = this.parent;
            return new AlertDialog.Builder(parent.getActivity())
                    .setMessage(R.string.request_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent,
                                    new String[]{REQUEST_PERMISSION},
                                    REQUEST_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    callback.onPermission(REQUEST_PERMISSION,false,"");

                                }
                            })
                    .create();
        }
    }
}
