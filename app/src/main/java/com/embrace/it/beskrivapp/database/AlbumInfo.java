package com.embrace.it.beskrivapp.database;

import io.realm.RealmObject;

/**
 * Created by waseem on 2/9/17.
 */

public class AlbumInfo extends RealmObject {

    public AlbumInfo(){}
    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Boolean getTime() {
        return time;
    }

    public void setTime(Boolean time) {
        this.time = time;
    }

    public Boolean getDate() {
        return date;
    }

    public void setDate(Boolean date) {
        this.date = date;
    }

    public Boolean getGps() {
        return gps;
    }

    public void setGps(Boolean gps) {
        this.gps = gps;
    }


    private String albumTitle;
    private String who;
    private String where;
    private String other;
    private Boolean time;
    private Boolean date;
    private Boolean gps;

}
