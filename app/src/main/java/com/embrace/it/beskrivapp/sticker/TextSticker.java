package com.embrace.it.beskrivapp.sticker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.DynamicLayout;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.util.TypedValue;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

/**
 * Customize your sticker with text and image background.
 * You can place some text into a given region, however,
 * you can also add a plain text sticker. To support text
 * auto resizing , I take most of the code from AutoResizeTextView.
 * See https://adilatwork.blogspot.com/2014/08/android-textview-which-resizes-its-text.html
 * Notice: It's not efficient to add long text due to too much of
 * StaticLayout object allocation.
 * <p>
 * Created by liutao on 30/11/2016.
 */

abstract public class TextSticker extends Sticker {

    /**
     * Our ellipsis string.
     */
    private static final String mEllipsis = "\u2026";

    /**
     * Upper bounds for text size.
     * This acts as a starting point for resizing.
     */
    private float mMaxTextSizePixels;

    /**
     * Lower bounds for text size.
     */
    private float mMinTextSizePixels;

    /**
     * Lower bounds for text size.
     */
    private float mTargetTextSizePixels;

    /**
     * Line spacing multiplier.
     */
    private float mLineSpacingMultiplier = 1.0f;

    /**
     * Additional line spacing.
     */
    private float mLineSpacingExtra = 0.0f;

    protected float scaleX = 1.0f;
    protected float scaleY = 1.0f;

    private Context mContext;
    private Drawable mDrawable;
    private Rect mRealBounds;
    protected Rect mTextRect;

    protected Matrix matrixText;

    private StaticLayout mStaticLayout;
    private DynamicLayout mDynamicLayout;
    private TextPaint mTextPaint;
    private Paint mCursorPaint;
    private Layout.Alignment mAlignment;
    protected String mText;

    Boolean showCursur = false;
    public TextSticker(Context context) {
        this(context, null);
    }

    public TextSticker(Context context, Drawable drawable) {
        mContext = context;
        mDrawable = drawable;
        mText = " ";
        if (drawable == null) {
            mDrawable = ContextCompat.getDrawable(context, R.drawable.sticker_transparent_background);
        }
        matrix = new Matrix();
        matrixText = new Matrix();
        mTextPaint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
        mCursorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        updateRects();
        mMinTextSizePixels = convertSpToPx(1);
        mMaxTextSizePixels = convertSpToPx(100);
        mAlignment = Layout.Alignment.ALIGN_NORMAL;
        mTextPaint.setTextSize(mMinTextSizePixels);
    }

    public void updateRects() {
//        setTextRect();
        scaleTextSize();
        mRealBounds = new Rect(0, 0, getWidth(), getHeight());
        setTextRect();

//        mTextRect = new Rect(0, 0, getWidth(), getHeight());

    }

    private void setTextRect() {
        float width = mRealBounds.width();
        float height = mRealBounds.height();
        float scalex = getCurrentScaleX();
        float scaley = getCurrentScaleY();
        height = height * getCurrentScaleY();
        width = width * getCurrentScaleX();
        mTextRect = new Rect(0, 0, (int) width, (int) height);
    }

    public abstract void scaleTextSize();

    public abstract void scaleMatrixText();

    /*
    *
    * matrix = [scaleX,skewX,transX,skewY,scaleY,transY,persp0,persp1,persp2]
    *  [scaleX,skewX,transX,
    *  skewY,scaleY,transY,
    *  persp0,persp1,persp2]
    *  */
    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.concat(matrix);
        if (mDrawable != null) {
            mDrawable.setBounds(mRealBounds);
            mDrawable.draw(canvas);
        }
        canvas.restore();

        canvas.save();
        canvas.concat(matrixText);
        canvas.translate(0, 0);
//        if (mTextRect.width() == getWidth()) {
//            int dy = getHeight() / 2 - mStaticLayout.getHeight() / 2;
//            // center vertical
//            if (dy >= 0)
//                canvas.translate(0, 0);
//        } else {
//            int dx = mTextRect.left;
//            int dy = mTextRect.top + mTextRect.height() / 2 - mStaticLayout.getHeight() / 2;
//            if (dy >= 0)
//            canvas.translate(dx, dy);
//            else
//                canvas.translate(0, 0);
//        }

    mStaticLayout.draw(canvas);
    if(showCursur) {
        int d = mStaticLayout.getLineCount();
        int dd = mStaticLayout.getLineEnd(d - 1);
        int ddd = mStaticLayout.getLineVisibleEnd(d - 1);
        int dddf = mStaticLayout.getLineStart(0);
        Log.v("","count:" + d + " lineEnd:"+ dd + " visible:"+ddd);
//        Rect r = new Rect();
//        canvas.drawRect(r,mCursorPaint);
    }
        canvas.restore();
    }


    void canvasDynamic(Canvas canvas) {
        canvas.save();
        canvas.concat(matrix);
        if (mDrawable != null) {
            mDrawable.setBounds(mRealBounds);
            mDrawable.draw(canvas);
        }
        canvas.restore();

        canvas.save();
        canvas.concat(matrix);
        if (mTextRect.width() == getWidth()) {
            int dy = getHeight() / 2 - mDynamicLayout.getHeight() / 2;
            // center vertical
            if (dy >= 0)
                canvas.translate(0, dy);
        } else {
            int dx = mTextRect.left;
            int dy = mTextRect.top + mTextRect.height() / 2 - mDynamicLayout.getHeight() / 2;
            canvas.translate(dx, dy);
        }
        mDynamicLayout.draw(canvas);
        canvas.restore();
    }

    @Override
    public int getWidth() {
        int width = mDrawable.getIntrinsicWidth();
        width = Utils.convertDpToPx(width);
        if (width > Utils.getScreenWidth()) {
            width = Utils.getScreenWidth() - Utils.getScreenWidth() / 3;
        }

        width = Utils.convertPxToDp(width);
        return width;
    }

    @Override
    public int getHeight() {
        return mDrawable.getIntrinsicHeight();
    }

    @Override
    public void release() {
        super.release();
        if (mDrawable != null) {
            mDrawable = null;
        }
    }

    @Override
    public Drawable getDrawable() {
        return mDrawable;
    }

    @Override
    public void itemUpdated(int mainIndex, int chidlIndex) {

    }

    @Override
    public void setProjectItem(ProjectItem p) {

    }

    @Override
    public ProjectItem getProjectItem() {
        return null;
    }

    @Override
    public void setDrawable(Drawable drawable) {
        mDrawable = drawable;
        mRealBounds.set(0, 0, getWidth(), getHeight());
        mTextRect.set(0, 0, getWidth(), getHeight());
    }

    public void setDrawable(Drawable drawable, Rect region) {
        mDrawable = drawable;
        mRealBounds.set(0, 0, getWidth(), getHeight());
        if (region == null) {
            mTextRect.set(0, 0, getWidth(), getHeight());
        } else {
            mTextRect.set(region.left, region.top, region.right, region.bottom);
        }
    }

    public void setTypeface(Typeface typeface) {
        mTextPaint.setTypeface(typeface);
    }

    public void setTextColor(int color) {
        mTextPaint.setColor(color);
    }


    public void setTextSize(int textsize) {
        mTargetTextSizePixels = convertSpToPx(textsize);
        mTextPaint.setTextSize(mTargetTextSizePixels);
        mCursorPaint.setTextSize(mTargetTextSizePixels);
    }


    public void setTextAlign(Layout.Alignment alignment) {
        mAlignment = alignment;
    }

    public void setMaxTextSize(float size) {
        setMaxTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    public void setMaxTextSize(int unit, float size) {
        mTextPaint.setTextSize(convertSpToPx(size));
        mCursorPaint.setTextSize(convertSpToPx(size));
        mMaxTextSizePixels = mTextPaint.getTextSize();
    }

    /**
     * Sets the lower text size limit
     *
     * @param minTextSizeScaledPixels the minimum size to use for text in this view,
     *                                in scaled pixels.
     */
    public void setMinTextSize(float minTextSizeScaledPixels) {
        mMinTextSizePixels = convertSpToPx(minTextSizeScaledPixels);
    }

    public void setLineSpacing(float add, float multiplier) {
        mLineSpacingMultiplier = multiplier;
        mLineSpacingExtra = add;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getText() {
        return mText;
    }

    /**
     * Resize this view's text size with respect to its width and height
     * (minus padding). You should always call this method after the initialization.
     */
    public void resizeText() {
        String textToWrite = mText;
        final int availableHeightPixels = mTextRect.height();

        final int availableWidthPixels = mTextRect.width();

        final CharSequence text = getText();

        // Safety check
        // (Do not resize if the view does not have dimensions or if there is no text)
        if (text == null
                || text.length() <= 0
                || availableHeightPixels <= 0
                || availableWidthPixels <= 0
                || mMaxTextSizePixels <= 0) {
            return;
        }

        float targetTextSizePixels = mMaxTextSizePixels;
        int targetTextHeightPixels =
                getTextHeightPixels(text, availableWidthPixels, targetTextSizePixels);

        // Until we either fit within our TextView
        // or we have reached our minimum text size,
        // incrementally try smaller sizes
        while (targetTextHeightPixels > availableHeightPixels
                && targetTextSizePixels > mMinTextSizePixels) {
            targetTextSizePixels = Math.max(targetTextSizePixels - 2, mMinTextSizePixels);

            targetTextHeightPixels =
                    getTextHeightPixels(text, availableWidthPixels, targetTextSizePixels);
        }

        // If we have reached our minimum text size and the text still doesn't fit,
        // append an ellipsis
        // (NOTE: Auto-ellipsize doesn't work hence why we have to do it here)

        if (targetTextSizePixels == mMinTextSizePixels
                && targetTextHeightPixels > availableHeightPixels) {
            // Make a copy of the original TextPaint object for measuring
            TextPaint textPaintCopy = new TextPaint(mTextPaint);
            textPaintCopy.setTextSize(targetTextSizePixels);

            // Measure using a StaticLayout instance
            StaticLayout staticLayout =
                    new StaticLayout(text, textPaintCopy, availableWidthPixels, Layout.Alignment.ALIGN_NORMAL,
                            mLineSpacingMultiplier, mLineSpacingExtra, false);

            // Check that we have a least one line of rendered text
            if (staticLayout.getLineCount() > 0) {
                // Since the line at the specific vertical position would be cut off,
                // we must trim up to the previous line and add an ellipsis
                int lastLine = staticLayout.getLineForVertical(availableHeightPixels) - 1;

                if (lastLine >= 0) {
                    int startOffset = staticLayout.getLineStart(lastLine);
                    int endOffset = staticLayout.getLineEnd(lastLine);
                    float lineWidthPixels = staticLayout.getLineWidth(lastLine);
                    float ellipseWidth = textPaintCopy.measureText(mEllipsis);

                    // Trim characters off until we have enough room to draw the ellipsis
                    while (availableWidthPixels < lineWidthPixels + ellipseWidth) {
                        endOffset--;
                        lineWidthPixels =
                                textPaintCopy.measureText(text.subSequence(startOffset, endOffset + 1).toString());
                    }

//                    setText(text.subSequence(0, endOffset) + mEllipsis);
                    textToWrite = (text.subSequence(0, endOffset) + mEllipsis);

                }
            }
        }
        mTextPaint.setTextSize(targetTextSizePixels);
        mStaticLayout =
                new StaticLayout(textToWrite, mTextPaint, mTextRect.width(), mAlignment, mLineSpacingMultiplier,
                        mLineSpacingExtra, true);


        SpannableStringBuilder base = new SpannableStringBuilder(mText);
        DynamicLayout dynamicLayout = new DynamicLayout(base, base, mTextPaint, mTextRect.width(), mAlignment, mLineSpacingMultiplier,
                mLineSpacingExtra, true);

    }

    public SpannableStringBuilder base;

    public void invalidateText() {
        String textToWrite = mText;
        int availableHeightPixels = mTextRect.height();

        int availableWidthPixels = mTextRect.width();

//        availableHeightPixels = getHeight();
//        availableWidthPixels = getWidth();
        final CharSequence text = getText();

        // Safety check
        // (Do not resize if the view does not have dimensions or if there is no text)
        if (text == null
                || text.length() <= 0
//                || availableHeightPixels <= 0
//                || availableWidthPixels <= 0
                || mMaxTextSizePixels <= 0) {
            return;
        }

        float targetTextSizePixels = mTargetTextSizePixels;
        int targetTextHeightPixels =
                getTextHeightPixels(text, availableWidthPixels, targetTextSizePixels);

        // Until we either fit within our TextView
        // or we have reached our minimum text size,
        // incrementally try smaller sizes
//        while (targetTextHeightPixels > availableHeightPixels
//                && targetTextSizePixels > mMinTextSizePixels) {
//            targetTextSizePixels = Math.max(targetTextSizePixels - 2, mMinTextSizePixels);
//
//            targetTextHeightPixels =
//                    getTextHeightPixels(text, availableWidthPixels, targetTextSizePixels);
//        }

        // If we have reached our minimum text size and the text still doesn't fit,
        // append an ellipsis
        // (NOTE: Auto-ellipsize doesn't work hence why we have to do it here)
        if (targetTextHeightPixels > availableHeightPixels) {
            // Make a copy of the original TextPaint object for measuring
            TextPaint textPaintCopy = new TextPaint(mTextPaint);
            textPaintCopy.setTextSize(targetTextSizePixels);

            // Measure using a StaticLayout instance
            StaticLayout staticLayout =
                    new StaticLayout(text, textPaintCopy, availableWidthPixels, Layout.Alignment.ALIGN_NORMAL,
                            mLineSpacingMultiplier, mLineSpacingExtra, false);

            // Check that we have a least one line of rendered text
            if (staticLayout.getLineCount() > 0) {
                // Since the line at the specific vertical position would be cut off,
                // we must trim up to the previous line and add an ellipsis
                int lastLine = staticLayout.getLineForVertical(availableHeightPixels) - 1;

                if (lastLine >= 0) {
                    int startOffset = staticLayout.getLineStart(lastLine);
                    int endOffset = staticLayout.getLineEnd(lastLine);
                    float lineWidthPixels = staticLayout.getLineWidth(lastLine);
                    float ellipseWidth = textPaintCopy.measureText(mEllipsis);

                    // Trim characters off until we have enough room to draw the ellipsis
                    while (availableWidthPixels < lineWidthPixels + ellipseWidth) {
                        endOffset--;
                        lineWidthPixels =
                                textPaintCopy.measureText(text.subSequence(startOffset, endOffset + 1).toString());
                    }

                    textToWrite = (text.subSequence(0, endOffset) + mEllipsis);

                } else {
                    textToWrite = "";
                }
            }
        }else{
            //add cursor

        }
        mTextPaint.setTextSize(targetTextSizePixels);
        mStaticLayout =
                new StaticLayout(textToWrite, mTextPaint, mTextRect.width(), mAlignment, mLineSpacingMultiplier,
                        mLineSpacingExtra, true);
        if (base == null) {
            SpannableStringBuilder base = new SpannableStringBuilder(mText);
            DynamicLayout dynamicLayout = new DynamicLayout(base, base, mTextPaint, mTextRect.width(), mAlignment, mLineSpacingMultiplier,
                    mLineSpacingExtra, true);
        }
    }

    /**
     * @return lower text size limit, in pixels.
     */
    public float getMinTextSizePixels() {
        return mMinTextSizePixels;
    }

    /**
     * Sets the text size of a clone of the view's {@link TextPaint} object
     * and uses a {@link StaticLayout} instance to measure the height of the text.
     *
     * @return the height of the text when placed in a view
     * with the specified width
     * and when the text has the specified size.
     */
    private int getTextHeightPixels(CharSequence source, int availableWidthPixels,
                                    float textSizePixels) {
        mTextPaint.setTextSize(textSizePixels);
        // It's not efficient to create a StaticLayout instance
        // every time when measuring, we can use StaticLayout.Builder
        // since api 23.
        StaticLayout staticLayout =
                new StaticLayout(source, mTextPaint, availableWidthPixels, Layout.Alignment.ALIGN_NORMAL,
                        mLineSpacingMultiplier, mLineSpacingExtra, true);
        return staticLayout.getHeight();
    }

    /**
     * @return the number of pixels which scaledPixels corresponds to on the device.
     */
    private float convertSpToPx(float scaledPixels) {
        return scaledPixels * mContext.getResources().getDisplayMetrics().scaledDensity;
    }


    @Override
    public RectF getInitialFrame() {
        return null;
    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }
}
