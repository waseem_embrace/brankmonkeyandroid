package com.embrace.it.beskrivapp.commons;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatEditText;


/**
 * Created by waseem on 10/17/17.
 */

public class MyEditText extends AppCompatEditText {
    public MyEditText(Context context) {
        super(context);
        this.setSingleLine(false);
        this.setBackgroundColor(Color.TRANSPARENT);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
