package com.embrace.it.beskrivapp.toolData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waseem on 2/14/17.
 */

public class ToolItemData {

    String title;
    String description;
    List<Integer> toolItemIcons;
    List<Integer> toolItemData;

    public ToolItemData() {
        toolItemIcons = new ArrayList<Integer>();
    }

    public ToolItemData(List<Integer> itemIcons, List<Integer> itemData, String title) {
        this.toolItemIcons = itemIcons;
        this.toolItemData = itemData;
        this.title = title;
        this.description = "";
    }

    public ToolItemData(List<Integer> itemIcons, List<Integer> itemData, String title, String description) {
        this.toolItemIcons = itemIcons;
        this.toolItemData = itemData;
        this.title = title;
        this.description = description;
    }

    public List<Integer> getToolItemData() {
        return toolItemData;
    }

    public void setToolItemData(List<Integer> toolItemData) {
        this.toolItemData = toolItemData;
    }


    public List<Integer> getToolItemIcons() {
        return toolItemIcons;
    }

    public void setToolItemIcons(List<Integer> toolItems) {
        this.toolItemIcons = toolItems;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
