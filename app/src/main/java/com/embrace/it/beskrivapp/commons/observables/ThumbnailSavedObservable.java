package com.embrace.it.beskrivapp.commons.observables;

import android.os.Bundle;

import java.util.Observable;

/**
 * Created by waseem on 2/21/17.
 */

public class ThumbnailSavedObservable extends Observable {


    public void notifyThumbnailSaved(String projectId,String subprojectId){
        setChanged();
        Bundle bundle = new Bundle();
        bundle.putString("projectId",projectId);
        bundle.putString("subProjectId",subprojectId);
        notifyObservers(bundle);
    }
}
