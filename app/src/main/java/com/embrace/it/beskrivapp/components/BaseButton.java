package com.embrace.it.beskrivapp.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.support.v7.widget.AppCompatImageView;


import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants.*;
import com.embrace.it.beskrivapp.commons.Constants.ToolType;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;


/**
 * Created by wasimshigri on 1/25/17.
 */

abstract public class BaseButton extends AppCompatImageView {


    protected ToolType buttonType;
    private ButtonGroupType buttonGroupType;


    public ButtonGroupType getButtonGroupType() {
        return buttonGroupType;
    }

    public void setButtonGroupType(ButtonGroupType buttonGroupType) {
        this.buttonGroupType = buttonGroupType;
    }

    public int buttonLocation = 1;

    public ToolType getButtonType() {
        return buttonType;
    }

    public void setButtonType(ToolType buttonType) {
        this.buttonType = buttonType;
    }

    public BaseButton(Context context) {
        super(context);
        buttonGroupType = ButtonGroupType.group0;
        buttonType = ToolType.None;
    }

    int width = 30;
    int height = 30;

    public BaseButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseButton);
            height = (int) a.getDimension(R.styleable.BaseButton_shape_height, 30);
            width = (int) a.getDimension(R.styleable.BaseButton_shape_width, 30);
            a.recycle();
        } catch (Exception e) {
        }
    }

    public BaseButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        setMeasuredDimension(width,height);
//    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                MyLog.emptyLog();
                break;
            case MotionEvent.ACTION_UP:
                MyLog.emptyLog();
                break;
        }
        return true;
    }
}

