package com.embrace.it.beskrivapp.components.touchbutton;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.components.BaseButton;

/**
 * Created by wasimshigri on 1/25/17.
 */

public  class ThumbSubButton extends BaseButton {

    public static ThumbSubButton getNewInstance(Context context) {
        ThumbSubButton btn = new ThumbSubButton(context);
        btn.setBackgroundColor(Color.parseColor("#00000000"));
        btn.setImageResource(R.drawable.icon);
        btn.setButtonType(Constants.ToolType.None);
        return btn;
    }
    public ThumbSubButton(Context context) {
        super(context);
        setBackgroundColor(Color.parseColor("#00ffffff"));

    }

    public ThumbSubButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThumbSubButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
