package com.embrace.it.beskrivapp.sticker;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.embrace.it.beskrivapp.database.ProjectItem;

/**
 * @author wupanjie
 */
public class DrawableSticker extends Sticker {

    private Drawable drawable;
    private Rect realBounds;

    public DrawableSticker(Drawable drawable, ImageView.ScaleType scaletype) {
        this.drawable = drawable;
        this.scaletype = scaletype;
        init();
    }
    public DrawableSticker(Drawable drawable) {
        this.drawable = drawable;
        this.scaletype = null;
        init();
    }

    public DrawableSticker() {
        init();
    }

    public void init() {
        this.matrix = new Matrix();
        realBounds = new Rect(0, 0, getWidth(), getHeight());
    }

    @Override
    public Drawable getDrawable() {
        return drawable;
    }

    @Override
    public void itemUpdated(int mainIndex, int chidlIndex) {

    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        return null;
    }

    @Override
    public void setProjectItem(ProjectItem p) {

    }
    @Override
    public RectF getInitialFrame() {
        return null;
    }
    @Override
    public ProjectItem getProjectItem() {
        return null;
    }
    @Override
    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.concat(matrix);
        drawable.setBounds(realBounds);
        drawable.draw(canvas);
        canvas.restore();
    }


    @Override
    public int getWidth() {
        if (drawable != null)
            return drawable.getIntrinsicWidth();
        else return 0;
    }

    @Override
    public int getHeight() {
        if (drawable != null)
            return drawable.getIntrinsicHeight();
        else return 0;
    }

    @Override
    public void release() {
        super.release();
        if (drawable != null) {
            drawable = null;
        }
    }
}
