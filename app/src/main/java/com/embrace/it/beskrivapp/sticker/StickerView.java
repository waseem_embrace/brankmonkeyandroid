package com.embrace.it.beskrivapp.sticker;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.text.Editable;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.MyEditText;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.toolframs.EditTextSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.MainImageSticker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Sticker view
 * Created by snowbean on 16-8-2.
 */

public class StickerView extends FrameLayout {

    private enum ActionMode {
        NONE,   //nothing
        DRAG,   //drag the sticker with your finger
        ZOOM_WITH_TWO_FINGER,   //zoom in or zoom out the sticker and rotate the sticker with two finger
        ICON,    //touch in icon
        CLICK    //Click the Sticker
    }

    private static final String TAG = "StickerView";

    private static final int DEFAULT_MIN_CLICK_DELAY_TIME = 200;
    public static final int FLIP_HORIZONTALLY = 0;
    public static final int FLIP_VERTICALLY = 1;

    private Paint borderPaint;
    private Paint borderPaintDashed;
    private Paint bgEdditingModePoint;
    private Paint bgHandlingStickerPaint;
    private Paint gridPaint;
    private Path gridPath;

    private RectF stickerRect;
    private Matrix sizeMatrix;
    private Matrix downMatrix;
    private Matrix downMatrixText;

    private Matrix moveMatrix;

    private BitmapStickerIcon currentIcon;

    private List<BitmapStickerIcon> icons = new ArrayList<>(4);
    //the first point down position
    private float downX;
    private float downY;

    private float oldDistance = 0f;
    private float oldRotation = 0f;

    private PointF midPoint;
    private PointF startPoint;

    private ActionMode currentMode = ActionMode.NONE;
    private Boolean stickerSelected = false;

    private List<Sticker> stickers = new ArrayList<>();

    private Sticker handlingSticker;

    private boolean locked;
    private boolean constrained;

    private int touchSlop = 3;

    private SimpleTouchListener mGestureListener;
    private OnStickerOperationListener onStickerOperationListener;

    private long lastClickTime = 0;
    private long touchDownTime = 0;
    private int minClickDelayTime = DEFAULT_MIN_CLICK_DELAY_TIME;

    private boolean drawGrid = false;
    private boolean zoomOnPinch = false;

    public StickerView(Context context) {
        this(context, null);
    }

    public StickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        borderPaint = new Paint();
        borderPaint.setAntiAlias(true);
        borderPaint.setColor(Color.WHITE);
        borderPaint.setStrokeWidth(2f);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setAlpha(200);

        gridPaint = new Paint();
        gridPaint.setAntiAlias(true);
        gridPaint.setColor(Color.DKGRAY);
        gridPaint.setStrokeWidth(3f);
        gridPaint.setStyle(Paint.Style.STROKE);
        gridPaint.setAlpha(200);

        bgEdditingModePoint = new Paint();
        bgEdditingModePoint.setAntiAlias(true);
        bgEdditingModePoint.setColor(Color.BLACK);
        bgEdditingModePoint.setStyle(Paint.Style.FILL);
        bgEdditingModePoint.setAlpha(128);

        bgHandlingStickerPaint = new Paint();
        bgHandlingStickerPaint.setAntiAlias(true);
        bgHandlingStickerPaint.setColor(Color.WHITE);
        bgHandlingStickerPaint.setStyle(Paint.Style.FILL);
        bgHandlingStickerPaint.setAlpha(128);

        borderPaintDashed = new Paint();
        borderPaintDashed.setAntiAlias(true);
        borderPaintDashed.setColor(Color.WHITE);
        borderPaintDashed.setStyle(Paint.Style.STROKE);
        borderPaintDashed.setStrokeJoin(Paint.Join.ROUND);
        borderPaintDashed.setStrokeWidth(2f);
        borderPaintDashed.setAlpha(200);
        DashPathEffect dashPath = new DashPathEffect(new float[]{6, 6}, 1);
        borderPaintDashed.setPathEffect(dashPath);

        sizeMatrix = new Matrix();
        downMatrix = new Matrix();
        moveMatrix = new Matrix();
        downMatrixText = new Matrix();
        stickerRect = new RectF();

//        initInputText();
    }

    public void configDefaultIcons() {
        BitmapStickerIcon deleteIcon = new BitmapStickerIcon(
                ContextCompat.getDrawable(getContext(), R.drawable.cancel),
                BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());
        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(
                ContextCompat.getDrawable(getContext(), R.drawable.expand_arrow),
                BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());
        BitmapStickerIcon flipIcon = new BitmapStickerIcon(
                ContextCompat.getDrawable(getContext(), R.drawable.double_arrow),
                BitmapStickerIcon.RIGHT_TOP);
        flipIcon.setIconEvent(new RotateIconEvent());

        icons.clear();
        icons.add(deleteIcon);
        icons.add(zoomIcon);
        icons.add(flipIcon);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            stickerRect.left = left;
            stickerRect.top = top;
            stickerRect.right = right;
            stickerRect.bottom = bottom;
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        drawStickers(canvas);
    }

    private void drawStickers(Canvas canvas) {
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null) {
                if (handlingSticker == null || !handlingSticker.equals(sticker))
                    sticker.draw(canvas);
            }
        }


        if (handlingSticker != null && !locked) {


            canvas.drawPaint(bgEdditingModePoint);

            float[] bitmapPoints = getStickerPoints(handlingSticker);

            float x1 = bitmapPoints[0];
            float y1 = bitmapPoints[1];
            float x2 = bitmapPoints[2];
            float y2 = bitmapPoints[3];
            float x3 = bitmapPoints[4];
            float y3 = bitmapPoints[5];
            float x4 = bitmapPoints[6];
            float y4 = bitmapPoints[7];

            Path path = new Path();
            path.moveTo(x1, y1);
            path.lineTo(x2, y2);
            path.lineTo(x4, y4);
            path.lineTo(x3, y3);
            path.close();

            if (!(handlingSticker instanceof MainImageSticker)) {
                canvas.drawPath(path, bgHandlingStickerPaint);
                canvas.drawPath(path, borderPaintDashed);
            }
            //draw icons
            float rotation = calculateRotation(x4, y4, x3, y3);
            for (BitmapStickerIcon icon : icons) {
                switch (icon.getPosition()) {
                    case BitmapStickerIcon.LEFT_TOP:
                        if (handlingSticker.crossable) {
                            configIconMatrix(icon, x1, y1, rotation);
                            icon.draw(canvas, bgHandlingStickerPaint);
                            icon.draw(canvas, borderPaint);

                        }
                        break;

                    case BitmapStickerIcon.RIGHT_TOP:
                        if (handlingSticker.rotatable) {
                            configIconMatrix(icon, x2, y2, rotation);
                            icon.draw(canvas, bgHandlingStickerPaint);
                            icon.draw(canvas, borderPaint);

                        }
                        break;

                    case BitmapStickerIcon.LEFT_BOTTOM:
//                        if(handlingSticker.crossable) {
                        configIconMatrix(icon, x3, y3, rotation);
                        icon.draw(canvas, bgHandlingStickerPaint);
                        icon.draw(canvas, borderPaint);

//                        }
                        break;

                    case BitmapStickerIcon.RIGHT_BOTOM:
                        if (handlingSticker.resizeable) {
                            configIconMatrix(icon, x4, y4, rotation);
                            icon.draw(canvas, bgHandlingStickerPaint);
                            icon.draw(canvas, borderPaint);

                        }
                        break;
                }

            }
        }

        if (handlingSticker != null)
            handlingSticker.draw(canvas);

        if (drawGrid && gridPath != null) {
            canvas.drawPath(gridPath, gridPaint);
        }
    }

    public void clearEditing() {
        handlingSticker = null;
        invalidate();
    }


    private void configIconMatrix(BitmapStickerIcon icon, float x, float y, float rotation) {
        icon.setX(x);
        icon.setY(y);
        icon.getMatrix().reset();

        icon.getMatrix().postRotate(rotation, icon.getWidth() / 2, icon.getHeight() / 2);
        icon.getMatrix().postTranslate(x - icon.getWidth() / 2, y - icon.getHeight() / 2);
    }

    Boolean BackgorundTouchMode = false;
    Sticker tempDownSticker;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (locked) return super.onInterceptTouchEvent(ev);

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = ev.getX();
                downY = ev.getY();
                BackgorundTouchMode = false;
                tempDownSticker = findHandlingSticker();
                Boolean b = findCurrentIconTouched() != null || tempDownSticker != null;
                onStickerOperationListener.touchedDown(ev.getX(), ev.getY(), b);
                if (b == false) {
                    BackgorundTouchMode = true;
                }
                return true;
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (mGestureListener != null)
            mGestureListener.onTouch(this, event);
        if (locked) return super.onTouchEvent(event);

        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                touchDownTime = SystemClock.uptimeMillis();

                currentMode = ActionMode.DRAG;
                downX = event.getX();
                downY = event.getY();

                midPoint = calculateMidPoint();
                startPoint = calculateStartPoint();

                oldDistance = calculateDistance(midPoint.x, midPoint.y, downX, downY);
                oldDistanceX = Math.abs(midPoint.x - downX);
                oldDistanceY = Math.abs(midPoint.y - downY);

                oldDistanceX = Math.abs(startPoint.x - downX);
                oldDistanceY = Math.abs(startPoint.y - downY);

                oldRotation = calculateRotation(midPoint.x, midPoint.y, downX, downY);

                currentIcon = findCurrentIconTouched();
                if (currentIcon != null) {
                    currentMode = ActionMode.ICON;
                    currentIcon.onActionDown(this, event);
                }
//                else {
//                    handlingSticker = findHandlingSticker();
//                }

                if (handlingSticker != null) {
                    downMatrix.set(handlingSticker.getMatrix());
                    if (handlingSticker instanceof EditTextSticker) {
                        EditTextSticker et = ((EditTextSticker) handlingSticker);
                        downMatrixText.set(et.getMatrixText());
                    }
                }

//                if (!BackgorundTouchMode) {
//                    invalidate();
//                }
                break;

            case MotionEvent.ACTION_POINTER_DOWN:

//                if (!BackgorundTouchMode) {

                oldDistance = calculateDistance(event);
                oldRotation = calculateRotation(event);

                midPoint = calculateMidPoint(event);

                if (handlingSticker != null && isInStickerArea(handlingSticker, event.getX(1),
                        event.getY(1)) && findCurrentIconTouched() == null) {

                    //TODO: uncomment
                    if (this.zoomOnPinch)
                        currentMode = ActionMode.ZOOM_WITH_TWO_FINGER;
                }
//                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (!BackgorundTouchMode) {
//                    if(stickerSelected == false){
//                        handlingSticker = null;
//                    }
                    handleCurrentMode(event);
                    invalidate();
                }
                break;

            case MotionEvent.ACTION_UP:
                long currentTime = SystemClock.uptimeMillis();
//                if (!BackgorundTouchMode) {

                if (currentMode == ActionMode.ICON && currentIcon != null && handlingSticker != null) {
                    currentIcon.onActionUp(this, event);
                }

                if (currentMode == ActionMode.DRAG
                        && Math.abs(event.getX() - downX) < touchSlop
                        && Math.abs(event.getY() - downY) < touchSlop
                        ) {

                    if ((currentTime - touchDownTime) < minClickDelayTime) {
//                        if ((currentTime - touchDownTime) < minClickDelayTime && (handlingSticker == null || BackgorundTouchMode))

                        clicked(event);

                        if (handlingSticker != null && BackgorundTouchMode == false) {
                            currentMode = ActionMode.CLICK;
                            if (onStickerOperationListener != null) {
                                onStickerOperationListener.onStickerClicked(handlingSticker);
                            }
                        } else if (onStickerOperationListener != null) {
                            onStickerOperationListener.onStickerViewClicked(downX, downY);
                            stickerSelected = false;
                        }
                    } else
//                        if (handlingSticker != null) {
//                        currentMode = ActionMode.CLICK;
//                        if (onStickerOperationListener != null) {
////                            clicked(event);
//                            onStickerOperationListener.onStickerClicked(handlingSticker);
//                        }
                        if (currentTime - lastClickTime < minClickDelayTime) {
                            if (onStickerOperationListener != null) {
                                onStickerOperationListener.onStickerDoubleTapped(handlingSticker);
                            }
                        }
//                    }
                }

                if (currentMode == ActionMode.DRAG && handlingSticker != null) {
                    if (onStickerOperationListener != null) {
                        onStickerOperationListener.onStickerDragFinished(handlingSticker);
                    }
                }

                currentMode = ActionMode.NONE;
                lastClickTime = currentTime;
//                }
                onStickerOperationListener.touchedUp();
                break;

            case MotionEvent.ACTION_POINTER_UP:
                if (!BackgorundTouchMode) {

                    if (currentMode == ActionMode.ZOOM_WITH_TWO_FINGER && handlingSticker != null) {
                        if (onStickerOperationListener != null) {
                            onStickerOperationListener.onStickerZoomFinished(handlingSticker);
                        }
                    }
                    currentMode = ActionMode.NONE;
                }
                onStickerOperationListener.touchedUp();
                break;
        }//end of switch(action)

        return true;
    }

    void clicked(MotionEvent event) {
/*
        currentMode = ActionMode.CLICK;

        touchDownTime = SystemClock.uptimeMillis();
        currentMode = ActionMode.DRAG;
        downX = event.getX();
        downY = event.getY();

        midPoint = calculateMidPoint();
        startPoint = calculateStartPoint();

        oldDistance = calculateDistance(midPoint.x, midPoint.y, downX, downY);
        oldDistanceX = Math.abs(midPoint.x - downX);
        oldDistanceY = Math.abs(midPoint.y - downY);

        oldDistanceX = Math.abs(startPoint.x - downX);
        oldDistanceY = Math.abs(startPoint.y - downY);

        oldRotation = calculateRotation(midPoint.x, midPoint.y, downX, downY);

        currentIcon = findCurrentIconTouched();
        if (currentIcon != null) {
           currentMode = ActionMode.ICON;
            currentIcon.onActionDown(this, event);
        } else
        */
        if (currentIcon == null) {
            handlingSticker = findHandlingSticker();
        }
/*
        if (handlingSticker != null) {
//            downMatrix.set(handlingSticker.getMatrix());
            if (handlingSticker instanceof EditTextSticker) {
                EditTextSticker et = ((EditTextSticker) handlingSticker);
                downMatrixText.set(et.getMatrixText());
            }
        }
        */
        if (!BackgorundTouchMode) {
            invalidate();
            stickerSelected = true;
        }
    }

    private void handleCurrentMode(MotionEvent event) {
        switch (currentMode) {
            case NONE:
                break;
            case DRAG:
                if (handlingSticker != null && stickerSelected && handlingSticker.equals(tempDownSticker)) {
                    moveMatrix.set(downMatrix);
                    moveMatrix.postTranslate(event.getX() - downX, event.getY() - downY);
                    handlingSticker.getMatrix().set(moveMatrix);
                    if (handlingSticker instanceof EditTextSticker) {
                        EditTextSticker et = (EditTextSticker) handlingSticker;
                        moveMatrix.set(downMatrixText);
                        moveMatrix.postTranslate(event.getX() - downX, event.getY() - downY);
                        et.getMatrixText().set(moveMatrix);
                        moveEdittext(et);
                    }
                    //constrain sticker
                    if (constrained) constrainSticker();
                } else {
                    return;
                }
                break;
            case ZOOM_WITH_TWO_FINGER:
                if (handlingSticker != null) {
                    float newDistance = calculateDistance(event);
                    float newRotation = calculateRotation(event);

                    moveMatrix.set(downMatrix);
                    moveMatrix.postScale(newDistance / oldDistance, newDistance / oldDistance, midPoint.x,
                            midPoint.y);
                    moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y);
                    handlingSticker.getMatrix().set(moveMatrix);
                }

                break;

            case ICON:
                if (handlingSticker != null && currentIcon != null) {
                    currentIcon.onActionMove(this, event);
                }

                break;
        }// end of switch(currentMode)

        invalidate();
    }

    float angleBeforeZoom = 0;

    public void zoomActionDown(MotionEvent event) {
        Sticker sticker = handlingSticker;
        try {
            if (sticker != null) {
                float[] bitmapPoints = getStickerPoints(sticker);
                float x1 = bitmapPoints[0];
                float y1 = bitmapPoints[1];
                float x2 = bitmapPoints[2];
                float y2 = bitmapPoints[3];
                float x3 = bitmapPoints[4];
                float y3 = bitmapPoints[5];
                float x4 = bitmapPoints[6];
                float y4 = bitmapPoints[7];
                float cx = (x1 + x2 + x3 + x4) / 4f;
                float cy = (y1 + y2 + y3 + y4) / 4f;
//                        int width = (int) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
//                        int height = (int) Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
                angleBeforeZoom = calculateRotation(x4, y4, x3, y3);
            }
        } catch (Exception e) {
        }
    }

    public void zoomCurrentSticker(MotionEvent event) {
        zoomSticker(handlingSticker, event);
    }

    float oldDistanceX = 0, oldDistanceY = 0;

    PointF rotate_point(float cx, float cy, float angle, PointF p) {
        double s = Math.sin(angle);
        double c = Math.cos(angle);

        // translate point back to origin:
        p.x -= cx;
        p.y -= cy;

        // rotate point
        double xnew = p.x * c - p.y * s;
        double ynew = p.x * s + p.y * c;

        // translate point back:
        p.x = (float) (xnew + cx);
        p.y = (float) (ynew + cy);
        return p;
    }

    public void zoomSticker(Sticker sticker, MotionEvent event) {
        if (sticker != null) {

            float newDistance = calculateDistance(midPoint.x, midPoint.y, event.getX(), event.getY());
            float newDistanceX = Math.abs(midPoint.x - event.getX());
            float newDistanceY = Math.abs(midPoint.y - event.getY());
            //testing
            newDistanceX = Math.abs(startPoint.x - event.getX());
            newDistanceY = Math.abs(startPoint.y - event.getY());
            //
            moveMatrix.set(downMatrix);
            if (handlingSticker instanceof EditTextSticker) {

//                PointF newP = new PointF(event.getX(), event.getY());
//                rotate_point(midPoint.x, midPoint.x,-angleBeforeZoom, newP);
//                newDistanceX = Math.abs(startPoint.x - newP.x);
//                newDistanceY = Math.abs(startPoint.y - newP.y);

//                moveMatrix.postRotate(-angleBeforeZoom, midPoint.x, midPoint.y);
                moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, startPoint.x, startPoint.y);
//                moveMatrix.postRotate(angleBeforeZoom, midPoint.x, midPoint.y);


//                moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, midPoint.x, midPoint.y);
                handlingSticker.getMatrix().set(moveMatrix);

                EditTextSticker et = ((EditTextSticker) handlingSticker);

//                    moveMatrix.set(downMatrixText);
//                    moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, startPoint.x, startPoint.y);
//                    et.getMatrixText().set(moveMatrix);
//                    et.scaleTextSize();

                et.updateRects();
                et.invalidateText();
                resizeEdittext(et);
            } else {
//                moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, midPoint.x, midPoint.y);
                moveMatrix.postScale((newDistanceX / oldDistanceX), newDistanceY / oldDistanceY, startPoint.x, startPoint.y);
                handlingSticker.getMatrix().set(moveMatrix);
            }
        }
    }

    public void zoomStickerOld(Sticker sticker, MotionEvent event) {
        if (sticker != null) {
            float newDistance = calculateDistance(midPoint.x, midPoint.y, event.getX(), event.getY());
            float newDistanceX = Math.abs(midPoint.x - event.getX());
            float newDistanceY = Math.abs(midPoint.y - event.getY());
            //testing
            newDistanceX = Math.abs(startPoint.x - event.getX());
            newDistanceY = Math.abs(startPoint.y - event.getY());
            //
            moveMatrix.set(downMatrix);
            if (handlingSticker instanceof EditTextSticker) {

                moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, startPoint.x, startPoint.y);


//                moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, midPoint.x, midPoint.y);
                handlingSticker.getMatrix().set(moveMatrix);

                EditTextSticker et = ((EditTextSticker) handlingSticker);

//                    moveMatrix.set(downMatrixText);
//                    moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, startPoint.x, startPoint.y);
//                    et.getMatrixText().set(moveMatrix);
//                    et.scaleTextSize();

                et.updateRects();
                et.invalidateText();
                resizeEdittext(et);
            } else {
//                moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, midPoint.x, midPoint.y);
                moveMatrix.postScale((newDistanceX / oldDistanceX), newDistanceY / oldDistanceY, startPoint.x, startPoint.y);
                handlingSticker.getMatrix().set(moveMatrix);
            }
        }
    }


    public void rotateCurrentSticker(MotionEvent event) {
        rotateSticker(handlingSticker, event);
    }


    public void rotateSticker(Sticker sticker, MotionEvent event) {
        if (sticker != null) {
            float newDistance = calculateDistance(midPoint.x, midPoint.y, event.getX(), event.getY());
            float newRotation = calculateRotation(midPoint.x, midPoint.y, event.getX(), event.getY());

            moveMatrix.set(downMatrix);

            moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y);
            handlingSticker.getMatrix().set(moveMatrix);

            if (handlingSticker instanceof EditTextSticker) {
                EditTextSticker et = ((EditTextSticker) handlingSticker);
                moveMatrix.set(downMatrixText);
                moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y);
                et.getMatrixText().set(moveMatrix);
//                et.scaleMatrixText();
//                et.scaleTextSize();
//                et.invalidateText();
                rotateEdittext(newRotation);
            }
        }
    }

    private void constrainSticker() {
        float moveX = 0;
        float moveY = 0;
        PointF currentCenterPoint = handlingSticker.getMappedCenterPoint();
        if (currentCenterPoint.x < 0) {
            moveX = -currentCenterPoint.x;
        }

        if (currentCenterPoint.x > getWidth()) {
            moveX = getWidth() - currentCenterPoint.x;
        }

        if (currentCenterPoint.y < 0) {
            moveY = -currentCenterPoint.y;
        }

        if (currentCenterPoint.y > getHeight()) {
            moveY = getHeight() - currentCenterPoint.y;
        }

        handlingSticker.getMatrix().postTranslate(moveX, moveY);
        if (handlingSticker instanceof EditTextSticker) {
            EditTextSticker et = ((EditTextSticker) handlingSticker);
            et.getMatrixText().postTranslate(moveX, moveY);
        }
    }


    private BitmapStickerIcon findCurrentIconTouched() {
        for (BitmapStickerIcon icon : icons) {
            float x = icon.getX() - downX;
            float y = icon.getY() - downY;
            float distance_pow_2 = x * x + y * y;
            if (distance_pow_2 <= Math.pow(icon.getIconRadius() + icon.getIconRadius(), 2)) {
                return icon;
            }
        }

        return null;
    }

    /**
     * find the touched Sticker
     **/
    private Sticker findHandlingSticker() {
        for (int i = stickers.size() - 1; i >= 0; i--) {
            if (isInStickerArea(stickers.get(i), downX, downY)) {
                return stickers.get(i);
            }
        }
        return null;
    }

    private boolean isInStickerArea(Sticker sticker, float downX, float downY) {
        return sticker.contains(downX, downY);
    }

    private PointF calculateMidPoint(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) return new PointF();
        float x = (event.getX(0) + event.getX(1)) / 2;
        float y = (event.getY(0) + event.getY(1)) / 2;
        return new PointF(x, y);
    }

    private PointF calculateMidPoint() {
        if (handlingSticker == null) return new PointF();
        return handlingSticker.getMappedCenterPoint();
    }

    private PointF calculateStartPoint(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) return new PointF();
        float x = (event.getX(0) + event.getX(1)) / 2;
        float y = (event.getY(0) + event.getY(1)) / 2;
        return new PointF(x, y);
    }

    private PointF calculateStartPoint() {
        if (handlingSticker == null) return new PointF();
        return handlingSticker.getMappedStartPoint();
    }


    /**
     * calculate rotation in line with two fingers and x-axis
     **/
    private float calculateRotation(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) return 0f;
        double x = event.getX(0) - event.getX(1);
        double y = event.getY(0) - event.getY(1);
        double radians = Math.atan2(y, x);
        return (float) Math.toDegrees(radians);
    }

    private float calculateRotation(float x1, float y1, float x2, float y2) {
        double x = x1 - x2;
        double y = y1 - y2;
        double radians = Math.atan2(y, x);
        return (float) Math.toDegrees(radians);
    }

    /**
     * calculate Distance in two fingers
     **/
    private float calculateDistance(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) return 0f;
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);

        return (float) Math.sqrt(x * x + y * y);
    }

    private float calculateDistance(float x1, float y1, float x2, float y2) {
        double x = x1 - x2;
        double y = y1 - y2;

        return (float) Math.sqrt(x * x + y * y);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null) {
                transformSticker(sticker);
            }
        }
    }

    /**
     * Sticker's drawable will be too bigger or smaller
     * This method is to transform it to fit
     * step 1：let the center of the sticker image is coincident with the center of the View.
     * step 2：Calculate the zoom and zoom
     **/
    private void transformSticker(Sticker sticker) {
        if (sticker == null) {
            Log.e(TAG, "transformSticker: the bitmapSticker is null or the bitmapSticker bitmap is null");
            return;
        }

        if (sizeMatrix != null) {
            sizeMatrix.reset();
        }

        //step 1
        float offsetX = (getWidth() - sticker.getWidth()) / 2;
        float offsetY = (getHeight() - sticker.getHeight()) / 2;

        if (sticker.getFrame() != null) {
            offsetX = sticker.getFrame().centerX();
            offsetY = sticker.getFrame().centerY();
        }
        sizeMatrix.postTranslate(offsetX, offsetY);
        if (sticker instanceof EditTextSticker) {
            try {
                ((EditTextSticker) sticker).getMatrixText().reset();
                ((EditTextSticker) sticker).getMatrixText().postTranslate(offsetX, offsetY);
            } catch (Exception e) {
            }
        }
        //step 2
        float scaleFactor;
        if (getWidth() > getHeight()) {
            scaleFactor = (float) getWidth() / sticker.getWidth();
        } else {
            scaleFactor = (float) getHeight() / sticker.getHeight();
        }

        if (sticker.getFrame() != null) {

            if (getWidth() < getHeight()) {
                scaleFactor = (float) (sticker.getFrame().right - sticker.getFrame().left) / sticker.getWidth();
            } else {
                scaleFactor = (float) (sticker.getFrame().bottom - sticker.getFrame().top) / sticker.getHeight();
            }
            sizeMatrix.postScale(scaleFactor, scaleFactor, sticker.getFrame().centerX(), sticker.getFrame().centerY());
        }
        if (sticker.getFrame() == null) {
            if (sticker.scaletype == ImageView.ScaleType.FIT_XY)
                sizeMatrix.postScale(scaleFactor, scaleFactor, getWidth() / 2, getHeight() / 2);
            else
                sizeMatrix.postScale(scaleFactor / 3, scaleFactor / 3, getWidth() / 2, getHeight() / 2);
        }
        sticker.getMatrix().reset();
        sticker.getMatrix().set(sizeMatrix);
        if (sticker instanceof EditTextSticker) {
            ((EditTextSticker) sticker).updateRects();
        }
        invalidate();
    }

    public void flipCurrentSticker(int direction) {
        flip(handlingSticker, direction);
    }

    public void flip(Sticker sticker, int direction) {
        if (sticker != null) {
            if (direction == FLIP_HORIZONTALLY) {
                sticker.getMatrix().preScale(-1, 1, sticker.getCenterPoint().x, sticker.getCenterPoint().y);
                sticker.setFlippedHorizontally(!sticker.isFlippedHorizontally);
            } else if (direction == FLIP_VERTICALLY) {
                sticker.getMatrix().preScale(1, -1, sticker.getCenterPoint().x, sticker.getCenterPoint().y);
                sticker.setFlippedVertically(!sticker.isFlippedVertically);
            }

            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerFlipped(sticker);
            }

            invalidate();
        }
    }

    public boolean replace(Sticker sticker) {
        return replace(sticker, true);
    }

    public boolean replace(Sticker sticker, boolean needStayState) {
        if (handlingSticker != null && sticker != null) {
            if (needStayState) {
                sticker.getMatrix().set(handlingSticker.getMatrix());
                sticker.setFlippedVertically(handlingSticker.isFlippedVertically());
                sticker.setFlippedHorizontally(handlingSticker.isFlippedHorizontally());
            } else {
                handlingSticker.getMatrix().reset();
                // reset scale, angle, and put it in center
                float offsetX = (getWidth() - handlingSticker.getWidth()) / 2;
                float offsetY = (getHeight() - handlingSticker.getHeight()) / 2;
                sticker.getMatrix().postTranslate(offsetX, offsetY);

                float scaleFactor;
                if (getWidth() < getHeight()) {
                    scaleFactor = (float) getWidth() / handlingSticker.getDrawable().getIntrinsicWidth();
                } else {
                    scaleFactor = (float) getHeight() / handlingSticker.getDrawable().getIntrinsicHeight();
                }
                sticker.getMatrix().postScale(scaleFactor / 2, scaleFactor / 2, getWidth() / 2, getHeight() / 2);
            }
            int index = stickers.indexOf(handlingSticker);
            stickers.set(index, sticker);
            handlingSticker = sticker;

            invalidate();
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(Sticker sticker) {
        stickerSelected = false;
        if (stickers.contains(sticker)) {
            stickers.remove(sticker);
            if (onStickerOperationListener != null) {
                onStickerOperationListener.onStickerDeleted(sticker);
            }
            if (handlingSticker == sticker) {
                handlingSticker = null;
            }
            invalidate();

            return true;
        } else {
            Log.d(TAG, "remove: the sticker is not in this StickerView");

            return false;
        }
    }

    public boolean removeCurrentSticker() {
        return remove(handlingSticker);
    }

    public void removeAllStickers() {
        stickers.clear();
        if (handlingSticker != null) {
            handlingSticker.release();
            handlingSticker = null;
        }
        invalidate();
    }

    public void addSticker(Sticker sticker) {
        initSticker(sticker);
    }

    public void addSticker(Sticker sticker, int x, int y) {
        initSticker(sticker, x, y);
    }

    public void addSticker(Sticker sticker, RectF fram) {
        sticker.setFrame(fram);
        initSticker(sticker);
    }


    public void addStickerFitImage(Sticker sticker) {
        initSticker(sticker);
    }

    private void initSticker(Sticker sticker) {
        initSticker(sticker, true, 0, 0);
        stickerSelected = true;
    }

    private void initSticker(Sticker sticker, int x, int y) {
        initSticker(sticker, true, x, y);
        stickerSelected = true;
    }

    private void initStickerOld(Sticker sticker, boolean shouldScale) {
        if (sticker == null) {
            Log.e(TAG, "Sticker to be added is null!");
            return;
        }
//        float offsetX = (getWidth() - sticker.getWidth()) / 2;
//        float offsetY = (getHeight() - sticker.getHeight()) / 2;
//        sticker.getMatrix().postTranslate(offsetX, offsetY);
//
//        float scaleFactor;
//        if (getWidth() < getHeight()) {
//            scaleFactor = (float) getWidth() / sticker.getDrawable().getIntrinsicWidth();
//        } else {
//            float ff = getHeight();
//            float fsfsf = sticker.getDrawable().getIntrinsicHeight();
//            Drawable d = sticker.getDrawable();
//            scaleFactor = (float) getHeight() / sticker.getDrawable().getIntrinsicHeight();
//        }
//        sticker.getMatrix().postScale(scaleFactor / 2, scaleFactor / 2, getWidth() / 2, getHeight() / 2);
//
//        handlingSticker = sticker;
//        stickers.add(sticker);
//        invalidate();
//


        //step 1
        float offsetX = (getWidth() - sticker.getWidth()) / 2;
        float offsetY = (getHeight() - sticker.getHeight()) / 2;

        if (sticker.getFrame() != null) {
            offsetX = sticker.getFrame().left;
            offsetY = sticker.getFrame().top;
        }
        sticker.getMatrix().postTranslate(offsetX, offsetY);
        if (sticker instanceof EditTextSticker) {
            ((EditTextSticker) sticker).getMatrixText().postTranslate(offsetX, offsetY);
        }
        //step 2
        float scaleFactor;
        if (getWidth() > getHeight()) {
            scaleFactor = (float) getWidth() / sticker.getWidth();
        } else {
            scaleFactor = (float) getHeight() / sticker.getHeight();
        }

        if (sticker.getFrame() != null) {

            if (getWidth() < getHeight()) {
                scaleFactor = (float) (sticker.getFrame().right - sticker.getFrame().left) / sticker.getWidth();
            } else {
                scaleFactor = (float) (sticker.getFrame().bottom - sticker.getFrame().top) / sticker.getHeight();
            }
            sticker.getMatrix().postScale(scaleFactor, scaleFactor, sticker.getFrame().centerX(), sticker.getFrame().centerY());
//            if(sticker instanceof EditTextSticker)
//            {
//                ((EditTextSticker) sticker).getMatrixText().postScale(scaleFactor, scaleFactor, sticker.getFrame().centerX(), sticker.getFrame().centerY());
//            }
        }
        if (sticker.getFrame() == null && !(sticker instanceof EditTextSticker)) {
            if (sticker.scaletype == ImageView.ScaleType.FIT_XY) {
                sticker.getMatrix().postScale(scaleFactor, scaleFactor, getWidth() / 2, getHeight() / 2);
            } else {
                sticker.getMatrix().postScale(scaleFactor / 3, scaleFactor / 3, getWidth() / 2, getHeight() / 2);
            }
        }
        handlingSticker = sticker;
        stickers.add(sticker);
        if (sticker instanceof EditTextSticker) {
            ((EditTextSticker) sticker).updateRects();
            ((EditTextSticker) sticker).invalidateText();
        }
        invalidate();
    }

    private void initSticker(Sticker sticker, boolean shouldScale, int x, int y) {
        if (sticker == null) {
            Log.e(TAG, "Sticker to be added is null!");
            return;
        }

        //step 1
        float offsetX = (getWidth() - sticker.getWidth()) / 2;
        float offsetY = (getHeight() - sticker.getHeight()) / 2;
        if (x > 0 && y > 0) {
            offsetX = (x - sticker.getWidth()) / 2;
            offsetY = (y - sticker.getHeight()) / 2;

        }
        if (sticker.getFrame() != null) {
            offsetX = sticker.getFrame().left;
            offsetY = sticker.getFrame().top;
        }
        sticker.getMatrix().postTranslate(offsetX, offsetY);
        if (sticker instanceof EditTextSticker) {
            ((EditTextSticker) sticker).getMatrixText().postTranslate(offsetX, offsetY);
        }
        //step 2
        float scaleFactor;
        if (getWidth() > getHeight()) {
            scaleFactor = (float) getHeight() / sticker.getHeight();
        } else {
            scaleFactor = (float) getWidth() / sticker.getWidth();

        }

        if (sticker.getFrame() != null) {

            if (getWidth() < getHeight()) {
                scaleFactor = (float) (sticker.getFrame().right - sticker.getFrame().left) / sticker.getWidth();
            } else {
                scaleFactor = (float) (sticker.getFrame().bottom - sticker.getFrame().top) / sticker.getHeight();
            }
            sticker.getMatrix().postScale(scaleFactor, scaleFactor, sticker.getFrame().centerX(), sticker.getFrame().centerY());
//            if(sticker instanceof EditTextSticker)
//            {
//                ((EditTextSticker) sticker).getMatrixText().postScale(scaleFactor, scaleFactor, sticker.getFrame().centerX(), sticker.getFrame().centerY());
//            }
        } else if (sticker.getFrame() == null && !(sticker instanceof EditTextSticker)) {
            if (sticker.scaletype == ImageView.ScaleType.FIT_XY) {
                sticker.getMatrix().postScale(scaleFactor, scaleFactor, getWidth() / 2, getHeight() / 2);
            } else {
                if (x > 0 && y > 0) {
                    sticker.getMatrix().postScale(1f / (6f - scaleFactor), 1f / (6f - scaleFactor), x, y);
                } else {
                    sticker.getMatrix().postScale(scaleFactor / 3, scaleFactor / 3, getWidth() / 2, getHeight() / 2);
                }
            }
        }
        handlingSticker = sticker;
        stickers.add(sticker);
        if (sticker instanceof EditTextSticker) {
            ((EditTextSticker) sticker).updateRects();
            ((EditTextSticker) sticker).invalidateText();
        }
        invalidate();
    }

    public float[] getStickerPoints(Sticker sticker) {
        if (sticker == null) return new float[8];
        return sticker.getMappedBoundPoints();
    }

    public void save(File file) {
        StickerUtils.saveImageToGallery(file, createBitmap());
        StickerUtils.notifySystemGallery(getContext(), file);
    }

    public Bitmap createBitmap() {
        handlingSticker = null;
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        this.draw(canvas);
        return bitmap;
    }

    public int getStickerCount() {
        return stickers.size();
    }

    public boolean isNoneSticker() {
        return getStickerCount() == 0;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
        invalidate();
    }

    public void setMinClickDelayTime(int minClickDelayTime) {
        this.minClickDelayTime = minClickDelayTime;
    }

    public int getMinClickDelayTime() {
        return minClickDelayTime;
    }

    public boolean isConstrained() {
        return constrained;
    }

    public void setConstrained(boolean constrained) {
        this.constrained = constrained;
        postInvalidate();
    }

    public void zoomOnPinch(boolean b) {
        this.zoomOnPinch = b;
    }

    public void drawGrid(Boolean b) {
        drawGrid = b;
        if (gridPath == null) {
            float top = getTop();
            float left = getLeft();
            float bottom = getBottom();
            float right = getRight();
            float width = getWidth();
            float height = getHeight();
            float boxHeight = height / 5;
            float boxWidth = width / 3;

            gridPath = new Path();

            for (int i = 0; i <= 5; i++) {
                gridPath.moveTo(left, top + (i * boxHeight));
                gridPath.lineTo(right, top + (i * boxHeight));
            }
            for (int i = 0; i <= 3; i++) {
                gridPath.moveTo(left + (i * boxWidth), top);
                gridPath.lineTo(left + (i * boxWidth), bottom);
            }
        }
        invalidate();
    }

    public Sticker getStickerByTag(String tag) {
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null && sticker.getTag() != null) {
                if (tag.equals(sticker.getTag()))
                    return sticker;
            }
        }
        return null;
    }

    public boolean deleteStickerByTag(String tag) {
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null && sticker.getTag() != null) {
                if (tag.equals(sticker.getTag()))
                    remove(sticker);
                return true;
            }
        }
        return false;
    }

    public Sticker getHandlignSticker() {
        return handlingSticker;
    }

    public void setHandlingSticker(String tag) {
        handlingSticker = getStickerByTag(tag);
        invalidate();
    }

    public void setOnStickerOperationListener(OnStickerOperationListener onStickerOperationListener) {
        this.onStickerOperationListener = onStickerOperationListener;
    }

    public OnStickerOperationListener getOnStickerOperationListener() {
        return onStickerOperationListener;
    }

    public void setSimpleTouchlistener(SimpleTouchListener simpleTouchlistener) {
        this.mGestureListener = simpleTouchlistener;
    }

    public Sticker getCurrentSticker() {
        return handlingSticker;
    }

    public List<BitmapStickerIcon> getIcons() {
        return icons;
    }

    public void setIcons(List<BitmapStickerIcon> icons) {
        this.icons = icons;
        invalidate();
    }

    public interface OnStickerOperationListener {
        void onStickerClicked(Sticker sticker);

        void onStickerViewClicked(float x, float y);

        void onStickerDeleted(Sticker sticker);

        void onStickerDragFinished(Sticker sticker);

        void onStickerZoomFinished(Sticker sticker);

        void onStickerRotateFinished(Sticker sticker);

        void onStickerFlipped(Sticker sticker);

        void onStickerDoubleTapped(Sticker sticker);

        void touchedDown(float x, float y, Boolean overAnySticker);

        void touchedUp();
    }

    public RealmList<ProjectItem> getProjectItemList() {
        RealmList<ProjectItem> list = new RealmList<ProjectItem>();
        for (int i = 0; i < stickers.size(); i++) {
            Sticker sticker = stickers.get(i);
            if (sticker != null) {
                ProjectItem item = sticker.getProjectItem();
                if (list != null)
                    list.add(item);
            }
        }
        return list;
    }

    public void setProjectItemList(Context context, RealmList<ProjectItem> list) {
        if (list == null)
            return;

        removeAllStickers();
        for (int i = 0; i < list.size(); i++) {
            ProjectItem item = list.get(i);
            Sticker sticker = StickerFactory.getSticker(context, Utils.getToolType(item.getItemType()), RealmHelper.getListPointF(item.getDrawingPoints()));
            sticker.setProjectItem(item);
            stickers.add(sticker);
            // this.addSticker(sticker, RealmHelper.getFrameF(item.getFrame()));
        }
        handlingSticker = null;
        invalidate();
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return true;
    }


    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        BaseInputConnection fic = new BaseInputConnection(this, false);
        outAttrs.actionLabel = null;
        outAttrs.inputType = InputType.TYPE_NULL;
//        outAttrs.imeOptions = EditorInfo.IME_ACTION_;
        return fic;
    }

    String mText;

    public void initInputText() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        mText = "";
        setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {

                    try {
                        EditTextSticker et = (EditTextSticker) handlingSticker;
                        if (keyCode == KeyEvent.KEYCODE_DEL) {
                            et.deleteLastCharacter();
                            invalidate();
                            return true;
                        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                            Toast.makeText(getContext(), "The text is: " + mText, Toast.LENGTH_LONG).show();
                            dismissEditText();
                            return true;
                        } else if (keyCode == KeyEvent.KEYCODE_ENTER) {
                            et.addCharacter('\n');
                            invalidate();
                            return true;
                        } else
//                        ((keyCode >= KeyEvent.KEYCODE_A) && (keyCode <= KeyEvent.KEYCODE_Z))
                        {
                            et.addCharacter((char) event.getUnicodeChar());
                            invalidate();
                            return true;
                        }

                    } catch (Exception e) {
                    }
                } else if (event.getAction() == KeyEvent.ACTION_MULTIPLE && event.getKeyCode() == KeyEvent.KEYCODE_UNKNOWN) {

                    try {
                        EditTextSticker et = (EditTextSticker) handlingSticker;
                        et.addString(event.getCharacters());
                        invalidate();
                        return true;
                    } catch (Exception e) {
                    }
                }
                return false;
            }
        });
    }

    public void editText() {
        initInputText();
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
//        mSearchEditText.requestFocus();
//        imm.showSoftInput(this, InputMethodManager.SHOW_FORCED);
//        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
if(imm != null) {
    imm.toggleSoftInputFromWindow(this.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT,
            0);
//    imm.toggleSoftInputFromWindow(this.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT,
//            0);
}
//        ((EditTextSticker)handlingSticker).edittingMode(true);
//        invalidate();
    }

    public void dismissEditText() {
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT);
        setOnKeyListener(null);
//        ((EditTextSticker)handlingSticker).edittingMode(false);
//        invalidate();

    }

    public String getText() {
        return mText;
    }


    //--------New stuff for edittext--------//
    MyEditText et = null;

    public void addEditText(Context context, final ViewGroup parent, EditTextSticker sticker) {

      /*  try {
            if (et != null)
                parent.removeView(et);
            et = null;
        } catch (Exception e) {
        }
        et = new MyEditText(context);
        if (sticker != null) {
            String initialText = sticker.getText();
            et.setText(initialText);
        }
        updateEdittextEditor(sticker);
        parent.addView(et);
        et.beginBatchEdit();
        ViewTreeObserver viewTreeObserver = et.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    et.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = parent.getHeight();
                    int heigtMenu = et.getHeight();
                    et.beginBatchEdit();
                    et.isFocused();
                    et.requestFocus();
//                    ObjectAnimator.ofFloat(et, "y", parent.getHeight(), (parent.getHeight() - et.getHeight())).setDuration(300).start();
                }
            });
        }*/
    }

    private void updateEdittextEditor(EditTextSticker sticker) {
        if (et != null) {
            try {
                if (sticker != null) {
                    float[] bitmapPoints = getStickerPoints(sticker);
                    float x1 = bitmapPoints[0];
                    float y1 = bitmapPoints[1];
                    float x2 = bitmapPoints[2];
                    float y2 = bitmapPoints[3];
                    float x3 = bitmapPoints[4];
                    float y3 = bitmapPoints[5];
                    float x4 = bitmapPoints[6];
                    float y4 = bitmapPoints[7];
                    float cx = (x1 + x2 + x3 + x4) / 4f;
                    float cy = (y1 + y2 + y3 + y4) / 4f;
                    int width = (int) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
                    int height = (int) Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
                    float rotation = calculateRotation(x4, y4, x3, y3);

                    float x = cx - width / 2;
                    float y = cy - height / 2;
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(width, height);
                    et.setLayoutParams(params);
                    et.setRotation(rotation);
                    et.setX(x);
                    et.setY(y);
                    changeTextColorAndSize(sticker);
                } else {
                    int width = 300;
                    int height = 300;
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(width, height);
                    et.setLayoutParams(params);
                    et.setX(getWidth() / 2 - width / 2);
                    et.setY(getHeight() / 2 - height / 2);
                }
            } catch (Exception e) {
            }
        }
    }

    public void rotateEdittext(float rotation) {
        if (et != null) {
            try {
                et.setRotation(rotation);
            } catch (Exception e) {
            }
        }
    }

    public void resizeEdittext(EditTextSticker sticker) {
        if (sticker != null) {
            try {
                float[] bitmapPoints = getStickerPoints(sticker);
                float x1 = bitmapPoints[0];
                float y1 = bitmapPoints[1];
                float x2 = bitmapPoints[2];
                float y2 = bitmapPoints[3];
                float x3 = bitmapPoints[4];
                float y3 = bitmapPoints[5];
                float x4 = bitmapPoints[6];
                float y4 = bitmapPoints[7];
                float cx = (x1 + x2 + x3 + x4) / 4f;
                float cy = (y1 + y2 + y3 + y4) / 4f;
                int width = (int) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
                int height = (int) Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
                float x = cx - width / 2;
                float y = cy - height / 2;
                et.setX(x);
                et.setY(y);
//                et.requestLayout();
            } catch (Exception e) {
            }
        }
    }

    public void moveEdittext(EditTextSticker sticker) {
        if (sticker != null) {
            try {
                float[] bitmapPoints = getStickerPoints(sticker);
                float x1 = bitmapPoints[0];
                float y1 = bitmapPoints[1];
                float x2 = bitmapPoints[2];
                float y2 = bitmapPoints[3];
                float x3 = bitmapPoints[4];
                float y3 = bitmapPoints[5];
                float x4 = bitmapPoints[6];
                float y4 = bitmapPoints[7];
                float cx = (x1 + x2 + x3 + x4) / 4f;
                float cy = (y1 + y2 + y3 + y4) / 4f;
                int width = (int) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
                int height = (int) Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
                et.getLayoutParams().width = width;
                et.getLayoutParams().height = height;
                et.requestLayout();
            } catch (Exception e) {
            }
        }
    }

    public void changeTextColorAndSize(EditTextSticker sticker) {
        if (et != null) {
            try {
                et.setTextSize(sticker.getTextSizeScaled());
                et.setTextSize(sticker.getTextColor());
            } catch (Exception e) {
            }
        }
    }

    public void removeEditText(ViewGroup parent) {
        try {
            if (et != null) {
                parent.removeView(et);
                et.endBatchEdit();
            }
        } catch (Exception e) {
        }
    }
/*

    int mTextExtractionToken;
    class InputConnection extends BaseInputConnection {
        public InputConnection(PasscodeInputWidget editor) {
            super(editor, true);
            getEditable().append(getPasscode());
        }

        private void updateInputManager() {
            String content = getPasscode();
            int length = content.length();
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            PasscodeInputWidget passcodeWidget = PasscodeInputWidget.this;
            inputManager.updateSelection(passcodeWidget, length, length, length, length);
            if(mTextExtractionToken != -1) {
                ExtractedText extrText = createEmptyExtractedText();
                extrText.text = content;
                extrText.selectionEnd = extrText.selectionStart = content.length();
                inputManager.updateExtractedText(passcodeWidget, mTextExtractionToken, extrText);
            }
        }

        @Override
        public boolean beginBatchEdit() {
            synchronized(this) {
                ++mBatchLevel;
            }
            return true;
        }

        @Override
        public boolean endBatchEdit() {
            synchronized(this) {
                boolean batchEnded = 0 == mBatchLevel || 0 == --mBatchLevel;

                if(batchEnded) {
                    Editable editable = getEditable();
//                    if(!TextUtils.equals(getPasscode(), editable)) {
//                        setPasscode(editable.toString());
//                    }
                }

                return !batchEnded;
            }
        }

        protected void reportFinish() {
            synchronized(this) {
                while(mBatchLevel > 0) {
                    endBatchEdit();
                }
            }
        }

        @Override
        public ExtractedText getExtractedText(ExtractedTextRequest request, int flags) {
            ExtractedText extrText = createEmptyExtractedText();
            if(request.hintMaxLines > 0) {
                String content = getPasscode();
                extrText.text = content.substring(0, request.hintMaxChars);
                extrText.selectionEnd = extrText.selectionStart = extrText.text.length();
            }
            StickerView.this.mTextExtractionToken = (GET_EXTRACTED_TEXT_MONITOR == flags) ? request.token : -1;
            return extrText;
        }

        @Override
        public CharSequence getTextAfterCursor(int length, int flags) {
            return "";
        }
        @Override
        public CharSequence getTextBeforeCursor(int length, int flags) {
            return getPasscode();
        }
        @Override
        public boolean setSelection(int start, int end) {
            int atEnd = getPasscode().length();
            return super.setSelection(atEnd, atEnd);
        }
    };

    private static String getPasscode(){
        return "this is test passcode";
    }
    private static ExtractedText createEmptyExtractedText() {
        ExtractedText text = new ExtractedText();
        text.startOffset = 0;
        text.partialEndOffset = text.partialStartOffset = -1;
        return text;
    }
*/


}
