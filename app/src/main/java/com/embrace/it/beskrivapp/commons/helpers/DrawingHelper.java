package com.embrace.it.beskrivapp.commons.helpers;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.observables.BrightnessToolObservable;
import com.embrace.it.beskrivapp.commons.observables.CropToolObservable;
import com.embrace.it.beskrivapp.commons.observables.EventBus;
import com.embrace.it.beskrivapp.commons.observables.ToolMenuSubButtonObservable;
import com.embrace.it.beskrivapp.components.tools.ToolMenuSubButton;
import com.embrace.it.beskrivapp.components.tools.ToolMenu;
import com.embrace.it.beskrivapp.sticker.BitmapStickerIcon;
import com.embrace.it.beskrivapp.sticker.DeleteIconEvent;
import com.embrace.it.beskrivapp.sticker.RotateIconEvent;
import com.embrace.it.beskrivapp.sticker.Sticker;
import com.embrace.it.beskrivapp.sticker.StickerFactory;
import com.embrace.it.beskrivapp.sticker.StickerView;
import com.embrace.it.beskrivapp.sticker.TextSticker;
import com.embrace.it.beskrivapp.sticker.ZoomIconEvent;
import com.embrace.it.beskrivapp.sticker.toolframs.MainImageSticker;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolDataFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by waseem on 2/16/17.
 */

public class DrawingHelper implements Observer {

    public static int brightnessBgImage = 50;//50 = 0;  0 = -255, 100 = 255
    public static int contrastBgImage = 50; //50 = 5; 0 = 0; 100 = 10;
    private  int brightnessBgImagePrev = 0;
    private  float contrastBgImagePrev = 1;

   public MainImageSticker mainSticker;

    public DrawingHelper(StickerView stickerview, DrawingHelperCallback delegate) {
        this.stickerView = stickerview;
        this.delegate = delegate;
        EventBus.getToolMenuSubButtonObserver().addObserver(this);
        EventBus.getBrightnessToolObserver().addObserver(this);
        EventBus.getCropToolObserver().addObserver(this);
    }

    public void initBrightnessContrast(int b, float c){

        float fraction = 510f/(b + 255f);
        brightnessBgImage = (int)(100f/fraction);

        fraction = 10f/c;
        contrastBgImage = (int)(100f/fraction);


        brightnessBgImagePrev = b;
         contrastBgImagePrev = c;
    }
    //----------Bootom Menu---------//
    private ToolMenu menu;
    private Sticker stickerSelected;
    private StickerView stickerView;
    private DrawingHelperCallback delegate;

    public interface DrawingHelperCallback {
        void showDrawingPad(Constants.ToolType tooltype);

        void setBackgorundImageEdittingMode(Constants.ToolType tooltype);

        void dismissBackgroundImageEdittingMode();

        void updateBrightnessContrast(int brightness, float contrast, int brightnessNet, float contrastNet,int mainIndex);

        void updateTitle(String title);

        void setCrop();

        void undoCrop();

        void brightnessToolMenuChanged(int mainIndex,int brightness,float contrast);
    }

    public void addToolMenu(final ViewGroup parent, Context context, Sticker sticker, ToolData data, Constants.ToolType tooltype) {
        stickerSelected = sticker;
        try {
            if (menu != null)
                parent.removeView(menu);
        } catch (Exception e) {
        }

        delegate.updateTitle(data.getActiveText());
        menu = new ToolMenu(context, data, tooltype);//AnimatingUp from Bottom View
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        menu.setLayoutParams(params);
        menu.setBackgroundColor(Color.TRANSPARENT);
        parent.addView(menu);
        ViewTreeObserver viewTreeObserver = menu.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    menu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = parent.getHeight();
                    int heigtMenu = menu.getHeight();
                    ObjectAnimator.ofFloat(menu, "y", parent.getHeight(), (parent.getHeight() - menu.getHeight())).setDuration(300).start();
                }
            });
        }
    }

    public void removeToolMenu(ViewGroup parent) {
        if (menu != null)
            ObjectAnimator.ofFloat(menu, "y", menu.getY(), parent.getHeight()).setDuration(200).start();

    }

    //---------FrameTool-----------//
    public void handleAddNewTool(Context context, ViewGroup containerView, StickerView stickerview, Constants.ToolType tooltype,int x,int y) {
        switch (tooltype) {
            case Crop_Tool:
                if (delegate != null)
                    delegate.setBackgorundImageEdittingMode(Constants.ToolType.Crop_Tool);
                addBrightnessTool(context, containerView, stickerview, tooltype);
                break;
            case Brightness_Tool:
                if (delegate != null)
                    delegate.setBackgorundImageEdittingMode(Constants.ToolType.Brightness_Tool);

                initBrightnessContrast(mainSticker.getBrightness(),mainSticker.getContrast());
                addBrightnessTool(context, containerView, stickerview, tooltype);
                break;
            case Draw_Area:
            case Draw_Tool:
            case Count_Tool:
            case Angle_Tool:
            case Brackets_Tool:
                if (delegate != null)
                    delegate.showDrawingPad(tooltype);
                break;
            default:
                addToolItem(context, containerView, stickerview, tooltype,x,y);

        }
    }

    public void addBrightnessTool(Context context, ViewGroup containerView, StickerView stickerview, Constants.ToolType tooltype) {
        ToolData data = ToolDataFactory.getToolData(tooltype);
        addToolMenu(containerView, context, null, data, tooltype);
    }

    public void addToolItem(Context context, ViewGroup containerView, StickerView stickerview, Constants.ToolType tooltype,int x,int y) {
        addToolItem(context,containerView,stickerview,tooltype,null,x,y);

    }

    public void addToolItem(Context context, ViewGroup containerView, StickerView stickerview, Constants.ToolType tooltype, String resourcePath,int x,int y) {
        final Sticker sticker = StickerFactory.getSticker(context, tooltype, resourcePath);
        stickerview.addSticker(sticker,x,y);
        addToolMenu(containerView, context, sticker, sticker.getData(), tooltype);
    }

    public void addToolItem(Context context, ViewGroup containerView, StickerView stickerview, List<PointF> drawignPoitns, Constants.ToolType tooltype) {
        final Sticker sticker = StickerFactory.getSticker(context, tooltype, drawignPoitns);
        stickerview.addSticker(sticker, sticker.getInitialFrame());
        addToolMenu(containerView, context, sticker, sticker.getData(), tooltype);
    }

    public void addTextSticker(Context context, StickerView parentview) {
        final TextSticker sticker = (TextSticker) StickerFactory.getSticker(context, Constants.ToolType.Text_Area_Tool, null);
        parentview.addSticker(sticker);
    }

    public void initStickerView(Context context, StickerView stickerview) {
        stickerview.setBackgroundColor(Color.TRANSPARENT);
        stickerview.setLocked(false);
        stickerview.setConstrained(true);

        //currently you can config your own icons and icon event
        //the event you can custom
        BitmapStickerIcon deleteIcon = new BitmapStickerIcon(
                ContextCompat.getDrawable(context, R.drawable.cancel),
                BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());
        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(
                ContextCompat.getDrawable(context, R.drawable.expand_arrow),
                BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());
        BitmapStickerIcon flipIcon = new BitmapStickerIcon(
                ContextCompat.getDrawable(context, R.drawable.double_arrow),
                BitmapStickerIcon.RIGHT_TOP);
        flipIcon.setIconEvent(new RotateIconEvent());

        stickerview.setIcons(Arrays.asList(deleteIcon, zoomIcon, flipIcon));
    }


    /*
    called when an item of the ToolBarItemMenu is clicked.
     */
    @Override
    public void update(Observable o, Object arg) {
        try {
            if (o instanceof ToolMenuSubButtonObservable) {
                if (arg instanceof ToolMenuSubButton) {
                    ToolMenuSubButton item = (ToolMenuSubButton) arg;
                    int mainIndex = item.mainIndex;
                    int childIndex = item.childIndex;
                    if(childIndex < 0){
                        delegate.updateTitle(stickerSelected.getInstructions(mainIndex,childIndex));
                    }else {
                        stickerSelected.itemUpdated(mainIndex, childIndex);
                        stickerView.invalidate();
                    }
                }else if (arg instanceof Bundle){
                    Bundle b = (Bundle)arg;
                    String key = b.getString("key");
                    if(key.equals("ToolMenu")){
                        delegate.updateTitle(stickerSelected.getData().getActiveText());
                    }
                }
            } else if (o instanceof BrightnessToolObservable) {
                Bundle b = (Bundle) arg;
                String value = b.getString("value",null);
                String action = b.getString(Constants.ToolMenu_Action,null);


                if(action != null  && action.equals(Constants.ToolMenu_UpdateTitle) && value != null){
                    delegate.updateTitle(value);
                    if( b.get(Constants.Brightness_Main_Index) != null){
                        int mainIndex = (int) b.get(Constants.Brightness_Main_Index);
                        delegate.brightnessToolMenuChanged(mainIndex,calcualteBrightness(),calcualteContrast());
                    }

                }else {
                    int mainIndex = (int) b.get(Constants.Brightness_Main_Index);
                    int progress = (int) b.get(Constants.Brightness_Progess);
                    if (mainIndex == 0) {
                        int brightnessPrev = calcualteBrightness();
                        brightnessBgImage = progress;
                        int brightness = calcualteBrightness();
                        if (delegate != null) {
                            delegate.updateBrightnessContrast(brightness, calcualteContrast(), brightness - brightnessPrev, 0,0);
                        }
                    } else if (mainIndex == 1) {
                        float contrastPrev = calcualteContrast();
                        contrastBgImage = progress;
                        float contrast = calcualteContrast();
                        if (delegate != null) {
                            delegate.updateBrightnessContrast(calcualteBrightness(), contrast, 0, contrast - contrastPrev,1);
                        }
                    } else {
                        if (delegate != null) {
                            delegate.updateBrightnessContrast(brightnessBgImagePrev, contrastBgImagePrev, 0, 0,2); //reset to default
                            initBrightnessContrast(brightnessBgImagePrev, contrastBgImagePrev);
                            delegate.dismissBackgroundImageEdittingMode();
                            delegate.updateTitle(MainApplication.getContext().getResources().getString(R.string.Hold_fingeren_inden_for_at_åbne_menuen));
                        }
                    }
                }
            } else if (o instanceof CropToolObservable) {
                Bundle b = (Bundle) arg;
                int mainIndex = (int) b.get(Constants.Crop_Main_Index);
                if (mainIndex == 0) {
                    if (delegate != null)
                        delegate.setCrop();
                } else {
                    if (delegate != null)
                        delegate.undoCrop();
                }
            }


        } catch (Exception e) {
            Log.v("",e.getMessage());
        }
    }


    public int calcualteBrightness() {

        float fraction = 100f / (float) brightnessBgImage;
        float value = ((510f / fraction) - 255f);
//        brightnessBgImageNet  = (int)value - brightnessBgImageNet;
        return (int) value;
    }

    public float calcualteContrast() {
        float fraction = 100f / (float) contrastBgImage;
        float value = (10f / fraction);
//        contrastBgImageNet  = (int)value - contrastBgImageNet;
        return  value;
    }


}
