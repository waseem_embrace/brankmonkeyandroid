package com.embrace.it.beskrivapp.database;

import io.realm.RealmObject;

/**
 * Created by waseem on 2/9/17.
 */

public class ProjectMainItem extends RealmObject {

    public ProjectMainItem() {
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ProjectItem getItem() {
        return item;
    }

    public void setItem(ProjectItem item) {
        this.item = item;
    }

    private String imagePath;
    private ProjectItem item;



}
