package com.embrace.it.beskrivapp.sticker.toolframs;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.DrawableSticker;
import com.embrace.it.beskrivapp.sticker.StickerUtils;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

import java.util.List;

/**
 * @author wupanjie
 */
public class CountSticker extends DrawableSticker {

    //    private int imageIndex;
    private int colorIndex;
    private int textSizeIndex;
    private int textTypeIndex;
    private List<PointF> drawingPoints;

    public CountSticker(Drawable drawable) {
        super(drawable);
        colorIndex = 0;
        textTypeIndex = 0;
        textSizeIndex = 3;
        setUserIntractions(true, true, false);
//        typeface = Utils.getFontTypeface(MainApplication.getContext(), Constants.FONT_Heiti_TC);
    }

    public CountSticker(List<PointF> drawingPoints, ToolData data) {
        super();
        colorIndex = 5;
        textTypeIndex = 0;
        textSizeIndex = 3;
//        typeface = Utils.getFontTypeface(MainApplication.getContext(), Constants.FONT_Heiti_TC);
        this.drawingPoints = drawingPoints;
        setUserIntractions(true, true, false);
        if (data != null) {
            setData(data);
            init();
        }
    }

    @Override
    public void itemUpdated(int mainIndex, int childIndex) {
        if( mainIndex < 0 && childIndex < 0) {
            mainIndex = 0;
            childIndex = 0;
        }
        ToolData data = getData();
        if (data != null) {
            switch (mainIndex) {
                case 0:
                    this.colorIndex = childIndex;
                    Drawable drawable = getDrawable();
                    if (drawable == null)
                        drawable = getNewDrawable();
                    drawable.setTint(getMyColor());
                    setDrawable(drawable);
                    break;
                case 1:

                    this.textSizeIndex = childIndex;
                    Drawable drawable1 = getNewDrawable();
                    drawable1.setTint(getMyColor());
                    setDrawable(drawable1);
                    break;

                case 2:
                    this.textTypeIndex = childIndex;
                    Drawable drawable2 = getNewDrawable();
                    drawable2.setTint(getMyColor());
                    setDrawable(drawable2);
                    break;
            }
        }
    }

    float circleRadius = Utils.convertDpToPx(25);

    public RectF getBound() {
        if (drawingPoints != null)
            return StickerUtils.calculateBounds(drawingPoints, circleRadius);
        else return null;
    }


    //Used only the first time the sticker is being created when its drawn by user.
    private RectF firstTimeCalculatedFram;

    @Override
    public RectF getInitialFrame() {
        return firstTimeCalculatedFram;
    }

    private Drawable getNewDrawable() {
        RectF rect = StickerUtils.calculateBounds(drawingPoints, circleRadius); //rect = left,top,width,height
        RectF rectWithMargins = new RectF(rect.left - marginImage, (rect.top - marginImage), rect.left + rect.right + marginImage, (int) (rect.top + rect.bottom + marginImage));
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap((int) (rect.left + rect.right + marginImage), (int) (rect.top + rect.bottom + marginImage), conf); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bmp);
        initPaint();
        drawCanvas(canvas, this.drawingPoints);
        Bitmap resizedBitmap = getCroppedBitmap(bmp, rectWithMargins);
        Drawable drawable = new BitmapDrawable(MainApplication.getContext().getResources(), resizedBitmap);
        firstTimeCalculatedFram = rectWithMargins;
        return drawable;
    }

    private int getMyColor() {
        ToolItemData icons = data.getEditTools().get(0);
        return ContextCompat.getColor(MainApplication.getContext(), icons.getToolItemData().get(colorIndex));
    }

    private int getTextSize() {
        ToolItemData icons = data.getEditTools().get(1);
        return Utils.convertDpToPx(icons.getToolItemData().get(textSizeIndex));
    }

    private void initPaint() {
        int type = data.getEditTools().get(2).getToolItemData().get(textTypeIndex);

        textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(getMyColor());
        textPaint.setTextSize(getTextSize());
        if(textTypeIndex == 1 && typefaceHeiti != null)
            textPaint.setTypeface(typefaceHeiti);
        else if(textTypeIndex == 2 && typefaceVerdana != null)
            textPaint.setTypeface(typefaceVerdana);
    }

    TextPaint textPaint;
    Typeface typefaceHeiti;
    Typeface typefaceVerdana;

    void drawCanvas(Canvas canvas, List<PointF> circlePoints) {
        for (int i = 0; i < circlePoints.size(); i++) {
            PointF p = circlePoints.get(i);
            Rect bounds = new Rect();

            switch (textTypeIndex) {
                case 0:
                    String text = "" + (i + 1) + "";
                    textPaint.getTextBounds(text, 0, text.length(), bounds);
                    int height = bounds.height();
                    int width = bounds.width();
                    canvas.drawText(text, p.x - (width/2), p.y + (height/2), textPaint);

                    break;
                case 1:
                     text = alphabet[i];
                    textPaint.getTextBounds(text, 0, text.length(), bounds);
                     height = bounds.height();
                     width = bounds.width();
                    canvas.drawText(text, p.x - (width/2), p.y + (height/2), textPaint);
                    break;
                case 2:
                     text = roman[i];
                    textPaint.getTextBounds(text, 0, text.length(), bounds);
                     height = bounds.height();
                     width = bounds.width();
                    canvas.drawText(text, p.x - (width/2), p.y + (height/2), textPaint);
                    break;
                default:
                    break;
            }
        }
    }

    void drawCanvasTest(Canvas canvas, List<PointF> circlePoints) {
        String text = "Test1 text2 test3 test4 test5 test6 test7";
        PointF p = circlePoints.get(0);
        canvas.drawText(text, p.x, p.y, textPaint);
    }

    float marginImage = 10;

    public Bitmap getResizedImage(Bitmap bitmap, List<PointF> circlePoints, float circleRadius, RectF rect) {
        float left = (rect.left - marginImage) < 0 ? 0 : rect.left - marginImage;
        float top = (rect.top - marginImage) < 0 ? 0 : rect.top - marginImage;
        float right = (rect.right + marginImage) > bitmap.getWidth() ? bitmap.getWidth() : rect.right + marginImage;
        float bottom = (rect.bottom + marginImage) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom + marginImage;

        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) right, (int) bottom);
        return resizedbitmap;
    }

    private Bitmap getCroppedBitmap(Bitmap bitmap, RectF rect) {
        float left = (rect.left) < 0 ? 0 : rect.left;
        float top = (rect.top) < 0 ? 0 : rect.top;
        float right = (rect.right) > bitmap.getWidth() ? bitmap.getWidth() : rect.right;
        float bottom = (rect.bottom) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom;

        //if rect is updated then, update the object
        rect.left = left;
        rect.right = right;
        rect.top = top;
        rect.bottom = bottom;
        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) (right - left), (int) (bottom - top));
        return resizedbitmap;
    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }

    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);
        projectItem.setItemType(toolType.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(this.colorIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.textSizeIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.textTypeIndex));

        if (getFrame() != null) {
            projectItem.setFrame(getFrame());
        }

        projectItem.setDrawingPoints(drawingPoints);
        return projectItem;
    }

    @Override
    public void setProjectItem(ProjectItem p) {

        toolType = Utils.getToolType(p.getItemType());

        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        this.colorIndex = (int)characterstics[0];
        this.textSizeIndex = (int)characterstics[1];
        this.textTypeIndex = (int)characterstics[2];

        if (p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
            setFrame(fram);
        }
        drawingPoints = RealmHelper.getListPointF(p.getDrawingPoints());

        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);
        itemUpdated(1, this.textSizeIndex);

    }

    private final String[] roman = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", "XXI", "XXII", "XXIII", "XXIV", "XXV", "XXVI"};
    private final String[] alphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
}
