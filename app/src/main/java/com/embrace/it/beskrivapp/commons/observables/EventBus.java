package com.embrace.it.beskrivapp.commons.observables;

/**
 * Created by waseem on 2/21/17.
 */

public class EventBus {

    private static ToolMenuSubButtonObservable mToolbaritemObservable;
    private static BrightnessToolObservable mBrightnessToolObservable;
    private static CropToolObservable mCropToolObservable;
    private static ThumbnailSavedObservable mThumbnailSavedObservable;

    public static ToolMenuSubButtonObservable getToolMenuSubButtonObserver(){
        if(mToolbaritemObservable == null)
            mToolbaritemObservable = new ToolMenuSubButtonObservable();
        return mToolbaritemObservable;
    }

    public static CropToolObservable getCropToolObserver(){
        if(mCropToolObservable == null)
            mCropToolObservable = new CropToolObservable();
        return mCropToolObservable;
    }
    public static BrightnessToolObservable getBrightnessToolObserver(){
        if(mBrightnessToolObservable == null)
            mBrightnessToolObservable = new BrightnessToolObservable();
        return mBrightnessToolObservable;
    }

    public static ThumbnailSavedObservable getThumbnailSavedObserver(){
        if(mThumbnailSavedObservable == null)
            mThumbnailSavedObservable = new ThumbnailSavedObservable();
        return mThumbnailSavedObservable;
    }
}
