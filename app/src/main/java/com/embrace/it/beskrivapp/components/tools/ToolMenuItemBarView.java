package com.embrace.it.beskrivapp.components.tools;

/**
 * Created by wasimshigri on 1/30/17.
 */

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.commons.observables.EventBus;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by wasimshigri on 1/25/17.
 */

public class ToolMenuItemBarView extends ViewGroup implements View.OnClickListener {

    public ToolMenuItemBarView(Context context, List<Integer> data, int mainIndex, int positionInPager) {
        super(context);
        this.datalist = data;
        this.mainIndex = mainIndex;
        this.positionInPager = positionInPager;
        calculateContainerDimensions();
        setBackgroundColor(Color.parseColor("#00000000"));
        addChildViews(context, data.size());
    }

//    public void refreshViews(float x, float y){
//        removeAllViews();
//        addChildViews(this.getContext(),datalist.size());
//        calculateContainerDimensions();
//    }

    public ToolMenuItemBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolMenuItemBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ToolMenuItemBarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        for (int i = 0; i < getChildCount(); i++) {
            mTmpChildRect.set(0, 0, widthChild, heightChild);
            final ToolMenuSubButton child = (ToolMenuSubButton) getChildAt(i);
            mTmpChildRect = getChildRectAtPosition(child.itemLocation);
            child.layout(mTmpChildRect.left, mTmpChildRect.top,
                    mTmpChildRect.right, mTmpChildRect.bottom);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(widthParent, widthSize);
        } else {
            width = widthParent;
        }

        int height;
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(heightParent, heightSize);
        } else {
            height = heightParent;
        }

        int maxHeight = 0;
        int maxWidth = 0;
        int childState = 0;
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void measureChildWithMargins(View child, int parentWidthMeasureSpec, int widthUsed, int parentHeightMeasureSpec, int heightUsed) {
        super.measureChildWithMargins(child, parentWidthMeasureSpec, widthUsed, parentHeightMeasureSpec, heightUsed);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
//        animations();

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private List<ToolMenuSubButton> buttons = null;

    //-----------------//
    void addChildViews(Context context, int childCount) {
        int i = 1;
        buttons = new ArrayList<ToolMenuSubButton>();
        for (; i <= childCount; i++) {
            ToolMenuSubButton btn = ToolMenuSubButton.getNewInstance(context);
            btn.itemLocation = i - 1;
            btn.setImageResource(this.datalist.get(i - 1));
            btn.mainIndex = mainIndex;
            btn.childIndex = positionInPager * 5 + (i - 1);
            btn.setBackgroundColor(Color.parseColor("#00000000"));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(widthChild, heightChild);
            params.setMargins(paddingLeft, paddingTop, paddingRight, paddingBottom);
            btn.setLayoutParams(params);
            btn.setOnClickListener(this);
            addView(btn);
            buttons.add(btn);

        }

    }


    //---------_Variables----------------//
    private final Rect mTmpContainerRect = new Rect();
    private Rect mTmpChildRect = new Rect();
    private int widthChild = 0;
    private int heightChild = 100;
    private int widthParent = 0;
    private int heightParent = 0;
    private ShapeDrawable mDrawable;
    private int paddingLeft = 15;
    private int paddingRight = 15;
    private int paddingTop = 15;
    private int paddingBottom = 15;
    private List<Integer> datalist;
    private int mainIndex;
    private int positionInPager;

    void calculateContainerDimensions() {
        heightChild = Utils.convertDpToPx(50);
        widthParent = Utils.getScreenWidth();
        widthChild = (int) (widthParent / 5f) - paddingLeft - paddingRight;
        heightParent = heightChild + paddingTop + paddingBottom;
    }

    @Override
    public void onClick(View v) {
        if (v instanceof ToolMenuSubButton) {
            EventBus.getToolMenuSubButtonObserver().notifyItemClicked((ToolMenuSubButton) v);
        }
    }

    private Rect getChildRectAtPosition(int position) {
        int left = widthChild * position + paddingLeft + paddingLeft * position + paddingRight + position;
        int top = paddingTop;
        int right = left + widthChild;
        int bottom = top + heightChild;
        Rect rect = new Rect();
        rect.set(left, top, right, bottom);
        return rect;
    }


    public void animateUpButtons() {

        int ANIM_DURATION = 500;
        int ANIM_DELAY_BTWEEN = 100;
        int animCount = 0;
        AnimatorSet parentAnimSet = new AnimatorSet();

        for (int i = 0; i < buttons.size(); i++) {
            final View v = buttons.get(i);
            v.clearAnimation();

            final float xTo = v.getX();
            final float yTo = v.getY();
            final float xFrom = xTo;
            final float yFrom = yTo + heightChild;
            if (xTo == xFrom && yTo == yFrom)
                return;

            v.setVisibility(View.INVISIBLE);
            ObjectAnimator animX = ObjectAnimator.ofFloat(v, "x", xFrom, xTo);
            ObjectAnimator animY = ObjectAnimator.ofFloat(v, "y", yFrom, yTo);
            ObjectAnimator animScaleX = ObjectAnimator.ofFloat(v, "scaleX", 0.1f, 1.0f);
            ObjectAnimator animScaleY = ObjectAnimator.ofFloat(v, "scaleY", 0.1f, 1.0f);
            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(ANIM_DURATION);
            animSet.playTogether(animX, animY, animScaleX, animScaleY);
            parentAnimSet.play(animSet).after(ANIM_DELAY_BTWEEN * animCount);
            animSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    v.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            animCount++;
        }

        parentAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        parentAnimSet.start();
    }

    public void animateDownButtons() {

        int ANIM_DURATION = 500;
        int ANIM_DELAY_BTWEEN = 100;
        int animCount = 0;
        AnimatorSet parentAnimSet = new AnimatorSet();

        for (int i = 0; i < buttons.size(); i++) {
            final View v = buttons.get(i);
            v.clearAnimation();

            final float xTo = v.getX();
            final float yTo = v.getY();
            final float xFrom = xTo;
            final float yFrom = yTo + heightChild;
            if (xTo == xFrom && yTo == yFrom)
                return;

            v.setVisibility(View.INVISIBLE);
            ObjectAnimator animX = ObjectAnimator.ofFloat(v, "x", xFrom, xTo);
            ObjectAnimator animY = ObjectAnimator.ofFloat(v, "y", yFrom, yTo);
            ObjectAnimator animScaleX = ObjectAnimator.ofFloat(v, "scaleX", 0.1f, 1.0f);
            ObjectAnimator animScaleY = ObjectAnimator.ofFloat(v, "scaleY", 0.1f, 1.0f);
            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(ANIM_DURATION);
            animSet.playTogether(animX, animY, animScaleX, animScaleY);
            parentAnimSet.play(animSet).after(ANIM_DELAY_BTWEEN * animCount);
            animSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    v.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            animCount++;
        }

        parentAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        parentAnimSet.start();
    }
}
