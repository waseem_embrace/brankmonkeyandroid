package com.embrace.it.beskrivapp.commons;

/**
 * Created by wasimshigri on 1/26/17.
 */

public class Constants {


    public enum ButtonLocation {
        LOCATION_FIRST(1),
        LOCATION_SECOND(2),LOCATION_THIRD(3),LOCATION_FOURTH(4),LOCATION_FIFTH(5);
        private int value;
        private ButtonLocation(int value){this.value = value;}
        public int getValue(){return this.value;}
    }

    public enum ButtonGroupType{group0,group1,group2,group3,group4,group5};

    public enum ToolType {

        Crop_Tool(1),
        Brightness_Tool(2),
        Markings_Tool(3),
        Icon_Tool(4),
        Sign_Tool(5),
        Draw_Tool(6),
        Draw_Area(7),
        Shape_Tool(8),
        Brackets_Tool(9),
        Count_Tool(10),
        Angle_Tool(11),
        Text_Area_Tool(12),
        Symbol_Tool(13),

        Insert_Inage_Tool(14),
        Measure_Tool(15),

        Tool_Control(16),

        Main_Image(17),

        None(0);

        private int value;
        ToolType(int value){
            this.value = value;
        }

        public int getNumericValue(){
            return this.value;
        }

    }

    static public final String FONT_FrutigerLTStd_Bold = "FrutigerLTStd-Bold.otf";
    static public final String FONT_FrutigerLTStd_Light = "FrutigerLTStd-Light.otf";
    static public final String FONT_FrutigerLTStd_Black = "FrutigerLTStd-Black.otf";
    static public final String FONT_FrutigerLTStd_BlackCn = "FrutigerLTStd-BlackCn.otf";
    static public final String FONT_FrutigerLTStd_BlackItalic = "FrutigerLTStd-BlackItalic.otf";
    static public final String FONT_FrutigerLTStd_BoldCn = "FrutigerLTStd-BoldCn.otf";
    static public final String FONT_FrutigerLTStd_BoldItalitc = "FrutigerLTStd-BoldItalic.otf";
    static public final String FONT_FrutigerLTStd_Cn = "FrutigerLTStd-Cn.otf";
    static public final String FONT_FrutigerLTStd_ExtraBlackBCn = "FrutigerLTStd-ExtraBlackCn.otf";
    static public final String FONT_FrutigerLTStd_Italic = "FrutigerLTStd-Italic.otf";
    static public final String FONT_FrutigerLTStd_LightCn = "FrutigerLTStd-LightCn.otf";
    static public final String FONT_FrutigerLTStd_LightItalic = "FrutigerLTStd-LightItalic.otf";
    static public final String FONT_FrutigerLTStd_Roman = "FrutigerLTStd-Roman.otf";
    static public final String FONT_FrutigerLTStd_UltraBlack = "FrutigerLTStd-UltraBlack.otf";

    static public final String ifNoTitle = "Unavngivet";
    static public final String ifNoDescription = "Ingen beskrivelse";




    //------Tool Observer---------//

    static public final String  Brightness_Main_Index = "Brightness_Main_Index";
    static public final String  Brightness_Progess = "Brightness_Progess";
    static public final String  ToolMenu_UpdateTitle = "UpdateTitle";
    static public final String  ToolMenu_Action = "action";

    static public final String  Crop_Main_Index = "Crop_Main_Index";



    static final public String TAG_BG_IMAGE = "TAG_bgImage";

}
