package com.embrace.it.beskrivapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.activity.StartActivity;

/**
 * Created by waseem on 2/1/17.
 */

public class NewProjectCellViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {


    Context context;
    public NewProjectCellViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        itemView.setEnabled(true);
    }

    public void setValues(Context c){
        this.context = c;
    }
    @Override
    public void onClick(View v) {
        createNewProject();
    }


    public void createNewProject() {
        try {
            ((StartActivity) context).createNewProject();
        }catch (Exception e){
            ((MainActivity) context).createNewProject();
        }
    }
}
