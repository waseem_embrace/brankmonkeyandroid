package com.embrace.it.beskrivapp.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.components.tools.ToolMenu;

/**
 * Created by waseem on 1/31/17.
 */

public class MainFragment extends Fragment implements SimpleTouchListener.SimpleTouchListenerCallback {

    SimpleTouchListener touchListener;
    CardView bottomToolBarLayout;
    ViewGroup contentView; //Add Items in this View
    ToolMenu toolMenu;

    public void setCallback(MainFragmentCallBack callback){
        this.delegate = callback;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main,container,false);
        contentView = (ViewGroup) view.findViewById(R.id.fragment_main_rl_container);
        bottomToolBarLayout = (CardView)view.findViewById(R.id.fragment_main_bottom_tool_menu);
//        addToolMenu(container.getContext());
//        addFrame(container.getContext());
        touchListener = new SimpleTouchListener(container.getContext(), this);
        touchListener.setSwipeSensitivity(19);
        view.setOnTouchListener(touchListener);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //-------SimpleTouchListener Callbacks----------//
    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {
    }

    @Override
    public void click(View v,float x,float y) {
    }

    @Override
    public void swipe(View v,int direction){
        switch (direction){
            case SimpleTouchListener.SWIPE_RIGHT:
                delegate.showCamera();
                break;
            case SimpleTouchListener.SWIPE_DOWN:
                delegate.showProjects();
                break;
            case SimpleTouchListener.SWIPE_LEFT:
                break;
        }
    }


    MainFragmentCallBack delegate;
    public interface MainFragmentCallBack{
       void showCamera();
        void showHelpScreen();
        void showProjects();
    }
}
