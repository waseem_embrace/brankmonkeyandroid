package com.embrace.it.beskrivapp.components.touchbutton;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;

import com.embrace.it.beskrivapp.commons.Constants.*;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.components.BaseButton;

/**
 * Created by wasimshigri on 1/25/17.
 */

public  class ThumbButton extends BaseButton {

private int padding = 15;

    public ThumbButton(Context context) {
        super(context);
        setButtonGroupType(ButtonGroupType.group0);
        setButtonType(ToolType.None);
        initPaint();
        setBackgroundColor(Color.parseColor("#00000000"));
        padding = Utils.convertDpToPx(padding);
        setPadding(padding,padding,padding,padding);
    }

    public ThumbButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThumbButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        canvas.drawCircle(getWidth()/2,getHeight()/2,getWidth()/2,p);
        RectF circleBound = new RectF(0+strokewidth,0+strokewidth,getWidth()-strokewidth,getHeight()-strokewidth);
        canvas.drawArc(circleBound,0,arcDegree,false,paint);
    }


    long durationProgress = 1000;
    int interval = 20;
    private float arcDegree = 360;
    private Paint paint;
    //Paint
    private int strokewidth = 5;
    private int strokeColor = Color.WHITE;
    void initPaint(){
        paint = new Paint();
        paint.setStrokeWidth(strokewidth);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(strokeColor);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }

    void redrawWithArc(Message msg){
        arcDegree+= (360/interval);
        if(arcDegree < 0){
            arcDegree = 0;
        } else if(arcDegree >= 360){
            arcDegree = 360;
        } else{
            handler.postDelayed(runnable,durationProgress/interval);
        }
        postInvalidate();
    }

   private final Runnable runnable  = new Runnable() {
        @Override
        public void run() {
            Message msg = new Message();
            handler.dispatchMessage(msg);
        }
    };
    private final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            redrawWithArc(msg);
        }
    };
    public void showProgessForDuration(int miliseconds){
        arcDegree = 0;
        durationProgress = miliseconds;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable,0);
    }

}
