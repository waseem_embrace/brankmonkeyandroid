package com.embrace.it.beskrivapp.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.commons.helpers.MyGoogleLocation;
import com.embrace.it.beskrivapp.database.AlbumInfo;
import com.embrace.it.beskrivapp.database.DBController;
import com.embrace.it.beskrivapp.database.Project;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by waseem on 1/31/17.
 */

@RuntimePermissions
public class CreatProjectFragment extends Fragment implements SimpleTouchListener.SimpleTouchListenerCallback {

    ImageView ivBackground;
    TextView tvTitle;
    TextView tvDescription;
    TextView tvAlbumNew;
    EditText etAlbumTitle;
    EditText etWho;
    EditText etWhere;
    EditText etOther;
    Button btCancel;
    Button btSave;
    ImageButton btGps;
    ImageButton btDate;
    ImageButton btTime;
    SimpleTouchListener touchListener;

    Typeface boldTypeface;
    Typeface romanTypeface;

    Project project;

    private String textViewPlaceholder = "Fx. vejr eller andre forhold";

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Bundle b = getArguments();
        if (b != null) {
            String projectId = getArguments().getString("projectId");
            project = DBController.getProject(projectId);
        }

        View view = inflater.inflate(R.layout.fragment_newproject, container, false);
        ivBackground = (ImageView)view.findViewById(R.id.iv_background);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvAlbumNew = (TextView) view.findViewById(R.id.tv_album_title);
        tvDescription = (TextView) view.findViewById(R.id.tv_description);
        etAlbumTitle = (EditText) view.findViewById(R.id.et_album_title);
        etWho = (EditText) view.findViewById(R.id.et_who);
        etWhere = (EditText) view.findViewById(R.id.et_where);
        etOther = (EditText) view.findViewById(R.id.et_other);
        btCancel = (Button) view.findViewById(R.id.btn_newproject_cancel);
        btSave = (Button) view.findViewById(R.id.btn_newproject_save);
        btGps = (ImageButton) view.findViewById(R.id.btn_newproject_gps);
        btTime = (ImageButton) view.findViewById(R.id.btn_newproject_time);
        btDate = (ImageButton) view.findViewById(R.id.btn_newproject_date);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        touchListener = new SimpleTouchListener(getActivity().getApplicationContext(), this);
        touchListener.setSwipeSensitivity(15);
        view.setOnTouchListener(touchListener);
        view.findViewById(R.id.scrollview).setOnTouchListener(touchListener);
        btCancel.setOnTouchListener(touchListener);
        btSave.setOnTouchListener(touchListener);
        btGps.setOnTouchListener(touchListener);
        btTime.setOnTouchListener(touchListener);
        btDate.setOnTouchListener(touchListener);

        boldTypeface = Utils.getFontTypeface(getActivity().getApplicationContext(), Constants.FONT_FrutigerLTStd_Bold);
        romanTypeface = Utils.getFontTypeface(getActivity().getApplicationContext(), Constants.FONT_FrutigerLTStd_Roman);
        ((TextView) view.findViewById(R.id.tv_album_title)).setTypeface(boldTypeface);
        ((TextView) view.findViewById(R.id.tv_who)).setTypeface(boldTypeface);
        ((TextView) view.findViewById(R.id.tv_where)).setTypeface(boldTypeface);
        ((TextView) view.findViewById(R.id.tv_other)).setTypeface(boldTypeface);
        etAlbumTitle.setTypeface(romanTypeface);
        etWhere.setTypeface(romanTypeface);
        etWho.setTypeface(romanTypeface);
        etOther.setTypeface(romanTypeface);

        View.OnFocusChangeListener focusListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                btSave.setText(R.string.Save);
                if (hasFocus) {
                    switch (v.getId()) {
                        case R.id.et_album_title:
                            tvDescription.setText(getString(R.string.whatGuide));
                            break;
                        case R.id.et_where:
                            tvDescription.setText(getString(R.string.whereGuide));
                            break;
                        case R.id.et_who:
                            tvDescription.setText(getString(R.string.whoGuide));
                            break;
                        case R.id.et_other:
                            tvDescription.setText(getString(R.string.otherGuide));
                            break;
                    }
                }else{
                    tvDescription.setText(titleDescription);
                }
            }
        };
        etAlbumTitle.setOnFocusChangeListener(focusListener);
        etWhere.setOnFocusChangeListener(focusListener);
        etWho.setOnFocusChangeListener(focusListener);
        etOther.setOnFocusChangeListener(focusListener);

        ((TextView) view.findViewById(R.id.tv_gps)).setTypeface(romanTypeface);
        ((TextView) view.findViewById(R.id.tv_time)).setTypeface(romanTypeface);
        ((TextView) view.findViewById(R.id.tv_date)).setTypeface(romanTypeface);

        btCancel.setTypeface(romanTypeface);
        btSave.setTypeface(boldTypeface);
        btSave.setText(R.string.Skip_over);
        tvDescription.setText(titleDescription);

        initViews();
    }
String titleDescription;
    void initViews() {

        try {
            MainActivity m = (MainActivity) ivBackground.getContext();
            ivBackground.setAlpha(1.0f);
        }catch(Exception e){
            ivBackground.setAlpha(0.4f);
        }
        if (project != null) {
            AlbumInfo albuminfo = project.getAlbum();
            tvTitle.setText(getString(R.string.updateProject));
            tvAlbumNew.setText(R.string.album_name);
            tvTitle.setAllCaps(false);
            titleDescription = getString(R.string.updateProject);
            tvDescription.setText(getString(R.string.updateProjectSubtitle));
            if (albuminfo.getAlbumTitle() != null && !albuminfo.getAlbumTitle().isEmpty()) {
                etAlbumTitle.setText(albuminfo.getAlbumTitle());
            }
            if (albuminfo.getWho() != null && !albuminfo.getWho().isEmpty()) {
                etWho.setText(albuminfo.getWho());
            }
            if (albuminfo.getWhere() != null && !albuminfo.getWhere().isEmpty()) {
                etWhere.setText(albuminfo.getWhere());
            }
            if (albuminfo.getOther() != null && !albuminfo.getOther().isEmpty()) {
                etOther.setText(albuminfo.getOther());
            }
            if (albuminfo.getGps() != null && albuminfo.getGps()) {
                btGps.setBackgroundResource(R.drawable.cirkel_valgt);
            }
            if (albuminfo.getTime() != null && albuminfo.getTime()) {
                btTime.setBackgroundResource(R.drawable.cirkel_valgt);
            }
            if (albuminfo.getDate() != null && albuminfo.getDate()) {
                btDate.setBackgroundResource(R.drawable.cirkel_valgt);
            }

            btSave.setText(R.string.Save);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    //-------SimpleTouchListener Callbacks----------//

    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {
        if (yTotal < -30) {
//            getActivity().getFragmentManager().popBackStack();
        }
    }


    @Override
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void click(View v,float x,float y) {

        View focused = getActivity().getCurrentFocus();
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focused != null) {
            focused.clearFocus();
            inputManager.hideSoftInputFromWindow(focused.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

        tvDescription.setText(titleDescription);
        if (v.equals(btSave)) {
            saveData();
            if (this.delegate != null)
                this.delegate.startDrawing(this.project.getProjectId());
        } else if (v.equals(btCancel)) {
            getActivity().getFragmentManager().popBackStack();
        } else if(v.equals(btGps)){
            MyGoogleLocation.sharedInstance().initLocationService();
            ImageButton btn = (ImageButton) v;
            if (isActive(btn))
                btn.setBackgroundResource(R.drawable.cirkel_tom);
            else
                btn.setBackgroundResource(R.drawable.cirkel_valgt);

        }else if ( v.equals(btDate) || v.equals(btTime)) {

            ImageButton btn = (ImageButton) v;
            if (isActive(btn))
                btn.setBackgroundResource(R.drawable.cirkel_tom);
            else
                btn.setBackgroundResource(R.drawable.cirkel_valgt);
        }
    }

    @Override
    public void swipe(View v, int direction) {
        switch (direction) {
            case SimpleTouchListener.SWIPE_UP:
//                getActivity().getFragmentManager().popBackStack();
                break;
        }
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        Toast.makeText(this.getActivity(), R.string.location_permision_denied, Toast.LENGTH_SHORT).show();
        btGps.setBackgroundResource(R.drawable.cirkel_valgt);
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this.getActivity())
                .setMessage(R.string.location_permission_request)
                .setPositiveButton(R.string.allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void showNeverAskForCamera() {
        Toast.makeText(this.getActivity(), "Will never aks for location permission again", Toast.LENGTH_SHORT).show();
    }
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    Boolean isActive(ImageButton btn) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (btn.getBackground().getConstantState() == getResources().getDrawable(R.drawable.cirkel_valgt, MainApplication.getContext().getTheme()).getConstantState()) {
                return true;
            } else
                return false;
        } else {
            if (btn.getBackground().getConstantState() == getResources().getDrawable(R.drawable.cirkel_valgt).getConstantState()) {
                return true;
            } else
                return false;
        }
    }

    void saveData() {
//        if (etAlbumTitle.getText().toString().trim().isEmpty()) {
//            showAlert(etAlbumTitle.getText().toString().trim(), "Please fill the required fields");
//        } else if (DBController.getInstance().isProjectExisting(etAlbumTitle.getText().toString().trim())) {
//            showAlert(etAlbumTitle.getText().toString().trim(), "Projeect already exists. Please change the title");
//        } else

//        if(btSave.getText().equals(getString(R.string.Save)))
        {
            String value;
            Project proj = new Project();
            AlbumInfo album = new AlbumInfo();
            String projectID = DBController.getNextIdForProject();
            proj.setProjectId(projectID);

            value = etAlbumTitle.getText().toString().trim();
            if (value != null && !value.isEmpty())
                album.setAlbumTitle(etAlbumTitle.getText().toString().trim());
            else
                album.setAlbumTitle(Constants.ifNoTitle);

            value = etOther.getText().toString().trim();
            if (value != null && !value.isEmpty())
                album.setOther(etOther.getText().toString().trim());
            else
                album.setOther(Constants.ifNoDescription);


            proj.setProjectTitle(album.getAlbumTitle());
            album.setWho(etWho.getText().toString().trim());
            album.setWhere(etWhere.getText().toString().trim());
            album.setGps(isActive(btGps));
            album.setTime(isActive(btTime));
            album.setDate(isActive(btDate));
            proj.setAlbum(album);

            if (this.project != null)
                DBController.updateProject(this.project, proj);
            else {
                DBController.getInstance().saveProject(proj);
                this.project = proj;
            }
        }
    }


    void showAlert(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setNeutralButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }


    CreateProjectCallback delegate;

    public interface CreateProjectCallback {
        void startDrawing(String projectID);
    }

    public void setDelegate(CreateProjectCallback d) {
        this.delegate = d;
    }

}
