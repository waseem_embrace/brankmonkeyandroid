package com.embrace.it.beskrivapp;

import android.app.Application;
import android.content.Context;

import com.embrace.it.beskrivapp.commons.helpers.MyGoogleLocation;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.wonderkiln.blurkit.BlurKit;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by wasimshigri on 1/26/17.
 */

public class MainApplication extends Application  {
    public static Context getContext() {
        return context;
    }

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("BeskriveApp.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        BlurKit.init(this);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        MyGoogleLocation.sharedInstance().dismiss();
    }





}
