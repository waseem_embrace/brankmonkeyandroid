package com.embrace.it.beskrivapp.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.components.NonSwipeableViewPager;
import com.embrace.it.beskrivapp.fragments.camera.CameraFragment;

/**
 * Created by waseem on 1/31/17.
 */

public class HelpFragment extends Fragment implements SimpleTouchListener.SimpleTouchListenerCallback {

    String FRAGMENT_NOIMAGE_DIALOGE = "noImage_dialog";


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    Button btnLeft;
    Button btnRight;
    Button btShowCamera;
    TextView tvTitleToAnimateHide;
    ImageSlidePagerAdapter adapter;
    private SimpleTouchListener touchListener;
    private int[] imageList = {R.drawable.guide_tip1, R.drawable.guide_tip4, R.drawable.guide_tip5};
    NonSwipeableViewPager pager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        btnLeft = (Button) view.findViewById(R.id.btn_help_swipe_left);
        btnRight = (Button) view.findViewById(R.id.btn_help_swipe_right);
        btShowCamera = (Button) view.findViewById(R.id.btn_help_camera);
        tvTitleToAnimateHide = ((TextView) view.findViewById(R.id.tv_help_title1));

        pager = (NonSwipeableViewPager) view.findViewById(R.id.pager_help);
        pager.setPagingEnabled(false);
        adapter = new ImageSlidePagerAdapter(container.getContext(), imageList);
        pager.setAdapter(adapter);

        LinearLayout layoutDots = (LinearLayout) view.findViewById(R.id.ll_help_circles);
        addDots(container.getContext(), layoutDots);

        touchListener = new SimpleTouchListener(container.getContext(), this);
        touchListener.setSwipeSensitivity(13);
        view.setOnTouchListener(touchListener);
        btnLeft.setOnTouchListener(touchListener);
        btnRight.setOnTouchListener(touchListener);
        btShowCamera.setOnTouchListener(touchListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface boldTypeface = Utils.getFontTypeface(getActivity().getApplicationContext(), Constants.FONT_FrutigerLTStd_Bold);
        Typeface romanTypeface = Utils.getFontTypeface(getActivity().getApplicationContext(), Constants.FONT_FrutigerLTStd_Roman);
        tvTitleToAnimateHide.setTypeface(boldTypeface);
        ((TextView) view.findViewById(R.id.tv_help_title2)).setTypeface(romanTypeface);
        btShowCamera.setTypeface(boldTypeface);
        handler.postDelayed(animateTitleHide,3000);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }




    final Handler handler = new Handler();
    Runnable animateTitleHide = new Runnable() {
        public void run() {
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            animateTitleToHide();
                        } catch (Exception e) {

                        }
                    }
                });
            } catch (Exception e) {
            }
        }
    };
    void animateTitleToHide(){
         View view = (View)tvTitleToAnimateHide.getParent();
        Animator anim1 =  ObjectAnimator.ofFloat(view,"y",view.getY(),view.getY() - tvTitleToAnimateHide.getHeight());
        Animator anim2 =  ObjectAnimator.ofFloat(tvTitleToAnimateHide,"alpha",1.0f,0.0f);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(1000);
        set.playTogether(anim1,anim2);
        set.start();
    }


    private class ImageSlidePagerAdapter extends PagerAdapter {

        Context mContext;
        int[] images;

        public ImageSlidePagerAdapter(Context context, int[] images) {
            this.mContext = context;
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
//            super.instantiateItem(container, position);
            ImageView mImageView = new ImageView(mContext);
            mImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            mImageView.setBackgroundResource(images[position]);
            ((ViewPager) container).addView(mImageView, 0);
            return mImageView;

        }

        @Override
        public void destroyItem(ViewGroup container, int i, Object obj) {
            ((ViewPager) container).removeView((ImageView) obj);
        }
    }






    //-------SimpleTouchListener Callbacks----------//

    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {
        if (v.equals(btnLeft)) {

        } else if (v.equals(btnRight)) {

        } else if (Math.abs(yTotal) > Math.abs(xTotal)) {
            //Swipe Up
            if (yTotal < -20) {

            }
        }else if (Math.abs(xTotal) > Math.abs(yTotal)){
            //Swipe Left
            if(xTotal < -20) {

            }
        }
    }

    @Override
    public void click(View v,float x,float y) {
        if (v.equals(btnLeft)) {
            int currPosition =pager.getCurrentItem();
            int nextPosition = currPosition - 1;
            if (nextPosition < 0)
                nextPosition = imageList.length -1 ;
            swipeToPosition(nextPosition);
        } else if (v.equals(btnRight)) {
            int currPosition = pager.getCurrentItem();
            int nextPosition = currPosition + 1;
            if (nextPosition >= imageList.length)
                nextPosition = 0;
            swipeToPosition(nextPosition);
        }else if(v.equals(btShowCamera)){
            ((MainActivity) getActivity()).showCamera();
        }
    }

    @Override
    public void swipe(View v,int direction){

        if (v.equals(btnLeft)) {

        } else if (v.equals(btnRight)) {

        } else if (v.equals(btShowCamera)) {

        }else
        {
            switch (direction){
                case SimpleTouchListener.SWIPE_LEFT:
                    new NoImageDialog().show(getChildFragmentManager(), FRAGMENT_NOIMAGE_DIALOGE);
                    break;
                case SimpleTouchListener.SWIPE_DOWN:
                    ((MainActivity) getActivity()).showProjects();
                    break;
                case SimpleTouchListener.SWIPE_RIGHT:
                    ((MainActivity) getActivity()).showCamera();
                    break;
            }
        }
    }

    void swipeToPosition(int nextPosition) {

        btnLeft.setVisibility(View.VISIBLE);
        btnRight.setVisibility(View.VISIBLE);
        if (nextPosition == 0)
            btnLeft.setVisibility(View.INVISIBLE);
        else if (nextPosition == imageList.length - 1)
            btnRight.setVisibility(View.INVISIBLE);
        else {}

        pager.setCurrentItem(nextPosition);
        setDotAtPosition(nextPosition);

    }

    private ImageView[] dotes ;
    void addDots(Context context, LinearLayout containerView) {
        int size = imageList.length;
        float width = getResources().getDimension(R.dimen.dp_4);
        width = Utils.dipToPixels(context,width);
        float height = getResources().getDimension(R.dimen.dp_4);
        height = Utils.dipToPixels(context,height);
        float margin = getResources().getDimension(R.dimen.dp_2);
        margin = Utils.dipToPixels(context,margin);
        dotes = new ImageView[imageList.length];
        for (int i = 0; i < size; i++) {
            ImageView iv = new ImageView(context);
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams((int)width, (int)height);
            parms.setMargins((int)margin, 0, (int)margin, 0);
            iv.setLayoutParams(parms);
            iv.setBackgroundResource( R.drawable.dot_default);
            containerView.addView(iv);
            dotes[i] = iv;
        }
        swipeToPosition(0);
    }

    void setDotAtPosition(int postition) {

        for (int i = 0; i < dotes.length; i++) {
            ImageView iv = dotes[i];
            iv.setBackgroundResource(R.drawable.dot_default);
        }
        ImageView iv = dotes[postition];
        iv.setBackgroundResource(R.drawable.dot_selected);
    }



    public static class NoImageDialog extends DialogFragment implements View.OnClickListener {

        private HelpFragment parent;
        TextView tvTitle;
        String title = "";
        Button btnNo;
        Button btnOpenCamera;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v = inflater.inflate(
                    R.layout.dialoge_fragment_alert, container, false);
            tvTitle = (TextView) v.findViewById(R.id.tv_dialog_title);
            tvTitle.setText(getString(R.string.You_have_not_taken_some_pictures_you_will_go_to_the_camera));
             btnNo = (Button) v.findViewById(R.id.bt_dialog_second);
             btnOpenCamera = (Button) v.findViewById(R.id.bt_dialog_first);
            btnNo.setOnClickListener(this);
            btnOpenCamera.setOnClickListener(this);
            return v;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog d = super.onCreateDialog(savedInstanceState);
            parent = (HelpFragment) getParentFragment();
            d.setCancelable(false);
            return d;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationRightToLeft;
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        }

        @Override
        public void onClick(View v) {
            if (v.equals(btnNo)) {
                this.dismiss();
            } else if (v.equals(btnOpenCamera)) {
                ((MainActivity) getActivity()).showCamera();
                this.dismiss();
            }
        }
    }

/*
    public static class NoImageDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.You_have_not_taken_some_pictures_you_will_go_to_the_camera)
                    .setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ((MainActivity) getActivity()).showCamera();
                        }
                    })
                    .setNegativeButton(R.string.No,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                    .create();
        }
    }*/
}
