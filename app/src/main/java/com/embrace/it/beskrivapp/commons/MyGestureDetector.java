package com.embrace.it.beskrivapp.commons;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by waseem on 2/15/17.
 */


//---------Gesture Recognizer--------------//
public  class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {

    public interface SimpleGestureListenerCallback {
        void paneGesture(double xNet, double yNet, double xTotal, double yTotal);

        void clickGesture(float x, float y);

        void swipeGesture(int direction);
    }

    private float xDown = 0, yDown = 0;
    private float xDownPrev = 0, yDownPrev = 0;
    SimpleGestureListenerCallback delegate;
    private final static int ACTION_FAKE = -13; //just an unlikely number
    private int swipe_Min_Distance = 50;
    private int swipe_Max_Distance = 1000;
    private int swipe_Min_Velocity = 50;

    private GestureDetector mGestureDetector;
    public final int Max_Swipe_Sensivity = 20;
    public final int Min_Swipe_Sensivity = 1;

    public void setSwipeSensitivity(int swipeSensitvity) {

        if (swipeSensitvity > Max_Swipe_Sensivity) {
            swipeSensitvity = Max_Swipe_Sensivity;
        } else if (swipeSensitvity < Min_Swipe_Sensivity) {
            swipeSensitvity = Min_Swipe_Sensivity;
        } else {
        }
        swipe_Min_Distance = ((Max_Swipe_Sensivity - swipeSensitvity) + 1) * 10;
        swipe_Min_Velocity = ((Max_Swipe_Sensivity - swipeSensitvity) + 1) * 10;

    }


    public MyGestureDetector(SimpleGestureListenerCallback callback) {
        this.delegate = callback;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        super.onSingleTapUp(e);
        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        super.onSingleTapConfirmed(e);
        delegate.clickGesture(xDown,yDown);
        return false;
    }

    @Override
    public boolean onDown(MotionEvent event) {
        super.onDown(event);
        xDownPrev = xDown = event.getX();
        yDownPrev = yDown = event.getY();
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, final float distanceX, float distanceY) {
        float xNet = e2.getX() - xDownPrev;
        float yNet = e2.getY() - yDownPrev;
        if (Math.abs(xNet) > 0.1 && Math.abs(yNet) > 0.1) {
            delegate.paneGesture(xNet, yNet, e2.getX()- e1.getX(), e2.getY()- e1.getY());
            xDownPrev = e2.getX();
            yDownPrev = e2.getY();
        }
        return super.onScroll(e1, e2, distanceX, distanceY);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // do something
        super.onFling(e1, e2, velocityX, velocityY);

        final float xDistance = Math.abs(e1.getX() - e2.getX());
        final float yDistance = Math.abs(e1.getY() - e2.getY());

        if (xDistance > swipe_Max_Distance || yDistance > swipe_Max_Distance)
            return false;

        velocityX = Math.abs(velocityX);
        velocityY = Math.abs(velocityY);
        boolean result = false;

        if (velocityX > swipe_Min_Velocity && (xDistance - yDistance) > swipe_Min_Distance) {
            if (e1.getX() > e2.getX()) // right to left
                delegate.swipeGesture(SimpleTouchListener.SWIPE_LEFT);
            else
                delegate.swipeGesture(SimpleTouchListener.SWIPE_RIGHT);

            result = true;
        }

        if (velocityY > swipe_Min_Velocity && (yDistance - xDistance) > swipe_Min_Distance) {
            if (e1.getY() > e2.getY()) // bottom to up
                delegate.swipeGesture(SimpleTouchListener.SWIPE_UP);
            else
                delegate.swipeGesture(SimpleTouchListener.SWIPE_DOWN);

            result = true;
        }

        return result;
    }
}