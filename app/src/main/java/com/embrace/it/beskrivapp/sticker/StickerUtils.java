package com.embrace.it.beskrivapp.sticker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.RectF;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static java.lang.Math.round;

/**
 * @author wupanjie
 */
public class StickerUtils {
    private static final String TAG = "StickerView";

    public static String saveImageToGallery(File file, Bitmap bmp) {
        if (bmp == null) {
            Log.e(TAG, "saveImageToGallery: the bitmap is null");
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "saveImageToGallery: the path of bmp is " + file.getAbsolutePath());
        return file.getAbsolutePath();
    }

    public static void notifySystemGallery(Context context, File file) {
        if (file == null || !file.exists()) {
            Log.e(TAG, "notifySystemGallery: the file do not exist.");
            return;
        }

        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(),
                    file.getName(), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
    }

    public static RectF trapToRect(float[] array) {
        RectF r = new RectF(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY,
                Float.NEGATIVE_INFINITY);
        for (int i = 1; i < array.length; i += 2) {
            float x = round(array[i - 1] * 10) / 10.f;
            float y = round(array[i] * 10) / 10.f;
            r.left = (x < r.left) ? x : r.left;
            r.top = (y < r.top) ? y : r.top;
            r.right = (x > r.right) ? x : r.right;
            r.bottom = (y > r.bottom) ? y : r.bottom;
        }
        r.sort();
        return r;
    }


    /*
    return rect = {left,top,width,height}
     */
    public static RectF calculateBounds(List<PointF> pointsList, float radius) {
        if (pointsList == null || pointsList.size() == 0) {
            return new RectF(0, 0, 0, 0);
        }

        float boundsMinX = Integer.MAX_VALUE;
        float boundsMinY = Integer.MAX_VALUE;
        float boundsMaxX = Integer.MIN_VALUE;
        float boundsMaxY = Integer.MIN_VALUE;

        for (int i = 0; i < pointsList.size(); i++) {
            PointF p = pointsList.get(i);
            float x = p.x;
            boundsMinX = Math.min(boundsMinX, x - radius);
            boundsMaxX = Math.max(boundsMaxX, x + radius);
            float y = p.y;
            boundsMinY = Math.min(boundsMinY, y - radius);
            boundsMaxY = Math.max(boundsMaxY, y + radius);
        }
        RectF bounds = new RectF(boundsMinX, boundsMinY,
                boundsMaxX - boundsMinX,
                boundsMaxY - boundsMinY);
        return bounds;
    }

   public static boolean isInsideCircle(PointF p, PointF center, float radius) {
        return (((p.x - center.x) * (p.x - center.x)) + ((p.y - center.y) * (p.y - center.y))) < (radius * radius);
    }

    public static  double angleBetween(PointF center, PointF current, PointF previous) {
        return Math.toDegrees(Math.atan2(current.x - center.x,current.y - center.y)-
                Math.atan2(previous.x- center.x,previous.y- center.y));
    }

    public static double  angleToXAxis(PointF p1, PointF p2)
    {
        if((p2.x - p1.x) == 0){
            return 90;
        }else if((p2.y - p1.y) == 0){
            return 0;
        }else{
            float m = ((p2.y - p1.y) / (p2.x - p1.x));
            return  Math.toDegrees(Math.atan2(p2.y - p1.y,p2.x - p1.x));
        }
    }
}
