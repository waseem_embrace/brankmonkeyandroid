package com.embrace.it.beskrivapp.commons.observables;

import com.embrace.it.beskrivapp.components.tools.ToolMenuSubButton;

import java.util.Observable;

/**
 * Created by waseem on 2/21/17.
 */

public class ToolMenuSubButtonObservable extends Observable {

    public void notifyItemClicked(Object item){
        setChanged();
        notifyObservers(item);
    }
}
