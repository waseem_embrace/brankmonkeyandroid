package com.embrace.it.beskrivapp.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.FileProvider;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.actionsheet.ActionSheet;
import com.embrace.it.beskrivapp.BuildConfig;
import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.commons.PdfCreator;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.embrace.it.beskrivapp.commons.observables.EventBus;
import com.embrace.it.beskrivapp.commons.observables.ThumbnailSavedObservable;
import com.embrace.it.beskrivapp.database.DBController;
import com.embrace.it.beskrivapp.database.Project;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.SubProject;
import com.embrace.it.beskrivapp.sticker.toolframs.MainImageSticker;

import java.io.File;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static android.app.Activity.RESULT_OK;

/**
 * Created by waseem on 1/31/17.
 */

public class SubProjectsFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener,Observer {


    ViewPager pager;
    SubProjectsAdapter adapter;
    Project project;
    SubProject subProject;
    TextView tvMenu1;
    TextView tvMenu2;
    TextView tvMenu3;
    TextView tvMenu4;
    ProgressBar progressbar;
    RelativeLayout progressView;
    BottomSheetBehavior mBottomSheetBehavior;
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_subproject, container, false);
        view.setEnabled(true);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        pager = (ViewPager) view.findViewById(R.id.pager_subproject);
        ((ImageButton) view.findViewById(R.id.btn_subproject_delete)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_copy)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_settings)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_share)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_import)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_share_copy)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_share_pdf)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_subproject_share_saveimg)).setOnClickListener(this);
        ((Button) view.findViewById(R.id.btn_subproject_share_others)).setOnClickListener(this);
        tvMenu1 = ((TextView) view.findViewById(R.id.tv_subproject_menu1));
        tvMenu2 = ((TextView) view.findViewById(R.id.tv_subproject_menu2));
        tvMenu3 = ((TextView) view.findViewById(R.id.tv_subproject_menu3));
        tvMenu4 = ((TextView) view.findViewById(R.id.tv_subproject_menu4));
        progressbar = (ProgressBar) view.findViewById(R.id.progress_subproejct);
        progressView = (RelativeLayout) view.findViewById(R.id.rl_progress);
        View bottomSheet = view.findViewById( R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        progressView.setVisibility(View.INVISIBLE);
        pager.setClipToPadding(false);
        pager.setClipChildren(false);
        pager.addOnPageChangeListener(this);
        pager.setPadding(Utils.convertDpToPx(container.getContext(), 50f), Utils.convertDpToPx(container.getContext(), 50f),
                Utils.convertDpToPx(container.getContext(), 50f), Utils.convertDpToPx(container.getContext(), 20f));
//        pager.setPageMargin(Utils.convertDpToPx(container.getContext(), 10f));
        adapter = new SubProjectsAdapter(project);
        pager.setAdapter(adapter);
        if (subProject != null && project != null && project.getSubProjects() != null) {
            int index = project.getSubProjects().indexOf(subProject);
            pager.setCurrentItem(index);
            setStickerCounts(index);
        }
        return view;
    }

    String projectId;
    String subProjectId;
    String subProjectIdFromMain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        if (b != null) {
            projectId = b.getString("projectId");
            subProjectId = b.getString("subProjectId");
            subProjectIdFromMain = subProjectId;
            initSubProjects(true);
        }
        EventBus.getThumbnailSavedObserver().addObserver(this);
    }

    void initSubProjects(boolean firstTime) {
        project = DBController.getProject(projectId);
        if (subProjectId != null && project != null && project.getSubProjects() != null) {

            for (SubProject sub : project.getSubProjects()) {
                if (sub.getSubProjectId().equals(subProjectId))
                    subProject = sub;
            }

            if (subProject == null && project.getSubProjects().size() > 0) {
                subProject = project.getSubProjects().get(0);
            }
        } else if (subProjectId == null && project != null && project.getSubProjects() != null && project.getSubProjects().size() > 0) {
            subProject = project.getSubProjects().get(0);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    View getViewForPagerAtPosition(ViewGroup container, Context context, int position, List<SubProject> datalist) {
        SubProject sub = datalist.get(position);
        View view = LayoutInflater.from(context).inflate(R.layout.subproject, container, false);

        ImageView iv = (ImageView) view.findViewById(R.id.iv_subproject);
        TextView tv = (TextView) view.findViewById(R.id.tv_subproject_title);
        tv.setText(sub.getProjectTitle());

        if (sub.getThumbnailPath() != null) {
            Bitmap b = ImageHelper.readImage(sub.getThumbnailPath(), false);
            iv.setImageBitmap(b);
        }
        iv.setEnabled(true);
        iv.setOnClickListener(this);
        view.setTag(sub.getSubProjectId());
        return view;
    }

    SubProject subProjectBeingEditted;

    @Override
    public void onClick(View v) {
        SubProject sub = project.getSubProjects().get(pager.getCurrentItem());
        subProjectBeingEditted = sub;
        switch (v.getId()) {
            case R.id.btn_subproject_delete:
                DeleteAlertDialog d = new DeleteAlertDialog();
                d.setTargetFragment(this, 0);
                d.show(getChildFragmentManager(), "");
                break;
            case R.id.iv_subproject:
                if (sub != null) {
                    ((MainActivity) getActivity()).selectSubProejct(sub.getSubProjectId());
                    ((MainActivity) getActivity()).popFragment(this);
                }
                break;

            case R.id.btn_subproject_settings:
                String title = subProjectBeingEditted.getProjectTitle();
                NewTitleDialogFragment f = new NewTitleDialogFragment();
                f.isEdittingTitle = true;
                f.oldTitle = title;
                f.show(getChildFragmentManager(), null);
                break;

            case R.id.btn_subproject_import:
                importImage();
                break;

            case R.id.btn_subproject_copy:
                String title1 = subProjectBeingEditted.getProjectTitle() + "(KOPI)";
                NewTitleDialogFragment ff = new NewTitleDialogFragment();
                ff.isEdittingTitle = false;
                ff.oldTitle = title1;
                ff.show(getChildFragmentManager(), null);
                break;

            case R.id.btn_subproject_share:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
///                shareToEmail();
                break;
            case R.id.btn_subproject_share_copy:
                 mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                break;
            case R.id.btn_subproject_share_pdf:
                createPDF();
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

            case R.id.btn_subproject_share_saveimg:
                Bitmap bitmap = ImageHelper.readImage(subProjectBeingEditted.getThumbnailPath(),false);
                ImageHelper.addImageToGallary(getActivity(),bitmap,subProjectBeingEditted.getProjectTitle(),"");
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                break;
            case R.id.btn_subproject_share_others:
                shareToEmail();
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

        }
    }

    void shouldCopy(String subProjectId){

    }
    void updateTitle(String title) {
        DBController.updateSubprojectTitle(subProjectBeingEditted, title);
        View v = pager.findViewWithTag(subProjectBeingEditted.getSubProjectId());
        if (v != null) {
            TextView tv = (TextView) v.findViewById(R.id.tv_subproject_title);
            tv.setText(title);
        }
    }

    //--------Import--------------//
    int RESULT_LOAD_IMG = 00123;
    void importImage(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                Bitmap b=BitmapFactory
                        .decodeFile(imgDecodableString);
                MyLog.emptyLog();
                createProjectForImport(b);
                MyLog.emptyLog();

            } else {

                Toast.makeText(getActivity(), "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }
    //------------//
    void duplicateImage1(final String title) {

        progressView.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(((MainActivity) getActivity()).duplicateImage(subProjectBeingEditted, title)) {
                            initSubProjects(false);
                            int count = project.getSubProjects().size();
                            adapter = new SubProjectsAdapter(project);
                            pager.setAdapter(null);
                            pager.setAdapter(adapter);
                        }
                        progressView.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }, 200);

    }

    public void duplicateImage( final String title) {
        SubProject sub = subProjectBeingEditted;
        final String subProjId = sub.getSubProjectId();
        final String newSubProjectId = ((MainActivity) getActivity()).getNextSubProjectId(project);
        final String mainImgPath  = sub.getMainItem().getImagePath();
        final String  thumbnailTitle = newSubProjectId;

        Bitmap thumbnail = null;
        String pathMain = null;
        progressView.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap thumbnail;
                Bitmap b = ImageHelper.readBitmapFromInternalStorage(subProjId + "background", false);
                if (b != null) {
                    thumbnail = ImageHelper.readBitmapFromInternalStorage(subProjId + "background", true);
                } else {
                    b = ImageHelper.readBitmapFromInternalStorage(mainImgPath, false);
                    thumbnail = ImageHelper.readBitmapFromInternalStorage(mainImgPath, true);
                }

                if (b != null && thumbnail != null) {
                    final String pathMain = ImageHelper.saveBitmapToInternalStorage(b, newSubProjectId + "background");
                    final String pathThumbnail = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);

                    ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final ProjectItem projectimtem = MainImageSticker.getDefaultProjectItem();
                            SubProject subPro = DBController.addSubProject(project, projectimtem, newSubProjectId, pathMain, title);
                            DBController.updateThumbnailImage(subPro.getSubProjectId(), pathThumbnail);

                            initSubProjects(false);
                            int count = project.getSubProjects().size();
                            adapter = new SubProjectsAdapter(project);
                            pager.setAdapter(null);
                            pager.setAdapter(adapter);
                            progressView.setVisibility(View.INVISIBLE);
                        }
                    });
                } else {
                    ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressView.setVisibility(View.INVISIBLE);

                        }
                    });
                }
            }
        }).start();
    }

    public void createProjectForImport(final Bitmap b) {
        final String fileName = "Importeret Billede";
        final String newSubProjectId = ((MainActivity) getActivity()).getNextSubProjectId(project);
        final String  thumbnailTitle = newSubProjectId;
        progressView.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                String path = ImageHelper.saveBitmapToInternalStorage(b,System.currentTimeMillis() + ".jpg");
                if (path != null) {
                   final Bitmap thumbnail = ImageHelper.readImage(path,true);

                    if (b != null && thumbnail != null) {
                        final String pathMain = ImageHelper.saveBitmapToInternalStorage(b, newSubProjectId + "background");
                        final String pathThumbnail = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);

                        ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final ProjectItem projectimtem = MainImageSticker.getDefaultProjectItem();
                                SubProject subPro = DBController.addSubProject(project, projectimtem, newSubProjectId, pathMain, fileName);
                                DBController.updateThumbnailImage(subPro.getSubProjectId(), pathThumbnail);

                                initSubProjects(false);
                                int count = project.getSubProjects().size();
                                adapter = new SubProjectsAdapter(project);
                                pager.setAdapter(null);
                                pager.setAdapter(adapter);
                                progressView.setVisibility(View.INVISIBLE);
                            }
                        });
                    } else {
                        ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressView.setVisibility(View.INVISIBLE);

                            }
                        });
                    }
                }else {
                        ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressView.setVisibility(View.INVISIBLE);

                            }
                        });
                }
            }
        }).start();
    }

    void createProjectForImport1(final Bitmap bitmap) {
        progressView.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(((MainActivity) getActivity()).createProject(bitmap)) {
                            initSubProjects(false);
                            int count = project.getSubProjects().size();
                            adapter = new SubProjectsAdapter(project);
                            pager.setAdapter(null);
                            pager.setAdapter(adapter);
                        }
                        progressView.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }, 200);

    }
    void shareToEmail() {
        try {
            progressView.setVisibility(View.INVISIBLE);
            Bitmap b = ImageHelper.readImage(subProjectBeingEditted.getThumbnailPath(), false);
            Bitmap watermarkBitmap = ImageHelper.getBitmapFromResources(getActivity(), R.drawable.snm_watermark);
            watermarkBitmap = ImageHelper.resizeBitmap(watermarkBitmap, watermarkBitmap.getWidth(), (int)Utils.dipToPixels(MainApplication.getContext(),110));
            final Bitmap bitmapWithWaterMark = ImageHelper.addWaterMark(b, watermarkBitmap, Gravity.TOP | Gravity.RIGHT);

            File file = new File(ImageHelper.saveBitmapToExternalStorage(bitmapWithWaterMark, "temp.png"));
            Context context = getActivity();
            if (file.exists()) {
                Uri contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, file);
//                Uri contentUri = FileProvider.getUriForFile(context, "com.embrace.it.beskrivapp.fileprovider", file);
                if (contentUri != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareIntent.setDataAndType(contentUri, getActivity().getContentResolver().getType(contentUri));
                    shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                    shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                    startActivity(Intent.createChooser(shareIntent, "Choose an app"));
                }
            }
        } catch (IllegalStateException e) {
        } catch (Exception e) {
        }
        progressView.setVisibility(View.INVISIBLE);
    }


    void sharePDF(String path) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        File file = new File(path);
        Uri uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID, file);
        intent.setDataAndType(uri, getActivity().getContentResolver().getType(uri));
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
        startActivity(Intent.createChooser(intent, "Choose an app"));
        PackageManager pm = getActivity().getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            startActivity(intent);
        }
    }

    void createPDF(){


        progressView.setVisibility(View.VISIBLE);
final String projectId = project.getProjectId();
        new Thread(new Runnable() {
            @Override
            public void run() {
                PdfCreator pdf = new PdfCreator();
              final  String path = pdf.createPdf(projectId);

                ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressView.setVisibility(View.INVISIBLE);
                        sharePDF(path);
                    }
                });
            }
        }).start();
    }

    void shareImage() {
        Drawable drawable = getResources().getDrawable(R.drawable.earth_day, getActivity().getTheme());
        Bitmap b = ImageHelper.drawableToBitmap(drawable);
        File file = new File(ImageHelper.saveBitmapToExternalStorage(b, "temp.png"));
        Context context = getActivity();
        if (file.exists()) {
            Uri contentUri = FileProvider.getUriForFile(context, "com.embrace.it.beskrivapp.fileprovider", file);

            if (contentUri != null) {

                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                shareIntent.setDataAndType(contentUri, getActivity().getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
//            shareIntent.setData(Uri.parse("mailto:"));
                startActivity(Intent.createChooser(shareIntent, "Choose an app"));

            }
        }
    }

    void deleteSubProject() {
        try {
            int index = pager.getCurrentItem();
            int count = project.getSubProjects().size();
            SubProject sub = project.getSubProjects().get(pager.getCurrentItem());
            String IdOfDeletedSubitem = sub.getSubProjectId();
            if (DBController.deleteSubProject(sub.getSubProjectId())) {
                subProjectId = null;
                initSubProjects(false);
                count = project.getSubProjects().size();
                adapter = new SubProjectsAdapter(project);
                pager.setAdapter(null);
                pager.setAdapter(adapter);
                index--;


                if (project.getSubProjects().size() <= 0) {
                    ((MainActivity) getActivity()).popFragment(this);
                    ((MainActivity) getActivity()).selectSubProejct("empty");
                } else if (IdOfDeletedSubitem.equals(subProjectIdFromMain)) {
                    sub = project.getSubProjects().get(pager.getCurrentItem());
                    ((MainActivity) getActivity()).selectSubProejct(sub.getSubProjectId());
                }

                if (index < 0 || (project.getSubProjects() != null && project.getSubProjects().size() == 1)) {
                    index = 0;
                    pager.setCurrentItem(index);
                    setStickerCounts(index);
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setStickerCounts(position);
    }

    void setStickerCounts(int position) {

        try {
            SubProject sub = project.getSubProjects().get(position);
            int menu1Count = 0, menu2Count = 0, menu3Count = 0, menu4Count = 0;
            if (sub.getProjectItems() != null) {
                for (ProjectItem item : sub.getProjectItems()) {

                    switch (Utils.getToolType(item.getItemType())) {
                        case Sign_Tool:
                        case Markings_Tool:
                        case Icon_Tool:
                            menu1Count++;
                            break;
                        case Draw_Tool:
                        case Draw_Area:
                        case Shape_Tool:
                            menu2Count++;
                            break;
                        case Angle_Tool:
                        case Brackets_Tool:
                        case Count_Tool:
                            menu3Count++;
                            break;
                        case Symbol_Tool:
                        case Text_Area_Tool:
                            menu4Count++;
                            break;

                        default:
                            break;
                    }
                }
            }

            tvMenu1.setText("" + menu1Count);
            tvMenu2.setText("" + menu2Count);
            tvMenu3.setText("" + menu3Count);
            tvMenu4.setText("" + menu4Count);
        }catch(Exception e){}
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ThumbnailSavedObservable) {
            try {
                Bundle bundle = (Bundle) arg;
//                initSubProjects(false)
                int index = pager.getCurrentItem();
                pager.setAdapter(null);
                pager.setAdapter(adapter);
                pager.setCurrentItem(index);
            } catch (Exception e) {
            }
        }
    }

    private int getIndexOfSubProject(String subProjectId){
        int index = 0;
        for(SubProject sub : project.getSubProjects())
        {
            if(sub.getSubProjectId().equals(subProjectId))
            {
                return index;
            }
            index++;
        }
        return -1;
    }
    public static class MyDialog extends DialogFragment {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(getString(R.string.would_you_like_to_delete_this))
                    .setPositiveButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    })
                    .setNegativeButton(R.string.Yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    ((SubProjectsFragment) getTargetFragment()).deleteSubProject();
                                }
                            })
                    .create();
        }
    }


    public static class NewTitleDialogFragment extends DialogFragment implements View.OnClickListener {

        private SubProjectsFragment parent;
        EditText editText;
        public String oldTitle = null;
        public boolean isEdittingTitle = false;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v = inflater.inflate(
                    R.layout.dialoge_fragment_camera, container, false);
            editText = (EditText) v.findViewById(R.id.et_dialog_title);
            Button btnCancel = (Button) v.findViewById(R.id.bt_dialog_cancel);
            Button btnSave = (Button) v.findViewById(R.id.bt_dialog_save);
            btnCancel.setOnClickListener(this);
            btnSave.setOnClickListener(this);
            editText.setHint("Enter name for image");
            if (oldTitle != null)
                editText.setText(oldTitle);
            return v;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog d = super.onCreateDialog(savedInstanceState);
            parent = (SubProjectsFragment) getParentFragment();
            d.setCancelable(false);
            return d;

        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationRightToLeft;
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        @Override
        public void onClick(View v) {
            this.dismiss();
            if (v.getId() == R.id.bt_dialog_cancel) {

            } else if (v.getId() == R.id.bt_dialog_save) {
                String filename = editText.getText().toString();
                if (isEdittingTitle)
                    parent.updateTitle(filename);
                else
                    parent.duplicateImage(filename);
            }
        }
    }


    class SubProjectsAdapter extends PagerAdapter {

        List<SubProject> dataList;

        @Override
        public int getCount() {

            int count = 0;
            if (dataList != null)
                count = dataList.size();
            return count;
        }

        public SubProjectsAdapter(Project project) {
            if (project != null)
                dataList = project.getSubProjects();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v = getViewForPagerAtPosition(container, container.getContext(), position, dataList);
            ((ViewPager) container).addView(v);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }


        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }
    }



    public static class DeleteAlertDialog extends DialogFragment implements View.OnClickListener {

        private SubProjectsFragment parent;
        TextView tvTitle;
        String title = "";
        Button btnDelete;
        Button btnCancel;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v = inflater.inflate(
                    R.layout.dialoge_fragment_alert, container, false);
            tvTitle = (TextView) v.findViewById(R.id.tv_dialog_title);
            tvTitle.setText(getString(R.string.would_you_like_to_delete_this));
            btnDelete = (Button) v.findViewById(R.id.bt_dialog_first);
            btnCancel = (Button) v.findViewById(R.id.bt_dialog_second);
            btnCancel.setText(getString(R.string.cancel));
            btnDelete.setText(getString(R.string.delete));
            btnCancel.setOnClickListener(this);
            btnDelete.setOnClickListener(this);
            return v;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog d = super.onCreateDialog(savedInstanceState);
            parent = (SubProjectsFragment) getParentFragment();
            d.setCancelable(false);
            return d;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationTopToDown;
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        }

        @Override
        public void onClick(View v) {
            if (v.equals(btnCancel)) {
                this.dismiss();
            } else if (v.equals(btnDelete)) {
                ((SubProjectsFragment) getTargetFragment()).deleteSubProject();
                this.dismiss();
            }
        }
    }
}
