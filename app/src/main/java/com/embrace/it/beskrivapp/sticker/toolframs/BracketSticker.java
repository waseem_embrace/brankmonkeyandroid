package com.embrace.it.beskrivapp.sticker.toolframs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.DrawableSticker;
import com.embrace.it.beskrivapp.sticker.StickerUtils;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

import java.util.List;

/**
 * @author wupanjie
 */
public class BracketSticker extends DrawableSticker {

    float circleRadius = 30;
    private int colorIndex;
    private int typeIndex;
    private List<PointF> drawingPoints;

    public BracketSticker(Drawable drawable) {
        super(drawable);
        colorIndex = 0;
        typeIndex = 0;
        setUserIntractions(true, true, false);
    }

    public BracketSticker(List<PointF> drawingPoints, ToolData data) {
        super();
        colorIndex = 0;
        typeIndex = 0;
        this.drawingPoints = drawingPoints;
        setUserIntractions(true, true, false);
        if (data != null) {
            setData(data);
            init();
        }
    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }

    @Override
    public void itemUpdated(int mainIndex, int childIndex) {
        if (mainIndex < 0 && childIndex < 0) {
            mainIndex = 0;
            childIndex = 0;
        }
        ToolData data = getData();
        if (data != null) {
            switch (mainIndex) {
                case 0:
                    this.colorIndex = childIndex;
                    Drawable drawable = getDrawable();
                    if (drawable == null)
                        drawable = getNewDrawable();
                    drawable.setTint(getMyColor());
                    setDrawable(drawable);
                    break;
                case 1:

                    this.typeIndex = childIndex;
                    Drawable drawable1 = getNewDrawable();
                    drawable1.setTint(getMyColor());
                    setDrawable(drawable1);
                    break;

            }
        }
    }


    //Used only the first time the sticker is being created when its drawn by user.
    private RectF firstTimeCalculatedFram;

    @Override
    public RectF getInitialFrame() {
        return firstTimeCalculatedFram;
    }

    private Drawable getNewDrawable() {
        RectF rect = StickerUtils.calculateBounds(drawingPoints, circleRadius); //rect = left,top,width,height
        RectF rectWithMargins = new RectF(rect.left - marginImage, (rect.top - marginImage), rect.left + rect.right + marginImage, (int) (rect.top + rect.bottom + marginImage));
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap((int) (rect.left + rect.right + marginImage), (int) (rect.top + rect.bottom + marginImage), conf); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bmp);
        initPaint();
        //TODO: change the bellow commented line
//          drawCanvas(canvas, this.drawingPoints);
        drawCanvas1(canvas, this.drawingPoints);
        ///

        Bitmap resizedBitmap = getCroppedBitmap(bmp, rectWithMargins);
        Drawable drawable = new BitmapDrawable(MainApplication.getContext().getResources(), resizedBitmap);
        firstTimeCalculatedFram = rectWithMargins;
        return drawable;
    }

    private int getMyColor() {
        ToolItemData icons = data.getEditTools().get(0);
        return ContextCompat.getColor(MainApplication.getContext(), icons.getToolItemData().get(colorIndex));
    }

    private int getMyType() {
        ToolItemData icons = data.getEditTools().get(1);
        return icons.getToolItemData().get(typeIndex);
    }

    private void initPaint() {
        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setColor(getMyColor());
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(4f);

        fillPaint = new Paint();
        fillPaint.setAntiAlias(true);
        fillPaint.setColor(getMyColor());
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setStrokeWidth(4f);
    }

    Paint linePaint;
    Paint fillPaint;

    private Bitmap getIcon() {
        return BitmapFactory.decodeResource(MainApplication.getContext().getResources(), getMyType());
    }

    void drawCanvas(Canvas canvas, List<PointF> drawingPoints) {
        if (drawingPoints.size() == 1) {
            PointF p = drawingPoints.get(0);
        } else {
            PointF p1 = drawingPoints.get(0);
            PointF p2 = drawingPoints.get(1);
            Bitmap icon, iconTop, iconBottom;
            if (p1.x < p2.x) {
                PointF ptemp = p2;
                p2 = p1;
                p1 = ptemp;
            }
            float sweepAngle = -(float) StickerUtils.angleBetween(p2, p1, new PointF(p2.x, p2.y - 100));
            switch (this.typeIndex) {
                case 0:
                    Path path = createShape(drawingPoints);
                    canvas.drawPath(path, linePaint);
                    break;
                case 1:
                    icon = getIcon();
                    iconTop = ImageHelper.resizeBitmap(icon, 12, 4);
                    iconBottom = ImageHelper.rotateBitmap(iconTop, 180);
                    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
                    canvas.drawBitmap(iconTop, p1.x - iconTop.getWidth() / 2, p1.y - iconTop.getHeight(), new Paint(Paint.ANTI_ALIAS_FLAG));
                    canvas.drawBitmap(iconBottom, p2.x - iconTop.getWidth() / 2, p2.y, new Paint(Paint.ANTI_ALIAS_FLAG));
                    break;
                case 2:
                    setCornerIcons(canvas, p1, p2, 40, 40, sweepAngle);
                    break;
                case 3:
                    setCornerIcons(canvas, p1, p2, 40, 40, sweepAngle);
                    break;
                case 4:
                    setCornerIcons(canvas, p1, p2, 40, 40, sweepAngle);
                    break;
            }
        }
    }

    void setCornerIcons(Canvas canvas, PointF p1, PointF p2, float widthIcon, float heightIcon, float sweepAngle) {
        float widthPadding = 0, heightPadding = 0;
        if (sweepAngle > 0 && sweepAngle <= 90) {
            widthPadding = ((((float) widthIcon) / 90f) * ((float) sweepAngle) / 2f);
            heightPadding = ((((float) heightIcon) / 90f) * ((float) sweepAngle) / 2f);

        } else if (sweepAngle > 90 && sweepAngle <= 180) {
            float angle = sweepAngle - 90;
            widthPadding = ((((float) widthIcon) / 90f) * ((float) angle) / 2f);
            widthPadding = 0;
            heightPadding = ((((float) heightIcon) / 90f) * ((float) sweepAngle) / 2f);
//            heightPadding =0;

        }

        if (this.typeIndex == 3) {

        }
        Bitmap icon = getIcon();
        icon = ImageHelper.resizeBitmap(icon, (int) widthIcon, (int) heightIcon);
        Bitmap iconTop = ImageHelper.rotateBitmap(icon, sweepAngle);
        Bitmap iconBottom = ImageHelper.rotateBitmap(iconTop, 180);
//          canvas.drawBitmap(iconTop, widthPadding + p1.x - iconTop.getWidth() / 2, heightPadding + p1.y - iconTop.getHeight() , new Paint(Paint.ANTI_ALIAS_FLAG));
//        canvas.drawBitmap(iconBottom, p2.x - widthPadding - iconTop.getWidth() / 2, p2.y - heightPadding + iconTop.getHeight()/10, new Paint(Paint.ANTI_ALIAS_FLAG));

        canvas.drawBitmap(iconTop, widthPadding + p1.x - iconTop.getWidth() / 2, heightPadding + p1.y - iconTop.getHeight(), new Paint(Paint.ANTI_ALIAS_FLAG));
        canvas.drawBitmap(iconBottom, p2.x - widthPadding - iconTop.getWidth() / 2, p2.y - heightPadding, new Paint(Paint.ANTI_ALIAS_FLAG));
        canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
//

    }


    void drawCanvas1(Canvas canvas, List<PointF> drawingPoints) {
        if (drawingPoints.size() == 1) {
            PointF p = drawingPoints.get(0);
        } else {
            PointF p1 = drawingPoints.get(0);
            PointF p2 = drawingPoints.get(1);
            Bitmap icon, iconTop, iconBottom;
//            if (p1.x < p2.x) {
//                PointF ptemp = p2;
//                p2 = p1;
//                p1 = ptemp;
//            }
            float angle = (float) StickerUtils.angleToXAxis(p2, p1);
//            if (angle < 90 && angle > -180 ) {
//                PointF ptemp = p2;
//                p2 = p1;
//                p1 = ptemp;
//            }
//            if (angle < 0) {
//                angle = angle + 180;
//            }


            float widthIcon = 40, heightIcon = 40;

            switch (this.typeIndex) {
                case 0:
                    Path path = createShape(drawingPoints);
                    canvas.drawPath(path, linePaint);
                    break;
                case 1:
                    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
                    drawRectangleFilled(canvas, p1, angle, widthIcon, 10, false);
                    drawRectangleFilled(canvas, p2, angle, widthIcon, 10, true);
                    break;
                case 2:
                    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
                    drawTriangle(canvas, p1, angle, widthIcon, heightIcon, true, false);
                    drawTriangle(canvas, p2, angle, widthIcon, heightIcon, true, true);
                    break;
                case 3:
                    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
                    drawTriangle(canvas, p1, angle, widthIcon, heightIcon, false, false);
                    drawTriangle(canvas, p2, angle, widthIcon, heightIcon, false, true);
                    break;
                case 4:
                    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, linePaint);
                    drawRectangleFilled(canvas, p1, angle, widthIcon, heightIcon, false);
                    drawRectangleFilled(canvas, p2, angle, widthIcon, heightIcon, true);
                    break;
            }
        }
    }


    float marginImage = 10;

    public void drawRectangleFilled(Canvas c, PointF p, float angle, float width, float height, boolean isFlipped) {
        float x1 = p.x + (float) ((width / 2f) * Math.cos(Math.toRadians(angle - 90)));
        float y1 = p.y + (float) ((width / 2f) * Math.sin(Math.toRadians(angle - 90)));

        float x2 = p.x + (float) ((width / 2f) * Math.cos(Math.toRadians(angle + 90)));
        float y2 = p.y + (float) ((width / 2f) * Math.sin(Math.toRadians(angle + 90)));

        if (isFlipped) {
            angle = angle + 180;
        }
        float x3 = x2 + (float) (height * Math.cos(Math.toRadians(angle + 180)));
        float y3 = y2 + (float) (height * Math.sin(Math.toRadians(angle + 180)));

        float x4 = x1 + (float) (height * Math.cos(Math.toRadians(angle + 180)));
        float y4 = y1 + (float) (height * Math.sin(Math.toRadians(angle + 180)));

        Path path = new Path();
        path.moveTo(x1, y1);
        path.lineTo(x2, y2);
        path.lineTo(x3, y3);
        path.lineTo(x4, y4);
        path.lineTo(x1, y1);
        c.drawPath(path, fillPaint);
    }

    public void drawTriangle(Canvas c, PointF point, float angle, float width, float height, boolean isFilled, boolean isFlipped) {
      PointF p = new PointF(point.x,point.y);
//        if (!isFilled) {

            float angleTemp = angle;
            if (!isFlipped) {
                angleTemp = angleTemp + 180;
            }
            float x = p.x + (float) (height * Math.cos(Math.toRadians(angleTemp)));
            float y = p.y + (float) (height * Math.sin(Math.toRadians(angleTemp)));
            p.x = x;
            p.y = y;
//        }
        float x1 = p.x + (float) ((width / 2f) * Math.cos(Math.toRadians(angle - 90)));
        float y1 = p.y + (float) ((width / 2f) * Math.sin(Math.toRadians(angle - 90)));

        float x2 = p.x + (float) ((width / 2f) * Math.cos(Math.toRadians(angle + 90)));
        float y2 = p.y + (float) ((width / 2f) * Math.sin(Math.toRadians(angle + 90)));

        if (isFlipped) {
            angle = angle + 180;
        }
        float x3 = x2 + (float) (height * Math.cos(Math.toRadians(angle)));
        float y3 = y2 + (float) (height * Math.sin(Math.toRadians(angle)));

        float x4 = x1 + (float) (height * Math.cos(Math.toRadians(angle)));
        float y4 = y1 + (float) (height * Math.sin(Math.toRadians(angle)));

        Path path = new Path();
        path.moveTo(x1, y1);
        path.lineTo((x3 + x4) / 2, (y3 + y4) / 2);
        path.lineTo(x2, y2);

        if (isFilled) {
            c.drawPath(path, fillPaint);
        } else {
            c.drawPath(path, linePaint);
        }
    }

    public Bitmap getResizedImage(Bitmap bitmap, List<PointF> circlePoints, float circleRadius, RectF rect) {
        float left = (rect.left - marginImage) < 0 ? 0 : rect.left - marginImage;
        float top = (rect.top - marginImage) < 0 ? 0 : rect.top - marginImage;
        float right = (rect.right + marginImage) > bitmap.getWidth() ? bitmap.getWidth() : rect.right + marginImage;
        float bottom = (rect.bottom + marginImage) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom + marginImage;

        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) right, (int) bottom);
        return resizedbitmap;
    }

    private Bitmap getCroppedBitmap(Bitmap bitmap, RectF rect) {
        float left = (rect.left) < 0 ? 0 : rect.left;
        float top = (rect.top) < 0 ? 0 : rect.top;
        float right = (rect.right) > bitmap.getWidth() ? bitmap.getWidth() : rect.right;
        float bottom = (rect.bottom) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom;

        //if rect is updated then, update the object
        rect.left = left;
        rect.right = right;
        rect.top = top;
        rect.bottom = bottom;
        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) (right - left), (int) (bottom - top));
        return resizedbitmap;
    }

    void drawCornerLines(Canvas c, PointF p1, PointF p2) {


        // Ready our variables and pick arbitrary points for the starter points.

        PointF midpoint = new PointF();
        PointF p3 = new PointF();
        PointF p4 = new PointF();

        double slope;


        // Calculate the slope of the original line. (y2 - y1) / (x2 - x1) = slope
        slope = ((double) (p2.y - p1.y) / (double) (p2.x - p1.x));

        // Perpendicular lines have a slope of (-1 / originalSlope) -- the negative reciprocal.
        slope = -1 / slope;

        // Formula to find the midpoint.
        midpoint.x = (p1.x + p2.x) / 2;
        midpoint.y = (p1.y + p2.y) / 2;

        p3.x = midpoint.x;
        p3.y = midpoint.y;

        // Find the y-intercept of this equation. y=mx + b
        double b = -slope * midpoint.x + midpoint.y;

        // Finally start calculating the final point.
        // Add the length of the line to X.
        p4.x = midpoint.x + 8;

        // Now plug our slope, intercept, and new X into y=mx + b
        p4.y = (int) (slope * (midpoint.x + 8) + b);

        // Draw that line!
        c.drawLine(p3.x, p3.y, p4.x, p4.y, linePaint);
//        surface.DrawLine(new Pen(Brushes.Blue), p3, p4);
    }

    void drawCornerLines1(Canvas c, PointF p1, PointF p2) {

//        PointF midpoint = new PointF();
        PointF p3 = new PointF();
        PointF p4 = new PointF();

        double slope;

        slope = ((double) (p2.y - p1.y) / (double) (p2.x - p1.x));
        slope = -1 / slope;
        p3.x = p1.x;
        p3.y = p1.y;
        double b = -slope * p1.x + p1.y;

        // Finally start calculating the final point.
        // Add the length of the line to X.
        p4.x = p1.x + 8;

        // Now plug our slope, intercept, and new X into y=mx + b
        p4.y = (int) (slope * (p1.x + 8) + b);

        // Draw that line!
        c.drawLine(p3.x, p3.y, p4.x, p4.y, linePaint);
//        surface.DrawLine(new Pen(Brushes.Blue), p3, p4);
    }


    /*
        Create Bracket shape
     */
    private Path createShape(List<PointF> drawingPoints) {
        PointF p1 = drawingPoints.get(0);
        PointF p2 = drawingPoints.get(1);
        PointF o = divide(plus(p1, p2), 2f);
        PointF u = minus(p2, o);
        PointF v = turn90(u);
        PointF c1 = minus(minus(o, times(u, 0.9f)), times(v, 0.15f));
        PointF c2 = minus(plus(o, times(u, 0.9f)), times(v, 0.15f));
        PointF midpoint = minus(o, times(v, 0.05f));
        PointF midP1 = minus(o, times(u, 0.08f));
        PointF midP2 = plus(o, times(u, 0.08f));
        Path path = new Path();
        path.moveTo(p1.x, p1.y);
        path.quadTo(c1.x, c1.y, midP1.x, midP1.y);
        path.lineTo(midpoint.x, midpoint.y);
        path.lineTo(midP2.x, midP2.y);
        path.quadTo(c2.x, c2.y, p2.x, p2.y);
        return path;
    }


    private PointF plus(PointF p, PointF p2) {
        return new PointF(p.x + p2.x, p.y + p2.y);
    }

    private PointF minus(PointF p, PointF p2) {
        return new PointF(p.x - p2.x, p.y - p2.y);
    }

    private PointF times(PointF p, float f) {
        return new PointF(p.x * f, p.y * f);
    }

    private PointF divide(PointF p, float f) {
        return times(p, 1.0f / f);
    }


    private PointF turn90(PointF p) {
        return new PointF(-p.y, p.x);
    }


    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);
        projectItem.setItemType(toolType.getNumericValue());
        projectItem.getCharacteristics().add(new RealmFloat(this.colorIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.typeIndex));
        if (getFrame() != null) {
            projectItem.setFrame(getFrame());
        }
        projectItem.setDrawingPoints(drawingPoints);
        return projectItem;
    }

    @Override
    public void setProjectItem(ProjectItem p) {
        toolType = Utils.getToolType(p.getItemType());
        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        this.colorIndex = (int) characterstics[0];
        this.typeIndex = (int) characterstics[1];
        if (p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
            setFrame(fram);
        }
        drawingPoints = RealmHelper.getListPointF(p.getDrawingPoints());
        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);
        itemUpdated(1, this.typeIndex);
    }
}
