package com.embrace.it.beskrivapp.sticker;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import com.embrace.it.beskrivapp.database.ProjectItem;

/**
 * @author wupanjie
 */
public class DrawingSticker extends Sticker {

    private Drawable drawable;
    private Rect realBounds;

    public DrawingSticker(Drawable drawable) {
        this.drawable = drawable;
        this.matrix = new Matrix();
        realBounds = new Rect(0, 0, getWidth(), getHeight());
    }

    @Override
    public Drawable getDrawable() {
        return drawable;
    }

    @Override
    public void itemUpdated(int mainIndex, int chidlIndex) {

    }

    @Override
    public void setProjectItem(ProjectItem p) {

    }

    @Override
    public ProjectItem getProjectItem() {
        return null;
    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        return null;
    }

    @Override
    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.concat(matrix);
        drawable.setBounds(realBounds);
        drawable.draw(canvas);
        canvas.restore();
    }

    @Override
    public RectF getInitialFrame() {
        return null;
    }
    @Override
    public int getWidth() {
        return drawable.getIntrinsicWidth();
    }

    @Override
    public int getHeight() {
        return drawable.getIntrinsicHeight();
    }

    @Override
    public void release() {
        super.release();
        if (drawable != null) {
            drawable = null;
        }
    }
}
