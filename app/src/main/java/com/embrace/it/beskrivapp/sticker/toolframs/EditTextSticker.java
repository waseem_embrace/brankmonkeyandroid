package com.embrace.it.beskrivapp.sticker.toolframs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.TextSticker;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

import java.util.List;

/**
 * Customize your sticker with text and image background.
 * You can place some text into a given region, however,
 * you can also add a plain text sticker. To support text
 * auto resizing , I take most of the code from AutoResizeTextView.
 * See https://adilatwork.blogspot.com/2014/08/android-textview-which-resizes-its-text.html
 * Notice: It's not efficient to add long text due to too much of
 * StaticLayout object allocation.
 * <p>
 * Created by liutao on 30/11/2016.
 */

public class EditTextSticker extends TextSticker {

    //    private int imageIndex;

    private int textSizeIndex = 2;
    private int colorIndex;


    public EditTextSticker(Context context, Drawable drawable) {
        super(context, drawable);
        setToolType(Constants.ToolType.Text_Area_Tool);
        colorIndex = 0;
        textSizeIndex = 2;
        values = new float[9];
        setUserIntractions(true, true, true);
    }

    private float[] values;


    @Override
    public void scaleTextSize() {
        try {
            if (getMatrix() != null) {
                if (values == null)
                    values = new float[9];
                getMatrix().getValues(values);
                if (values != null) {
                    scaleX = values[Matrix.MSCALE_X];
                    scaleY = values[Matrix.MSCALE_Y];
                    setTextSize(getMyTextSize());
                    values[Matrix.MSCALE_X] = 1.0f;
                    values[Matrix.MSCALE_Y] = 1.0f;
//                    matrixText.setValues(values);
                }
            }
        } catch (Exception e) {
        }
    }


    public int getTextSizeScaled() {
        return getMyTextSize();
    }
    public int getTextColor() {
        return getMyColor();
    }
    @Override
    public void scaleMatrixText() {
//        try {
//            if (getMatrix() != null) {
//                if (values == null)
//                    values = new float[9];
//                getMatrix().getValues(values);
//                if (values != null) {
//                    values[Matrix.MSCALE_X] = 1.0f;
//                    values[Matrix.MSCALE_Y] = 1.0f;
//                    matrixText.setValues(values);
//                }
//            }
//        } catch (Exception e) {
//        }
    }

    public EditTextSticker(Context context, String text, ToolData data) {
        super(context, null);
        setToolType(Constants.ToolType.Text_Area_Tool);
        colorIndex = 0;
        textSizeIndex = 2;
        values = new float[9];
        Drawable d = getDrawable();
        if (d == null) {
            d = ContextCompat.getDrawable(context, R.drawable.sticker_transparent_background);
        }
        setUserIntractions(true, true, true);
        if (data != null) {
            setData(data);
//            invalidateText();
//            init();
        }
    }

    @Override
    public void itemUpdated(int mainIndex, int childIndex) {
        ToolData data = getData();

        if( mainIndex < 0 && childIndex < 0) {
            mainIndex = 0;
            childIndex = 2;
        }
        if (data != null) {
            switch (mainIndex) {
                case 1:
                    this.colorIndex = childIndex;
                    Drawable drawable = getDrawable();
//                    if (drawable == null)
//                        drawable = getNewDrawable();
//                    drawable.setTint(getMyColor());
//                    setDrawable(drawable);
                    setTextColor(getMyColor());
                    invalidateText();

                    break;
                case 0:
                    this.textSizeIndex = childIndex;
//                    Drawable drawable1 = getNewDrawable();
//                    drawable1.setTint(getMyColor());
//                    setDrawable(drawable1);
                    setTextSize(getMyTextSize());
                    invalidateText();

                    break;
            }
        }
    }

    private int getMyColor() {
        ToolItemData icons = data.getEditTools().get(1);
        return ContextCompat.getColor(MainApplication.getContext(), icons.getToolItemData().get(colorIndex));
    }

    private int getMyTextSize() {
        ToolItemData icons = data.getEditTools().get(0);
        float size = icons.getToolItemData().get(textSizeIndex);
//        return (int) (size / scaleX);
        return (int) (size);
    }

    private void initPaint() {
        textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(getMyColor());
        textPaint.setTextSize(getMyTextSize());
    }
private Boolean isEdittingMode = false;
    public void edittingMode(Boolean b){
        isEdittingMode = b;
        if(isEdittingMode){
            setTextColor(Color.TRANSPARENT);
        }else{
            setTextColor(getMyColor());
        }
    }
    TextPaint textPaint;

    void drawCanvas(Canvas canvas) {
        canvas.drawText(mText, 0, 0, textPaint);
    }

    float marginImage = 10;

    public Bitmap getResizedImage(Bitmap bitmap, List<PointF> circlePoints, float circleRadius, RectF rect) {
        float left = (rect.left - marginImage) < 0 ? 0 : rect.left - marginImage;
        float top = (rect.top - marginImage) < 0 ? 0 : rect.top - marginImage;
        float right = (rect.right + marginImage) > bitmap.getWidth() ? bitmap.getWidth() : rect.right + marginImage;
        float bottom = (rect.bottom + marginImage) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom + marginImage;

        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) right, (int) bottom);
        return resizedbitmap;
    }

    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);

        getMatrixText().getValues(values);
        projectItem.setMatrixValues2(values);

        projectItem.setItemType(toolType.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(this.textSizeIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.colorIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.scaleX));
        projectItem.getCharacteristics().add(new RealmFloat(this.scaleY));

        projectItem.setTextValue(getText());
        if (getFrame() != null) {
//            projectItem.setFrame(getFrame());
        }
        return projectItem;
    }

    @Override
    public void setProjectItem(ProjectItem p) {
        toolType = Utils.getToolType(p.getItemType());

        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        this.textSizeIndex = (int) characterstics[0];
        this.colorIndex = (int) characterstics[1];
        this.scaleX = (int) characterstics[2];
        this.scaleY = (int) characterstics[3];

        if (p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
//            setFrame(fram);
        }

        this.setText(p.getTextValue().toString());
        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);

        values = RealmHelper.getArrayF(p.getMatrixValues2());
        getMatrixText().setValues(values);

        itemUpdated(0, this.textSizeIndex);
        setTextColor(getMyColor());
        updateRects();
        invalidateText();
    }

    public void addCharacter(char c) {
        String str = this.getText();
        if (str != null) {
            str = str + c;
        } else {
            str = "" + c;
        }
        this.setText(str);
        if (base != null)
            base.append(c);
        invalidateText();
    }
    public void addString(String c) {
        String str = this.getText();
        if (str != null) {
            str = str + c;
        } else {
            str = "" + c;
        }
        this.setText(str);
        if (base != null)
            base.append(c);
        invalidateText();
    }

    public void deleteLastCharacter() {
        String str = this.getText();
        if (str != null && str.length() > 1) {
            str = str.substring(0, str.length() - 1);
        }else{
            str = " ";
        }
        this.setText(str);
        invalidateText();

        if (base != null)
            base.delete(base.length() - 2, base.length() - 1);
    }

    public Matrix getMatrixText() {
        return matrixText;
    }
}
