package com.embrace.it.beskrivapp.database;

import io.realm.RealmObject;

/**
 * Created by waseem on 3/2/17.
 */

public class RealmFloat extends RealmObject {
    private Float val;
    public RealmFloat(){
    }

    public RealmFloat(Float value) {
        this.val = value;
    }

    public RealmFloat(Integer value) {
        this.val = (float)value;
    }

    public Float getValue() {
        return val;
    }

    public void setValue(Float value) {
        this.val = value;
    }


    @Override
    public String toString() {
        return ""+val;
    }
    public float toFloat() {
        return val;
    }
}