package com.embrace.it.beskrivapp.toolData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waseem on 2/14/17.
 */

public class ToolData {


    public ToolData() {
        editTools = new ArrayList<ToolItemData>();
    }

    public List<ToolItemData> getEditTools() {
        return editTools;
    }

    public void setEditTools(List<ToolItemData> editTools) {
        this.editTools = editTools;
    }

    public String getNonActiveText() {
        return nonActiveText;
    }

    public void setNonActiveText(String nonActiveText) {
        this.nonActiveText = nonActiveText;
    }

    public String getActiveText() {
        return activeText;
    }

    public void setActiveText(String activeText) {
        this.activeText = activeText;
    }

    public String getBeforeFirstClickText() {
        return beforeFirstClickText;
    }

    public void setBeforeFirstClickText(String beforeFirstClickText) {
        this.beforeFirstClickText = beforeFirstClickText;
    }
    public String getAfterFirstClickText() {
        return afterFirstClickText;
    }
    public void setAfterFirstClickText(String afterFirstClickText) {
        this.afterFirstClickText = afterFirstClickText;
    }
    public String getAfterSecondClickText() {
        return afterSecondClickText;
    }
    public void setAfterSecondClickText(String afterSecondClickText) {
        this.afterSecondClickText = afterSecondClickText;
    }
    public String getFinishText() {
        return finishText;
    }

    public void setFinishText(String finishText) {
        this.finishText = finishText;
    }

    String activeText;
    String afterFirstClickText;
    String afterSecondClickText;
    String beforeFirstClickText;
    String nonActiveText;
    String finishText;
    List<ToolItemData> editTools;

    public String getToolItemIntruction(int index){
        if( editTools != null && index >= 0 && index < editTools.size()){
            return editTools.get(index).getDescription();
        }
        return null;
    }
}
