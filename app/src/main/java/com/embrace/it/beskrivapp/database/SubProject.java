package com.embrace.it.beskrivapp.database;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by waseem on 2/9/17.
 */

public class SubProject extends RealmObject {
    public SubProject() {
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public RealmList<ProjectItem> getProjectItems() {
        return projectItems;
    }

    public void setProjectItems(RealmList<ProjectItem> projectItems) {
        this.projectItems = projectItems;
    }

    public ProjectMainItem getMainItem() {
        return mainItem;
    }

    public void setMainItem(ProjectMainItem mainItem) {
        this.mainItem = mainItem;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }


    public String getSubProjectId() {
        return subProjectId;
    }

    public void setSubProjectId(String subProjectId) {
        this.subProjectId = subProjectId;
    }

    public RealmString getGpsCoordinate() {
        return gpsCoordinate;
    }

    public void setGpsCoordinate(RealmString gpsCoordinate) {
        this.gpsCoordinate = gpsCoordinate;
    }

    public RealmString getDate() {
        return date;
    }

    public void setDate(RealmString date) {
        this.date = date;
    }

    public RealmString getTime() {
        return time;
    }

    public void setTime(RealmString time) {
        this.time = time;
    }

    private String projectTitle;
    private RealmList<ProjectItem> projectItems;
    private ProjectMainItem mainItem;
    private String thumbnailPath;
    private RealmString gpsCoordinate;
    private RealmString date;
    private RealmString time;
    @PrimaryKey
    private String subProjectId;
}
