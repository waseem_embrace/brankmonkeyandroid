package com.embrace.it.beskrivapp.commons.helpers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.Type;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.commons.ColorFilterGenerator;
import com.embrace.it.beskrivapp.commons.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by waseem on 2/15/17.
 */

public class ImageHelper {

    public static String DIRECTORY = "saved_images";

    public static String saveBitmapToInternalStorage(Bitmap bitmap, String name) {

        Context context = MainApplication.getContext();
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir(DIRECTORY, Context.MODE_PRIVATE);
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name = name + ".jpeg";

        File mypath = new File(directory, name);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.toString();
    }


    public static Bitmap readBitmapFromInternalStorage(String name,boolean isThumbnail) {

        Context context = MainApplication.getContext();
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir(DIRECTORY, Context.MODE_PRIVATE);
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name = name + ".jpeg";

        File mypath = new File(directory, name);
       return readImage(mypath.toString(),isThumbnail);
    }


    public static String saveBitmapToCache(Bitmap bitmap, String name) {

        Context context = MainApplication.getContext();
        File directory = context.getCacheDir();
        String[] list = name.split("/");
        name = list[list.length - 1];
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name = name + ".jpeg";
        File mypath = new File(directory, name);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.toString();
    }


    public static String saveBitmapToExternalCache(Bitmap bitmap, String name) {
        Context context = MainApplication.getContext();
        File directory = context.getExternalCacheDir();
        String[] list = name.split("/");
        name = list[list.length - 1];
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name = name + ".jpeg";
        File mypath = new File(directory, name);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.toString();
    }


    public static String saveBitmapToExternalStorage(Bitmap bitmap,String name){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/"+ DIRECTORY );
        myDir.mkdirs();
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name = name + ".jpeg";
        String fname = ""+ name;
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file.toString();
    }

    public static String saveBitmapToPath(Bitmap bitmap,String path,String name){
        File myDir = new File(path);
        myDir.mkdirs();
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name = name + ".jpeg";
        String fname = ""+ name;
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file.toString();
    }


    public static String saveBitmapToFileDirectoroy(Bitmap bitmap,String filepath){
        Context context = MainApplication.getContext();
        String root = context.getFilesDir() + File.separator + DIRECTORY;
//        File myDir = new File(root);
//        myDir.mkdirs();
        if (filepath.contains(".jpeg") || filepath.contains(".png") || filepath.contains(".bmp")) {
        } else
            filepath = filepath + ".jpeg";
        File file = new File (filepath);
        if (file.exists ()) {
            file.delete ();
            file = new File (filepath);
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file.toString();
    }



    public static Bitmap readImage(String path, boolean thumbnailQuality) {
        if (path == null) {
            return null;
        }
        Bitmap bmp = getSampleDownedBitmap(path, thumbnailQuality);
        if (bmp != null) {
            return bmp;
        }
        try {
            File f = new File(path);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File saveImage(Image mImage) {
        return saveImage(mImage, System.currentTimeMillis() + ".jpeg");
    }


    public static File saveImage(Image mImage, String name) {
        //TODO: uncomemnt
         /* deleteFile(name); */
        //

        if (name == null || name.equals(""))
            name = System.currentTimeMillis() + ".jpeg";
        Context context = MainApplication.getContext();
        ContextWrapper cw = new ContextWrapper(context);
        if (name.contains(".jpeg") || name.contains(".png") || name.contains(".bmp")) {
        } else
            name.concat(".jpeg");
        File directory = cw.getDir(DIRECTORY, Context.MODE_PRIVATE);
        File mypath = new File(directory, name);
        ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(mypath);
            output.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            mImage.close();
            if (null != output) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mypath;
    }


    public static Drawable getdrawable(int resourceId) {
        Drawable drawable = ContextCompat.getDrawable(MainApplication.getContext(), resourceId);
        return drawable;
    }

    public static Drawable getdrawable(String resourcePath) {
        Bitmap b = readImage(resourcePath, false);
        Drawable drawable = BitmapDrawable.createFromPath(resourcePath);
        return drawable;
    }

    public Drawable readFromAssets(Context c, String fileName) {
        try {
            // get input stream
            InputStream ims = c.getAssets().open(fileName);
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            return d;
        } catch (IOException ex) {
            return null;
        }
    }


    /**
     * @param bmp        input bitmap
     * @param contrast   0..10 1 is default
     * @param brightness -255..255 0 is default
     * @return new bitmap
     */
//    private static  Bitmap ret;
    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, Bitmap bmpRecycle, float contrast, float brightness) {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        try {
            if (bmpRecycle != null) {
//                bmpRecycle.recycle();
            }
        } catch (Exception e) {
        }

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);
        return ret;
    }
//    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, Bitmap bmpRecycle, float contrast, float brightness) {
//        bmp.setColorFilter(setBrightness(progress));
//
//
//        return ret;
//    }


    private static Paint paint = new Paint();
    private static  Canvas canvas = new Canvas();
    public static Bitmap changeBitmapBrightness(Bitmap bmp, Bitmap bmpRecycle, int brightness,int brightnessNet) {


        Bitmap ret= Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
        Canvas canvas = new Canvas(ret);
        if(brightness > 0) {
            paint.setColorFilter(new PorterDuffColorFilter( Color.argb(brightness, 255, 255, 255), PorterDuff.Mode.SRC_OVER));
        }else if(brightness < 0){
            paint.setColorFilter(new PorterDuffColorFilter(Color.argb((-brightness), 0, 0, 0), PorterDuff.Mode.SRC_ATOP));
        }
        if(brightness != 0) {
            canvas.drawBitmap(bmp, 0, 0, paint);
        }else{
            ret = bmp.copy(bmp.getConfig(),bmp.isMutable());
        }
//paint.setColorFilter(ColorFilterGenerator.adjustBrightness(brightness));
//        canvas.drawBitmap(bmp, 0, 0, paint);
        return ret;
    }


    public static Bitmap changeBitmapContrast(Bitmap bmp, Bitmap bmpRecycle, float contrast,float contrastNet) {

        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, 0,
                        0, contrast, 0, 0, 0,
                        0, 0, contrast, 0, 0,
                        0, 0, 0, 1, 0
                });

        try {
            if (bmpRecycle != null) {
//                bmpRecycle.recycle();
            }
        } catch (Exception e) {
        }

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);
        return ret;
/*        int mul = 0xFFFFFFFF;
        contrast = 0x00222222;
        Bitmap ret= Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
        Canvas canvas = new Canvas(ret);
        LightingColorFilter lightingColorFilter = new LightingColorFilter(mul, contrast);
        paint.setColorFilter(lightingColorFilter);

        if(contrast != 0) {
            canvas.drawBitmap(bmp, 0, 0, paint);
        }else{
            ret = bmp.copy(bmp.getConfig(),bmp.isMutable());
        }
        return ret;*/
    }

    public static PorterDuffColorFilter setBrightness(int progress) {
        if (progress >=    100)
        {
        int value = (int) (progress-100) * 255 / 100;

        return new PorterDuffColorFilter(Color.argb(value, 255, 255, 255), PorterDuff.Mode.SRC_OVER);

        }
        else
        {
        int value = (int) (100-progress) * 255 / 100;
        return new PorterDuffColorFilter(Color.argb(value, 0, 0, 0), PorterDuff.Mode.SRC_ATOP);


        }
        }
//    private Bitmap loadBitmap(int resource) {
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        return BitmapFactory.decodeResource(getResources(), resource, options);
//    }


    public static Drawable bitmapToDrawable(Bitmap b, Context c) {
        Drawable drawable = new BitmapDrawable(c.getResources(), b);
        return drawable;

    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable == null)
            return null;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
            Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
            Canvas canvas = new Canvas(bmOverlay);
            canvas.drawBitmap(bmp1, new Matrix(), null);
            canvas.drawBitmap(bmp2, 0, 0, null);
            return bmOverlay;

    }

    public static Bitmap addWaterMark(Bitmap baseBitmap, Bitmap watermarkBitmap, int gravity){
        Bitmap bmOverlay = Bitmap.createBitmap(baseBitmap.getWidth(), baseBitmap.getHeight(), baseBitmap.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(baseBitmap, new Matrix(), null);

        int left = 0 ,top = 10;
        switch(gravity){
            case (Gravity.TOP +  Gravity.RIGHT):
                 left = baseBitmap.getWidth() - watermarkBitmap.getWidth();
                break;

            case (Gravity.TOP +  Gravity.LEFT):
                 left = 0;
                break;
            case (Gravity.TOP +  Gravity.CENTER):
                 left = (baseBitmap.getWidth() - watermarkBitmap.getWidth())/2;
                break;

            case (Gravity.BOTTOM +  Gravity.RIGHT):
                top = watermarkBitmap.getHeight(); left = baseBitmap.getWidth() - watermarkBitmap.getWidth();
                break;
            case (Gravity.BOTTOM +  Gravity.LEFT):
                top = watermarkBitmap.getHeight();
                break;

            case (Gravity.BOTTOM +  Gravity.CENTER):
                top = watermarkBitmap.getHeight();left =(baseBitmap.getWidth() - watermarkBitmap.getWidth())/2;
                break;
            case (Gravity.CENTER +  Gravity.RIGHT):
                left =baseBitmap.getWidth() - watermarkBitmap.getWidth();
                top =(baseBitmap.getHeight() - watermarkBitmap.getHeight())/2;
                break;
            case (Gravity.CENTER +  Gravity.LEFT):
                top =(baseBitmap.getHeight() - watermarkBitmap.getHeight())/2;
                break;
            case Gravity.CENTER:
                left =(baseBitmap.getWidth() - watermarkBitmap.getWidth())/2;
                top =(baseBitmap.getHeight() - watermarkBitmap.getHeight())/2;
                break;
            case Gravity.TOP:
                left =(baseBitmap.getWidth() - watermarkBitmap.getWidth())/2;
                break;
            case Gravity.RIGHT:
                left =(baseBitmap.getWidth() - watermarkBitmap.getWidth());
                top =(baseBitmap.getHeight() - watermarkBitmap.getHeight())/2;
                break;
            case Gravity.LEFT:

                top =(baseBitmap.getHeight() - watermarkBitmap.getHeight())/2;
                break;
            case Gravity.BOTTOM:
                left =(baseBitmap.getWidth() - watermarkBitmap.getWidth())/2;
                top =watermarkBitmap.getHeight();
                break;
        }

        Paint paint = new Paint();
        paint.setAlpha(60);
        canvas.drawBitmap(watermarkBitmap, left, top, paint);
        return bmOverlay;
    }
    public static Bitmap getSampleDownedBitmap(String path, boolean thumbnailQuality) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;
        if (thumbnailQuality) {
            if (imageWidth > imageHeight) {
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenWidth() / 4, Utils.getScreenHeight() / 4);//if landscape
            } else {
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenHeight() / 4, Utils.getScreenWidth() / 4);//if portrait
            }
        } else {
            if (imageWidth > imageHeight) {
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenWidth(), Utils.getScreenHeight());//if landscape
            } else {
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenHeight(), Utils.getScreenWidth());//if portrait
            }
        }
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        return bitmap;
    }

    public static Bitmap getBitmapForPdf(String path) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;

            if (imageWidth > imageHeight) {
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenWidth()/2, Utils.getScreenHeight()/2);//if landscape
            } else {
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenHeight()/2, Utils.getScreenWidth()/2);//if portrait
            }

        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        return bitmap;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static boolean deleteFile(String path) {
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void addImageToGallary(Context context,Bitmap bitmap,String title,String description){

        MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, title , description);

    }
    public static Bitmap resizeBitmap(Bitmap b, int requiredWidth, int requiredHeight) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()), new RectF(0, 0, requiredWidth, requiredHeight), Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
    }

    public static Bitmap resizeBitmapWthScale(Bitmap b, float widthScale, float heightScale) {
        return resizeBitmap(b, (int)(b.getWidth() * widthScale), (int)(b.getHeight() * heightScale));
    }

    public static Bitmap resizeBitmapWithScale(Bitmap b, float scale) {
        return resizeBitmap(b, (int)(b.getWidth() * scale), (int)(b.getHeight() * scale));
    }

    public static Bitmap resizeBitmapWByWidth(Bitmap b, int width) {
        float scale = ((float)width)/((float)b.getWidth());
        return resizeBitmap(b, (int)(b.getWidth() * scale), (int)(b.getHeight() * scale));
    }

    public static Bitmap resizeBitmapWByHeight(Bitmap b, int height) {
        float scale = ((float)height)/((float)b.getHeight());
        return resizeBitmap(b, (int)(b.getWidth() * scale), (int)(b.getHeight() * scale));
    }

    public static Bitmap rotateBitmap(Bitmap b, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
    }


    public static Bitmap cropeBitmap(Bitmap bitmap, RectF rect) {
        float left = (rect.left) < 0 ? 0 : rect.left ;
        float top = (rect.top ) < 0 ? 0 : rect.top;
        float right = (rect.right ) > bitmap.getWidth() ? bitmap.getWidth() : rect.right;
        float bottom = (rect.bottom ) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom;
        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) (right - left), (int) (bottom - top));
        return resizedbitmap;
    }

    public static Bitmap blurBitmap(Bitmap bitmap, float radius, Context context) {


        radius = (radius > 25)? 25:(radius < 0)?0:radius;

        //Create renderscript
        RenderScript rs = RenderScript.create(context);

        //Create allocation from Bitmap
        Allocation allocation = Allocation.createFromBitmap(rs, bitmap);

        Type t = allocation.getType();

        //Create allocation with the same type
        Allocation blurredAllocation = Allocation.createTyped(rs, t);

        //Create script
        ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        //Set blur radius (maximum 25.0)
        blurScript.setRadius(radius);
        //Set input for script
        blurScript.setInput(allocation);
        //Call script for output allocation
        blurScript.forEach(blurredAllocation);

        //Copy script result into bitmap
        blurredAllocation.copyTo(bitmap);

        //Destroy everything to free memory
        allocation.destroy();
        blurredAllocation.destroy();
        blurScript.destroy();
        t.destroy();
        rs.destroy();
        return bitmap;
    }

    public static Bitmap getBitmapFromResources(Context context, int resourceID){
        Bitmap b = BitmapFactory.decodeResource(context.getResources(),resourceID);
        return b;
    }


}
