package com.embrace.it.beskrivapp.components.tools;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.helpers.DrawingHelper;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.commons.observables.EventBus;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by waseem on 2/1/17.
 */

public class ToolMenu extends CardView implements ViewPager.OnPageChangeListener, View.OnClickListener {


    ViewPager toolPager;
    FrameLayout toolItemsBar;
    LinearLayout toolOptionsBar;
    LinearLayout dotsLayout;
    ToolData toolData;
    Constants.ToolType tooltype;

    public ToolMenu(Context context, ToolData data, Constants.ToolType tooltype) {
        super(context);
        this.tooltype = tooltype;
        this.toolData = data;
        initView(context);
    }

    public ToolMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ToolMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    int parentHeight, parentWidth;
    int childWidth, childHeight;
    int paddingHorizontal = 4, paddingVertical = 4;

    private void initView(Context context) {
        View view = inflate(context, R.layout.tool_menu, null);
        toolItemsBar = (FrameLayout) view.findViewById(R.id.tool_menu_items_bar);
        toolOptionsBar = (LinearLayout) view.findViewById(R.id.tool_menu_options_bar);
        toolPager = (ViewPager) view.findViewById(R.id.tool_menu_pager);
        dotsLayout = (LinearLayout) view.findViewById(R.id.tool_menu_dots);
        parentWidth = Utils.getScreenWidth();
        childWidth = parentWidth / 5;
        childHeight = 50;
        addOptionsBarItems(context);
        view.setBackgroundColor(Color.TRANSPARENT);
        if (tooltype != Constants.ToolType.Crop_Tool && tooltype != Constants.ToolType.Brightness_Tool) {
            refreshAdapter(context, 0);
        }
        addView(view);
    }


    void refreshAdapter(Context context, int mainIndex) {
        List<Integer> data = toolData.getEditTools().get(mainIndex).getToolItemIcons();
        PagerAdapter adapter = new ToolPagerAdapter(data, mainIndex);
        toolPager.setVisibility(View.VISIBLE);
        dotsLayout.setVisibility(View.VISIBLE);
        toolPager.setAdapter(adapter);
        toolPager.addOnPageChangeListener(this);
        toolPager.setBackgroundColor(Color.TRANSPARENT);
        addDots(context, adapter.getCount());
        EventBus.getToolMenuSubButtonObserver().notifyItemClicked(new ToolMenuSubButton(context, mainIndex, -1));
    }


    View slider = null;

    void handleBrightnessControl(int mainIndex) {

        if (slider != null)
            toolItemsBar.removeView(slider);
        if (mainIndex == 2) {
            Bundle b = new Bundle();
            b.putInt(Constants.Brightness_Main_Index, 2);
            b.putInt(Constants.Brightness_Progess, 0);
            EventBus.getBrightnessToolObserver().notifyItemClicked(b);
        } else {
//            slider = new SliderBar(getContext(), mainIndex, 50);
            slider = getSliderBarView(getContext(), mainIndex);
            toolItemsBar.addView(slider);
            slider.setVisibility(View.VISIBLE);
            toolPager.setVisibility(View.GONE);
            dotsLayout.setVisibility(View.GONE);

            String value = toolData.getToolItemIntruction(mainIndex);
            if(value != null && !value.equals("")){
                Bundle b = new Bundle();
                b.putString(Constants.ToolMenu_Action,Constants.ToolMenu_UpdateTitle);
                b.putInt(Constants.Brightness_Main_Index, mainIndex);
                b.putString("value",value);
                EventBus.getBrightnessToolObserver().notifyItemClicked(b);
            }

        }
    }

    void handleCropControl(int mainIndex) {

        if (mainIndex == 0) {
            Bundle b = new Bundle();
            b.putInt(Constants.Crop_Main_Index, 0);
            EventBus.getCropToolObserver().notifyItemClicked(b);
        } else {

            Bundle b = new Bundle();
            b.putInt(Constants.Crop_Main_Index, 1);
            EventBus.getCropToolObserver().notifyItemClicked(b);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Rect fram = new Rect(left, top, right, bottom);

        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            v.layout(left, top, right, bottom);
        }

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setDotAtPosition(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class ToolPagerAdapter extends PagerAdapter {

        List<Integer> dataList = new ArrayList<Integer>();
        private int mainIndex;

        @Override
        public int getCount() {

            int count = 0;
            count = dataList.size() / 5;
            if (((float) dataList.size() % 5.0) > 0)
                count = count + 1;
            return count;
        }

        public ToolPagerAdapter(List<Integer> data, int mainIndex) {
            this.mainIndex = mainIndex;
            dataList = data;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ToolMenuItemBarView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ToolMenuItemBarView v = (ToolMenuItemBarView) getViewForPagerAtPosition(container.getContext(), mainIndex, position, dataList);
            ((ViewPager) container).addView(v);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ToolMenuItemBarView) object);
        }
    }

    View getViewForPagerAtPosition(Context context, int mainIndex, int position, List datalist) {
        List subDataList;
        int from = position * 5;
        int to = (from + 5);
        if (to > (datalist.size()))
            to = datalist.size();
        subDataList = datalist.subList(from, to);
        final ToolMenuItemBarView toolbar = new ToolMenuItemBarView(context, subDataList, mainIndex, position);

        ViewTreeObserver viewTreeObserver = toolbar.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    toolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    toolbar.animateUpButtons();
                }
            });
        }

        return toolbar;
    }


    private ImageView[] dotes = null;

    void addDots(Context context, int size) {

        dotsLayout.removeAllViews();
        dotes = new ImageView[size];

        float width = getResources().getDimension(R.dimen.dp_4);
        width = Utils.dipToPixels(context, width);
        float height = getResources().getDimension(R.dimen.dp_4);
        height = Utils.dipToPixels(context, height);
        float margin = getResources().getDimension(R.dimen.dp_2);
        margin = Utils.dipToPixels(context, margin);
        for (int i = 0; i < size; i++) {
            ImageView iv = new ImageView(context);
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams((int) width, (int) height);
            parms.setMargins((int) margin, (int) margin, (int) margin, (int) margin);
            iv.setLayoutParams(parms);
            iv.setBackgroundResource(R.drawable.dot_default);
            dotsLayout.addView(iv);
            dotes[i] = iv;
        }
        setDotAtPosition(0);
    }

    void setDotAtPosition(int postition) {

        for (int i = 0; i < dotes.length; i++) {
            ImageView iv = dotes[i];
            iv.setBackgroundResource(R.drawable.dot_default);
        }
        ImageView iv = dotes[postition];
        iv.setBackgroundResource(R.drawable.dot_selected);
    }

    private static int BUTTON_OPTIONS = 123;
    List<ToolMenuButton> optionButtons;


    //Adding subMenuButtons
    void addOptionsBarItems(Context context) {
        optionButtons = new ArrayList<ToolMenuButton>();
        int padding = 10;
        int widthButton = Utils.getScreenWidth() / toolData.getEditTools().size() - padding * 2;
        int heightButton = Utils.convertDpToPx(60);
        for (int i = 0; i < toolData.getEditTools().size(); i++) {
            ToolItemData item = toolData.getEditTools().get(i);
            ToolMenuButton btn = new ToolMenuButton(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(widthButton, heightButton);
            params.setMargins(padding, padding, padding, padding);
            btn.setBackgroundColor(Color.parseColor("#00000000"));
            if (i == 0) {
                if (tooltype != Constants.ToolType.Crop_Tool && tooltype != Constants.ToolType.Brightness_Tool) {
                    btn.setBackgroundResource(R.drawable.thin_white_border_line);
                }
            }
            btn.setTextColor(Color.parseColor("#ffffff"));
            btn.setText(item.getTitle());
            btn.setLayoutParams(params);
            btn.setType(BUTTON_OPTIONS);
            btn.setButtonId(i);
//            btn.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            btn.setOnClickListener(this);
            optionButtons.add(btn);
            toolOptionsBar.addView(btn);
        }

    }

    @Override
    public void onClick(View v) {
        if (v instanceof ToolMenuButton) {
            ToolMenuButton b = (ToolMenuButton) v;
            int position = b.getButtonId();

            if (hideToolButtons(b)) {
                b.setBackgroundColor(Color.parseColor("#00000000"));
                Bundle bundle = new Bundle();
                bundle.putString("key","ToolMenu");
                  if (tooltype == Constants.ToolType.Brightness_Tool) {
                      String value = toolData.getActiveText();
                      if(value != null && !value.equals("")){
                          bundle.putString(Constants.ToolMenu_Action,Constants.ToolMenu_UpdateTitle);
                          bundle.putString("value",value);
                          EventBus.getBrightnessToolObserver().notifyItemClicked(bundle);
                      }
                  }else {
                      EventBus.getToolMenuSubButtonObserver().notifyItemClicked(bundle);
                  }
                return;
            }

            if (tooltype == Constants.ToolType.Crop_Tool) {
                handleCropControl(position);
            } else if (tooltype == Constants.ToolType.Brightness_Tool) {
                handleBrightnessControl(position);
            } else {

                refreshAdapter(v.getContext(), position);
            }
            for (ToolMenuButton bt : optionButtons)
                bt.setBackgroundColor(Color.parseColor("#00000000"));
            b.setBackgroundResource(R.drawable.thick_white_border_line);
        }
    }


    View getSliderBarView(Context context, final int mainIndex) {
        final Bundle b = new Bundle();
        View v = LayoutInflater.from(context).inflate(R.layout.sliderbar, null, true);
        SeekBar seekbar = (SeekBar) v.findViewById(R.id.slider_seek_bar);
        if (mainIndex == 0)
            seekbar.setProgress(DrawingHelper.brightnessBgImage);
        else
            seekbar.setProgress(DrawingHelper.contrastBgImage);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                b.putInt(Constants.Brightness_Main_Index, mainIndex);
                b.putInt(Constants.Brightness_Progess, progress);
                EventBus.getBrightnessToolObserver().notifyItemClicked(b);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        int h = toolItemsBar.getHeight();
        int marginTop = 30;
        int marginBottom = 30;
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, h - marginTop - marginBottom);
        params.setMargins(0, marginTop, 0, marginBottom);
//        v.setLayoutParams(params);
        v.setY(marginTop);

        Drawable drawable = getResources().getDrawable(R.drawable.slidercirkel, context.getTheme());
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5),
                (int) (drawable.getIntrinsicHeight() * 0.5));
        float scaleHeight = (float) (h - marginTop - marginBottom) / (float) drawable.getIntrinsicHeight();
        ScaleDrawable sd = new ScaleDrawable(drawable, Gravity.CENTER, scaleHeight, scaleHeight);
        Drawable d = sd.getDrawable();
//      seekbar.setThumb(d);
        return v;
    }

    public void animateUpSubViews() {

    }

    boolean hideToolButtons(ToolMenuButton b) {

        int position = b.getButtonId();
        if ((b.getBackground().getConstantState() == b.getContext().getResources().getDrawable(R.drawable.thick_white_border_line, b.getContext().getTheme()).getConstantState())
                || (b.getBackground().getConstantState() == b.getContext().getResources().getDrawable(R.drawable.thin_white_border_line, b.getContext().getTheme()).getConstantState())) {

            if (tooltype == Constants.ToolType.Crop_Tool) {
                return false;
            } else if (tooltype == Constants.ToolType.Brightness_Tool) {
                if (position == 2)
                    return false;
                if (slider != null)
                    slider.setVisibility(View.INVISIBLE);
            } else {
                toolPager.setVisibility(View.INVISIBLE);
                dotsLayout.setVisibility(View.INVISIBLE);
            }
            return true;
        }
        return false;
    }

}
