package com.embrace.it.beskrivapp.database;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by waseem on 2/9/17.
 */

public class DBController {

    private static DBController mController;
    private static Realm realm;

    public static DBController getInstance() {
        if (mController == null)
            mController = new DBController();
        return mController;
    }

    public DBController() {
        realm = Realm.getDefaultInstance();
    }

    public Realm getRealm() {
//        return realm;
        return Realm.getDefaultInstance();
    }

    public void closeRealm() {
        if (realm != null)
            realm.close();
    }

    public static void saveAlbumInfo(AlbumInfo info) {
        Realm realm = DBController.getInstance().getRealm();
        realm.beginTransaction();
        realm.copyToRealm(info);
        realm.commitTransaction();
    }

    public static boolean isProjectExisting(String albumTitle) {
        List result = DBController.getInstance().getRealm().where(Project.class).equalTo("projectTitle", albumTitle).findAll();
        if (result != null && result.size() > 0)
            return true;
        else
            return false;
    }

    public static void saveProject(final Project project) {

        Realm realm = DBController.getInstance().getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                try {
//                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(project);
//                    realm.commitTransaction();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }


    public static Project getProject(String projectId) {
        try {
            return DBController.getInstance().getRealm().where(Project.class).equalTo("projectId", projectId).findFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static SubProject getSubProject(String subProjectId) {
        try {
            return DBController.getInstance().getRealm().where(SubProject.class).equalTo("subProjectId", subProjectId).findFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean deleteSubProject(String projectID) {
        try {
            Realm realm = DBController.getInstance().getRealm();
            SubProject p = getSubProject(projectID);
            realm.beginTransaction();
            p.deleteFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean deleteProject(String projectID) {
        try {
            Realm realm = DBController.getInstance().getRealm();
            Project p = getProject(projectID);
            realm.beginTransaction();
            p.deleteFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static List<Project> getProjects() {
        try {
            return DBController.getInstance().getRealm().where(Project.class).findAll().sort("projectId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNextIdForProject() {
        List result = DBController.getInstance().getRealm().where(Project.class).findAllSorted("projectId");
        Integer newId = 1;
        if (result != null && result.size() > 0) {
            Project p = (Project) result.get(result.size() - 1);
            Integer lastId = Integer.parseInt(p.getProjectId());
            newId = lastId + 1;
        }
        return newId.toString();
    }


    public static void updateProject(final Project project, final Project newProject) {
        Realm realm = DBController.getInstance().getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                AlbumInfo newAlbum = newProject.getAlbum();
                AlbumInfo album = project.getAlbum();
                try {
//            realm.beginTransaction();
                    project.setProjectTitle(newAlbum.getAlbumTitle());
                    album.setAlbumTitle(newAlbum.getAlbumTitle());
                    album.setWho(newAlbum.getWho());
                    album.setWhere(newAlbum.getWhere());
                    album.setGps(newAlbum.getGps());
                    album.setTime(newAlbum.getTime());
                    album.setDate(newAlbum.getDate());
                    album.setOther(newAlbum.getOther());
//            realm.commitTransaction();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    static public SubProject addSubProject(Project project, ProjectItem projectItem, String subProejctId, String imagepath, String filename) {
        if (project == null || projectItem == null)
            return null;

        Realm realm = DBController.getInstance().getRealm();
        try {
            realm.beginTransaction();
            SubProject sub = new SubProject();
            ProjectMainItem item = new ProjectMainItem();
            item.setItem(projectItem);
            item.setImagePath(imagepath);
            sub.setSubProjectId(subProejctId);
            sub.setMainItem(item);
            sub.setProjectTitle(filename);
            project.getSubProjects().add(sub);
            realm.commitTransaction();
            return sub;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void updateThumbnailImage(final String subProjectId, final String path) {
        Realm realm = DBController.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                try {
                    SubProject sub = getSubProject(subProjectId);
//                    realm.beginTransaction();
                    sub.setThumbnailPath(path);
//                    realm.commitTransaction();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void updateSubprojectTitle(SubProject sub, String title) {
        Realm realm = DBController.getInstance().getRealm();
        try {
            realm.beginTransaction();
            sub.setProjectTitle(title);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int getSubProjectCount(String projectID) {
        Project p = getProject(projectID);
        int count = 0;
        if (p != null && p.getSubProjects() != null) {
            count = p.getSubProjects().size();
        }
        return count;
    }

    public static void updateProjectItem1(ProjectMainItem projectmainItem, ProjectItem itemIn) {
        if (itemIn == null)
            return;
        Realm realm = DBController.getInstance().getRealm();
        try {
            realm.beginTransaction();
            ProjectItem oldItem = projectmainItem.getItem();
            oldItem.setDrawingPoints(itemIn.getDrawingPoints());
            oldItem.setItemType(itemIn.getItemType());
            oldItem.setCharacteristics(itemIn.getCharacteristics());
            oldItem.setFrame(itemIn.getFrame());

            RealmList<RealmFloat> list = itemIn.getMatrixValues();
            if (!list.isManaged()) { // if the 'list' is managed, all items in it is also managed
                RealmList<RealmFloat> managedImageList = new RealmList<RealmFloat>();
                for (RealmFloat item : list) {
                    if (item.isManaged()) {
                        managedImageList.add(item);
                    } else {
                        managedImageList.add(realm.copyToRealm(item));
                    }
                }
                list = managedImageList;
            }
            oldItem.setMatrixValues(list);
            realm.commitTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateProjectItem(final ProjectMainItem projectmainItem, final ProjectItem itemIn) {
        if (itemIn == null || projectmainItem == null)
            return;
        Realm realm = DBController.getInstance().getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {

                    ProjectMainItem projectmainItemManaged;
                    ProjectItem itemInMnanaged;
//                    realm.beginTransaction();
                    if (!projectmainItem.isManaged())
                        projectmainItemManaged = realm.copyToRealm(projectmainItem);
                    else
                        projectmainItemManaged = projectmainItem;
                        itemInMnanaged = realm.copyToRealm(itemIn);
                    if (projectmainItemManaged.getItem() != null)
                        projectmainItemManaged.getItem().deleteFromRealm();
                    projectmainItemManaged.setItem(itemInMnanaged);
//                    realm.commitTransaction();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static void updateProjectItem(final SubProject subProject, final RealmList<ProjectItem> itemIn) {
        if (subProject == null || itemIn == null)
            return;
        Realm realm = DBController.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                try {
//            realm.beginTransaction();

                    RealmList<ProjectItem> managedImageList = new RealmList<>();
                    if (!itemIn.isManaged()) { // if the 'list' is managed, all items in it is also managed
                        for (ProjectItem item : itemIn) {
                            if (item.isManaged()) {
                                managedImageList.add(item);
                            } else {
                                managedImageList.add(realm.copyToRealm(item));
                            }
                        }
                    } else {
                        managedImageList = itemIn;
                    }
                    if (subProject.getProjectItems() != null && subProject.getProjectItems().size() > 0)
                        subProject.getProjectItems().deleteAllFromRealm();
                    subProject.setProjectItems(managedImageList);
//            realm.commitTransaction();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public static void updateSubProject(final String subProjectID, final RealmList<ProjectItem> itemIn) {
        final SubProject subProject = getSubProject(subProjectID);
        if (subProject == null || itemIn == null)
            return;
        Realm realm = DBController.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                try {
//            realm.beginTransaction();

                    RealmList<ProjectItem> managedImageList = new RealmList<>();
                    if (!itemIn.isManaged()) { // if the 'list' is managed, all items in it is also managed
                        for (ProjectItem item : itemIn) {
                            if (item.isManaged()) {
                                managedImageList.add(item);
                            } else {
                                managedImageList.add(realm.copyToRealm(item));
                            }
                        }
                    } else {
                        managedImageList = itemIn;
                    }
                    if (subProject.getProjectItems() != null && subProject.getProjectItems().size() > 0)
                        subProject.getProjectItems().deleteAllFromRealm();
                    subProject.setProjectItems(managedImageList);
//            realm.commitTransaction();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }


    public static void updateSubProject(final SubProject subProject, final String time, final String date, final String gps) {
        if (subProject == null || ( date == null && time == null && gps == null))
            return;
        Realm realm = DBController.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                try {
                    if (time != null) {
                        subProject.setTime(new RealmString(time));
                    }
                    if(date != null){
                        subProject.setDate(new RealmString(date));
                    }
                    if(gps != null) {
                        subProject.setGpsCoordinate(new RealmString(gps));
                    }
                    realm.copyToRealmOrUpdate(subProject);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }


}
