package com.embrace.it.beskrivapp.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;

/**
 * Created by waseem on 1/31/17.
 */

public class StartFragment extends Fragment implements SimpleTouchListener.SimpleTouchListenerCallback {

    SimpleTouchListener touchListener;
    ImageButton btnInfo;
    ImageButton btnPlus;
    ImageView ivDownArrow;
    SplashFragmentCallback delegate;
    public void setDelegate(SplashFragmentCallback delegate) {
        this.delegate = delegate;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_start,container,false);
        btnInfo = (ImageButton)view.findViewById(R.id.btn_info_splash);
        btnPlus = (ImageButton)view.findViewById(R.id.btn_plus_splash);
        ivDownArrow = (ImageView) view.findViewById(R.id.iv_splash_downarrow);
        try{
        handler.removeCallbacks(animatesDownArrow);}catch (Exception e){}
        handler.postDelayed(animatesDownArrow,6000);
        touchListener = new SimpleTouchListener(container.getContext(), this);
        touchListener.setSwipeSensitivity(19);
        view.setOnTouchListener(touchListener);
        btnInfo.setOnTouchListener(touchListener);
        btnPlus.setOnTouchListener(touchListener);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    final Handler handler = new Handler();
    Runnable animatesDownArrow = new Runnable() {
        public void run() {
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            animateArrow();
                        } catch (Exception e) {

                        }
                    }
                });
            } catch (Exception e) {
            }
        }
    };


    void animateArrow(){
        handler.postDelayed(animatesDownArrow,6000);
        Animation anim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.down_arrow_anim);
        ivDownArrow.clearAnimation();
        ivDownArrow.setAnimation(anim);
        anim.start();
    }


    public interface SplashFragmentCallback {
        public void showInfoScreen();
        public void createNewProject();
        public void showProjects();
    }



    //-------SimpleTouchListener Callbacks----------//
    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {
        if(v.equals(btnInfo) || v.equals(btnPlus)){

        } else if(Math.abs(yTotal) > Math.abs(xTotal)) {
            if(yTotal > 20) {
//                delegate.showProjects();
            }
        }
    }

    @Override
    public void click(View v,float x,float y) {
        if(v.equals(btnInfo)){
            if(delegate != null){
                delegate.showInfoScreen();
            }
        }else if (v.equals(btnPlus)){
            if(delegate != null){
                delegate.createNewProject();
            }
        }
    }

    @Override
    public void swipe(View v,int direction){

        if(v.equals(btnInfo) || v.equals(btnPlus)){

        }else if(direction == SimpleTouchListener.SWIPE_DOWN){
                    delegate.showProjects();

        }
    }
}
