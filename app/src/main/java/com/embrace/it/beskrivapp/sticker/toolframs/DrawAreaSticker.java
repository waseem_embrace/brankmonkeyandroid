package com.embrace.it.beskrivapp.sticker.toolframs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.DrawableSticker;
import com.embrace.it.beskrivapp.sticker.StickerUtils;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

import java.util.List;

/**
 * @author wupanjie
 */
public class DrawAreaSticker extends DrawableSticker {

    float circleRadius = 30;
    private int lineSizeIndex;
    private int fillTypeIndex;
    private int colorIndex;
    private List<PointF> drawingPoints;
    Paint linePaint;
    Paint linePaintFill;

    public DrawAreaSticker(Drawable drawable) {
        super(drawable);
        colorIndex = 0;
        lineSizeIndex = 1;
        fillTypeIndex = 1;
        setUserIntractions(true,true,false);
    }

    public DrawAreaSticker(List<PointF> drawingPoints, ToolData data) {
        super();
        colorIndex = 0;
        lineSizeIndex = 1;
        fillTypeIndex = 1;
        this.drawingPoints = drawingPoints;
        setUserIntractions(true,true,false);
        if (data != null) {
            setData(data);
            init();
        }
    }

    @Override
    public void itemUpdated(int mainIndex, int childIndex) {
        if( mainIndex < 0 && childIndex < 0) {
            mainIndex = 2;
            childIndex = 0;
        }
        ToolData data = getData();
        if (data != null) {
            Drawable drawable;
            switch (mainIndex) {
                  case 0:

                    this.lineSizeIndex = childIndex;
                     drawable = getNewDrawable();
//                    drawable.setTint(getMyColor());
                    setDrawable(drawable);
                    break;
                case 1:
                    this.fillTypeIndex = childIndex;
                    drawable = getNewDrawable();
//                    drawable.setTint(getMyColor());
                    setDrawable(drawable);
                    break;

                case 2:
                    this.colorIndex = childIndex;
                    drawable = getNewDrawable();
//                    drawable.setTint(getMyColor());
                    setDrawable(drawable);
                    break;

            }
        }
    }




    //Used only the first time the sticker is being created when its drawn by user.
    private RectF firstTimeCalculatedFram;

    @Override
    public RectF getInitialFrame() {
        return firstTimeCalculatedFram;
    }

    private Drawable getNewDrawable() {
        RectF rect = StickerUtils.calculateBounds(drawingPoints, circleRadius); //rect = left,top,width,height
        RectF rectWithMargins = new RectF(rect.left -  marginImage ,(rect.top -  marginImage),rect.left + rect.right + marginImage , (int) (rect.top + rect.bottom + marginImage));
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap((int) (rect.left + rect.right + marginImage ), (int) (rect.top + rect.bottom + marginImage ), conf); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bmp);
        initPaint();
        drawCanvas(canvas, this.drawingPoints);
        Bitmap resizedBitmap = getCroppedBitmap(bmp, rectWithMargins);
        Drawable drawable = new BitmapDrawable(MainApplication.getContext().getResources(), resizedBitmap);
        firstTimeCalculatedFram = rectWithMargins;
        return drawable;
    }

    private int getMyColor() {
        ToolItemData icons = data.getEditTools().get(2);
        int color = ContextCompat.getColor(MainApplication.getContext(), icons.getToolItemData().get(colorIndex));
        color = (color & 0x00FFFFFF) | 0x99000000;
        return color;
    }

    private int getLineSize() {
        ToolItemData icons = data.getEditTools().get(0);
        return icons.getToolItemData().get(lineSizeIndex);
    }

    private int getFillType() {
        ToolItemData icons = data.getEditTools().get(1);
        return icons.getToolItemData().get(fillTypeIndex);
    }

    private void initPaint() {

        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(getLineSize());
        linePaint.setColor(getMyColor());

        linePaintFill = new Paint();
        linePaintFill.setAntiAlias(true);
        linePaintFill.setStyle(Paint.Style.FILL);
        linePaintFill.setStrokeWidth(getLineSize());
        linePaintFill.setAlpha(200);
        if(this.fillTypeIndex == 0){
            linePaintFill.setColor(Color.TRANSPARENT);
        }else if(this.fillTypeIndex == 1){
            linePaintFill.setColor(getMyColor());
        }else {
            Bitmap patternBMP = BitmapFactory.decodeResource(MainApplication.getContext().getResources(), getFillType());
            BitmapShader patternBMPshader = new BitmapShader(patternBMP,
                    Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            linePaintFill.setShader(patternBMPshader);
        }
    }


    void drawCanvas(Canvas canvas, List<PointF> drawingPoints) {

        PointF prevPoint = null;
        Path path = new Path();
        for (int i = 0; i < drawingPoints.size(); i++) {
            PointF p = drawingPoints.get(i);
            if (prevPoint != null) {
//                canvas.drawLine(prevPoint.x, prevPoint.y, p.x, p.y, linePaint);
            }
            prevPoint = p;
            if (i == 0) {
                path.moveTo(p.x, p.y);
            } else {
                path.lineTo(p.x, p.y);
            }
        }
        path.close();
        canvas.drawPath(path, linePaintFill);
        canvas.drawPath(path, linePaint);
    }

    float marginImage = 10;

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }

    public Bitmap getResizedImage(Bitmap bitmap, RectF rect) {
        float left = (rect.left - marginImage) < 0 ? 0 : rect.left - marginImage;
        float top = (rect.top - marginImage) < 0 ? 0 : rect.top - marginImage;
        float right = (rect.right + marginImage) > bitmap.getWidth() ? bitmap.getWidth() : rect.right + marginImage;
        float bottom = (rect.bottom + marginImage) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom + marginImage;

        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) right, (int) bottom);
        return resizedbitmap;
    }

    private Bitmap getCroppedBitmap(Bitmap bitmap, RectF rect) {
        float left = (rect.left) < 0 ? 0 : rect.left ;
        float top = (rect.top ) < 0 ? 0 : rect.top;
        float right = (rect.right ) > bitmap.getWidth() ? bitmap.getWidth() : rect.right;
        float bottom = (rect.bottom ) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom;

        //if rect is updated then, update the object
        rect.left = left;rect.right = right;rect.top = top; rect.bottom = bottom;
        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) (right - left), (int) (bottom - top));
        return resizedbitmap;
    }

    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);
        projectItem.setItemType(toolType.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(this.colorIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.fillTypeIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.lineSizeIndex));

        if(getFrame() != null) {
            projectItem.setFrame(getFrame());
        }

        projectItem.setDrawingPoints(drawingPoints);
        return projectItem;
    }

    @Override
    public void setProjectItem(ProjectItem p) {

        toolType = Utils.getToolType(p.getItemType());

        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        this.colorIndex = (int)characterstics[0];
        this.fillTypeIndex = (int)characterstics[1];
        this.lineSizeIndex = (int)characterstics[2];

        if(p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
            setFrame(fram);
        }
        drawingPoints = RealmHelper.getListPointF(p.getDrawingPoints());

        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);
        itemUpdated(1,this.fillTypeIndex);

    }
 }
