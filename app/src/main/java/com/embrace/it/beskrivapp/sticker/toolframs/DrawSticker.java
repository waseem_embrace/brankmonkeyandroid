package com.embrace.it.beskrivapp.sticker.toolframs;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.DrawableSticker;
import com.embrace.it.beskrivapp.sticker.StickerUtils;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

import java.util.List;

/**
 * @author wupanjie
 */
public class DrawSticker extends DrawableSticker {

    //    private int imageIndex;
    private int colorIndex;
    private int lineSizeIndex;
    private List<PointF> drawingPoints;

    public DrawSticker(Drawable drawable) {
        super(drawable);
        colorIndex = 0;
        lineSizeIndex = 0;
        setUserIntractions(true,true,false);
    }

    public DrawSticker(List<PointF> drawingPoints, ToolData data) {
        super();
        colorIndex = 5;
        lineSizeIndex = 0;
        this.drawingPoints = drawingPoints;
        setUserIntractions(true,true,false);
        if (data != null) {
            setData(data);
            init();
        }
    }

    @Override
    public void itemUpdated(int mainIndex, int childIndex) {
        if( mainIndex < 0 && childIndex < 0) {
            mainIndex = 0;
            childIndex = 0;
        }
        ToolData data = getData();
        if (data != null) {
            switch (mainIndex) {
                case 0:
                    this.colorIndex = childIndex;
                    Drawable drawable = getDrawable();
                    if (drawable == null)
                        drawable = getNewDrawable();
                    drawable.setTint(getMyColor());
                    setDrawable(drawable);
                    break;
                case 1:

                    this.lineSizeIndex = childIndex;
                    Drawable drawable1 = getNewDrawable();
                    drawable1.setTint(getMyColor());
                    setDrawable(drawable1);
                    break;

            }
        }
    }

    float circleRadius = 30;


    //Used only the first time the sticker is being created when its drawn by user.
    private RectF firstTimeCalculatedFram;

    @Override
    public RectF getInitialFrame() {
        return firstTimeCalculatedFram;
    }

    private Drawable getNewDrawable() {
        RectF rect = StickerUtils.calculateBounds(drawingPoints, circleRadius); //rect = left,top,width,height
        RectF rectWithMargins = new RectF(rect.left -  marginImage ,(rect.top -  marginImage),rect.left + rect.right + marginImage , (int) (rect.top + rect.bottom + marginImage));
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap((int) (rect.left + rect.right + marginImage ), (int) (rect.top + rect.bottom + marginImage ), conf); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bmp);
        initPaint();
        drawCanvas(canvas, this.drawingPoints);
        Bitmap resizedBitmap = getCroppedBitmap(bmp, rectWithMargins);
        Drawable drawable = new BitmapDrawable(MainApplication.getContext().getResources(), resizedBitmap);
        firstTimeCalculatedFram = rectWithMargins;
        return drawable;
    }



    private int getMyColor() {
        ToolItemData icons = data.getEditTools().get(0);
        return ContextCompat.getColor(MainApplication.getContext(), icons.getToolItemData().get(colorIndex));
    }

    private int getLineSize() {
        ToolItemData icons = data.getEditTools().get(1);
        return icons.getToolItemData().get(lineSizeIndex);
    }

    private void initPaint() {

        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setColor(getMyColor());
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(getLineSize());
    }

    Paint linePaint;

    void drawCanvas(Canvas canvas, List<PointF> drawingPoints) {

        PointF prevPoint = null;
        for (int i = 0; i < drawingPoints.size(); i++) {
            PointF p = drawingPoints.get(i);
//            canvas.drawCircle(p.x, p.y, circleRadius, circlePaint);
            if (prevPoint != null) {
                canvas.drawLine(prevPoint.x, prevPoint.y, p.x, p.y, linePaint);
            }
            prevPoint = p;
        }


    }

    float marginImage = 10;

    public Bitmap getResizedImage(Bitmap bitmap, RectF rect) {
        float left = (rect.left - marginImage) < 0 ? 0 : rect.left - marginImage;
        float top = (rect.top - marginImage) < 0 ? 0 : rect.top - marginImage;
        float right = (rect.right + marginImage) > bitmap.getWidth() ? bitmap.getWidth() : rect.right + marginImage;
        float bottom = (rect.bottom + marginImage) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom + marginImage;

        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) right, (int) bottom);
        return resizedbitmap;
    }

    private Bitmap getCroppedBitmap(Bitmap bitmap, RectF rect) {
        float left = (rect.left) < 0 ? 0 : rect.left ;
        float top = (rect.top ) < 0 ? 0 : rect.top;
        float right = (rect.right ) > bitmap.getWidth() ? bitmap.getWidth() : rect.right;
        float bottom = (rect.bottom ) > bitmap.getHeight() ? bitmap.getHeight() : rect.bottom;

        //if rect is updated then, update the object
        rect.left = left;rect.right = right;rect.top = top; rect.bottom = bottom;
        Bitmap resizedbitmap = Bitmap.createBitmap(bitmap, (int) left, (int) top, (int) (right - left), (int) (bottom - top));
        return resizedbitmap;
    }

    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);
        projectItem.setItemType(toolType.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(this.colorIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.lineSizeIndex));

        if(getFrame() != null) {
            projectItem.setFrame(getFrame());
        }

        projectItem.setDrawingPoints(drawingPoints);
        return projectItem;
    }

    @Override
    public void setProjectItem(ProjectItem p) {

        toolType = Utils.getToolType(p.getItemType());

        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        this.colorIndex = (int)characterstics[0];
        this.lineSizeIndex = (int)characterstics[1];

        if(p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
            setFrame(fram);
        }
        drawingPoints = RealmHelper.getListPointF(p.getDrawingPoints());

        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);
        itemUpdated(1,this.lineSizeIndex);

    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }
 }
