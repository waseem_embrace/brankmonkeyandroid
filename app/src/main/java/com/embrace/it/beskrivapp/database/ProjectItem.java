package com.embrace.it.beskrivapp.database;

import android.graphics.PointF;
import android.graphics.RectF;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by waseem on 2/9/17.
 */

public class ProjectItem extends RealmObject {


    /*
        value of ToolType
     */
    private int itemType;

    /*
        stores the matrix for sticker
   */
    private RealmList<RealmFloat> matrixValues;

    /*

     /*
        stores the matrix for text in EdittextSticker
   */
    private RealmList<RealmFloat> matrixValues2;

    /*
        stores the fram of sticker in parentView
     */
    private RealmList<RealmFloat> frame;

    /*
        used for sticker characteristics
        index = mainIndex,  value = subIndex
     */
    private RealmList<RealmFloat> characteristics;

    /*
        Used for drawing tools.
    */
    private RealmList<RealmPointF> drawingPoints;

    /*
           Used for areaText tools.
       */
    private RealmString textValue;


    public ProjectItem() {
        this.matrixValues = new RealmList<RealmFloat>();
        this.matrixValues2 = new RealmList<RealmFloat>();
        this.characteristics = new RealmList<RealmFloat>();
        this.drawingPoints = new RealmList<RealmPointF>();
        this.frame = new RealmList<RealmFloat>();
        this.textValue = new RealmString(" ");
    }

    public RealmList<RealmFloat> getMatrixValues2() {
        return matrixValues2;
    }

    public void setMatrixValues2(RealmList<RealmFloat> matrixValues2) {
        this.matrixValues2 = matrixValues2;
    }

    public void setMatrixValues2(float[] matrixValues2) {
        this.matrixValues2.clear();
        for(float f : matrixValues2)
            this.matrixValues2.add(new RealmFloat(f));
    }


    public RealmList<RealmFloat> getMatrixValues() {
        return matrixValues;
    }

    public void setMatrixValues(RealmList<RealmFloat> matrixValues) {
        this.matrixValues = matrixValues;
    }

    public void setMatrixValues(float[] matrixValues) {
        this.matrixValues.clear();
        for(float f : matrixValues)
            this.matrixValues.add(new RealmFloat(f));
    }

    public RealmList<RealmFloat> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(RealmList<RealmFloat> characteristics) {
        this.characteristics = characteristics;
    }


    public RealmList<RealmPointF> getDrawingPoints() {
        return drawingPoints;
    }

    public void setDrawingPoints(RealmList<RealmPointF> drwingPoints) {
        this.drawingPoints = drwingPoints;
    }

    public void setDrawingPoints(List<PointF> drwingPoints) {
        this.drawingPoints.clear();
        for(PointF f : drwingPoints)
            this.drawingPoints.add(new RealmPointF(f));
    }

    public RealmList<RealmFloat> getFrame() {
        return frame;
    }

    public void setFrame(RealmList<RealmFloat> frame) {
        this.frame = frame;
    }

    public void setFrame(RectF frame) {
        this.frame.clear();
        this.frame.add(new RealmFloat(frame.left));
        this.frame.add(new RealmFloat(frame.top));
        this.frame.add(new RealmFloat(frame.right));
        this.frame.add(new RealmFloat(frame.bottom));

    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public RealmString getTextValue() {
        return textValue;
    }

    public void setTextValue(RealmString textValue) {
        this.textValue = textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = new RealmString(textValue);
    }

}
