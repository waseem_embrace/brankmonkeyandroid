package com.embrace.it.beskrivapp.adapters;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.activity.StartActivity;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.embrace.it.beskrivapp.commons.ResizeAnimation;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.AlbumInfo;
import com.embrace.it.beskrivapp.database.Project;
import com.embrace.it.beskrivapp.database.SubProject;

/**
 * Created by waseem on 2/1/17.
 */


public class DetailCellViewHolder extends RecyclerView.ViewHolder implements SimpleTouchListener.SimpleTouchListenerCallback {
    ImageButton btnDelete;
    ImageButton btnSettings;
    TextView tvTitle;
    TextView tvDetails;
    View foregroundView;
    View backgroundView;
    Context context;
    ImageView ivArrow;
    ImageView ivDrawing;
    SimpleTouchListener touchListener;
    View contentView;
    View mainView;
    Project project;
    DetailViewholderCallback delegate;

    public interface DetailViewholderCallback {
        void cellIsOpened(DetailCellViewHolder viewholder, int position);

        void startDrawing(DetailCellViewHolder viewholder, int position, Project project);

        void deleteProject(DetailCellViewHolder viewholder, int position);

        void closeCell(int position);
    }

    public void setDelegate(DetailViewholderCallback d) {
        this.delegate = d;
    }

    public DetailCellViewHolder(View itemView) {
        super(itemView);
        btnDelete = (ImageButton) itemView.findViewById(R.id.btn_cell_delete);
        btnSettings = (ImageButton) itemView.findViewById(R.id.btn_cell_settings);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_cell_title);
        ivArrow = (ImageView) itemView.findViewById(R.id.iv_cell_arrow);
        tvDetails = (TextView) itemView.findViewById(R.id.tv_cell_details);
        foregroundView = itemView.findViewById(R.id.cell_foreground_layout);
        backgroundView = itemView.findViewById(R.id.bottom_wrapper);
        ivDrawing = (ImageView) itemView.findViewById(R.id.tv_cell_detail_imageview);
        contentView = itemView.findViewById(R.id.ll_cell_content);
        backgroundView.setVisibility(View.INVISIBLE);
        bgDrawable = null;
        mainView = itemView;


    }

    public void setValues(Context context, Project p) {
        this.context = context;
        this.project = p;
        touchListener = new SimpleTouchListener(context, this);
        foregroundView.setOnTouchListener(touchListener);
        btnDelete.setOnTouchListener(touchListener);
        btnSettings.setOnTouchListener(touchListener);
        ivDrawing.setEnabled(true);
        ivDrawing.setOnTouchListener(touchListener);
        Typeface fontBold = Utils.getFontTypeface(context, Constants.FONT_FrutigerLTStd_Black);
        Typeface fontLight = Utils.getFontTypeface(context, Constants.FONT_FrutigerLTStd_Light);
        tvTitle.setTypeface(fontBold);
        tvDetails.setTypeface(fontLight);
        bgDrawable = null;
        setBgImage();
        AlbumInfo album = p.getAlbum();
        String title = project.getProjectTitle().toUpperCase();
        int count = 0;
        if (project.getSubProjects() != null)
           count =  project.getSubProjects().size();
//        count = DBController.getNumberOfImagesInProject(project.getProjectId());
        if (count < 2)
            title += " - " + count + " " + context.getResources().getString(R.string.picture_one);
        else
            title += " - " + count + " " + context.getResources().getString(R.string.picture_multiple);
        tvTitle.setText(title);
        tvDetails.setText(album.getOther());

//        setSize();


        if(p.isOpened == null || p.isOpened == false)
            delegate.closeCell(getLayoutPosition());
        closeWithOutAnimation();
//        else
//            openWithAnimation();
//        bgDrawable = null;
    }

    void animateToSwipe() {
        backgroundView.setVisibility(View.VISIBLE);
        ObjectAnimator animXForeground = ObjectAnimator.ofFloat(foregroundView, "x", 0, -backgroundView.getWidth());
        ObjectAnimator animXBackground = ObjectAnimator.ofFloat(backgroundView, "x", foregroundView.getWidth(), foregroundView.getWidth() - backgroundView.getWidth());
        ObjectAnimator animFade = ObjectAnimator.ofFloat(ivArrow, "alpha", 1, 0);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animXForeground, animXBackground, animFade);
        set.setDuration(500).start();
    }

    void animateToUnSwipe() {
        ObjectAnimator animXForeground = ObjectAnimator.ofFloat(foregroundView, "x", foregroundView.getX(), 0);
        ObjectAnimator animXBackground = ObjectAnimator.ofFloat(backgroundView, "x", backgroundView.getX(), backgroundView.getX() + backgroundView.getWidth());
        ObjectAnimator animFade = ObjectAnimator.ofFloat(ivArrow, "alpha", 0, 1);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animXForeground, animXBackground, animFade);
        set.setDuration(200).start();
    }


    //-------SimpleTouchListener Callbacks----------//

    private Boolean StateOpened = false;

    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {
        if (v.equals(foregroundView)) {
            MyLog.e("", "xTotal=" + xTotal);
            if (xTotal < -20) {
                if (!StateOpened) {
                    animateToSwipe();
                    StateOpened = true;
                }
            } else if (xTotal > 20) {
                if (StateOpened) {
                    animateToUnSwipe();
                    StateOpened = false;
                }
            }
        }
    }

    @Override
    public void click(View v,float x,float y) {
        int pos = getLayoutPosition();

        if (v.equals(btnDelete)) {
            if (delegate != null) delegate.deleteProject(this, pos);
        } else if (v.equals(btnSettings)) {
            try {

                ((StartActivity) context).EditProject(project.getProjectId());
            } catch (Exception e) {
                ((MainActivity) context).EditProject(project.getProjectId());
            }

        } else if (v.equals(backgroundView)) {

        } else if (v.equals(ivDrawing)) {
            if (delegate != null) delegate.startDrawing(this, pos, project);
        } else {
            if (this.project.isOpened == null || !this.project.isOpened) {
                setSize();
                if (delegate != null) delegate.cellIsOpened(this, pos);
            } else {
                if (delegate != null) delegate.startDrawing(this, pos, project);
            }
        }
    }

    @Override
    public void swipe(View v, int direction) {

    }


    //---------Expand and collapse animations----------------//
    int cellOpenAnimationDuration = 300;

    void setSize() {
        if (project.isOpened != null && project.isOpened) {
            this.closeWithAnimation();
        } else {
            this.openWithAnimation();
        }
    }

    public void closeWithAnimation() {
        if (project.isOpened == null || !project.isOpened) {
            return;
        }
        View v = contentView;
        float closedHeight = context.getApplicationContext().getResources().getDimension(R.dimen.cell_height);
        v.clearAnimation();
        foregroundView.clearAnimation();
        ResizeAnimation resizeAnimation = new ResizeAnimation(v, v.getHeight(), -(v.getHeight() - (int) closedHeight));
        resizeAnimation.setDuration(cellOpenAnimationDuration);
        v.startAnimation(resizeAnimation);
        ObjectAnimator.ofFloat(v, "alpha", 1, 0).setDuration(cellOpenAnimationDuration).start();
        project.isOpened = false;

    }
    public void closeWithOutAnimation() {
        if (project.isOpened == null || !project.isOpened) {
//            return;
        }
        View v = contentView;
        float closedHeight = context.getApplicationContext().getResources().getDimension(R.dimen.cell_height);
        v.clearAnimation();
        foregroundView.clearAnimation();
        ResizeAnimation resizeAnimation = new ResizeAnimation(v, v.getHeight(), -(v.getHeight() - (int) closedHeight));
        resizeAnimation.setDuration(1);
        v.startAnimation(resizeAnimation);
        ObjectAnimator.ofFloat(v, "alpha", 1, 0).setDuration(1).start();
        project.isOpened = false;
    }

    private Drawable bgDrawable = null;

    void setBgImage() {
        if (project != null) {

            if (bgDrawable != null) {
                ivDrawing.setImageDrawable(bgDrawable);
            } else {

                if (project.getSubProjects().size() > 0) {
                    SubProject sub = project.getSubProjects().first();
                    if (sub.getThumbnailPath() != null && !sub.getThumbnailPath().equals("")) {

                        Bitmap b = ImageHelper.readImage(sub.getThumbnailPath(), true);
                        if (b != null) {
                            bgDrawable = ImageHelper.bitmapToDrawable(b, context);
                            ivDrawing.setImageDrawable(bgDrawable);
//                            b.recycle();
                        }

                    }
                }

            }
        }
    }

    public void openWithAnimation() {
        setBgImage();
        if (project.isOpened != null && project.isOpened) {
            return;
        }
        View v = contentView;
        float closedHeight = context.getApplicationContext().getResources().getDimension(R.dimen.cell_height);
        float openHeight = context.getApplicationContext().getResources().getDimension(R.dimen.dp_400);
        v.clearAnimation();
        foregroundView.clearAnimation();
//        ResizeAnimation resizeAnimation = new ResizeAnimation(v, (int) closedHeight, (int)openHeight);
        ResizeAnimation resizeAnimation = new ResizeAnimation(v, (int) closedHeight, ivDrawing.getHeight());
        resizeAnimation.setDuration(cellOpenAnimationDuration);
        v.startAnimation(resizeAnimation);
        ObjectAnimator.ofFloat(v, "alpha", 0, 1).setDuration(cellOpenAnimationDuration).start();
        project.isOpened = true;
    }
}
