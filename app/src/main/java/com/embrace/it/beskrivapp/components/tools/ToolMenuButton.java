package com.embrace.it.beskrivapp.components.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

/**
 * Created by waseem on 2/14/17.
 */

public class ToolMenuButton extends android.support.v7.widget.AppCompatButton {

    public int getType() {
        return ButtonType;
    }

    public void setType(int buttonType) {
        ButtonType = buttonType;
    }

    public int getButtonId() {
        return buttonId;
    }

    public void setButtonId(int buttonId) {
        this.buttonId = buttonId;
    }

    public int ButtonType;
    public int buttonId;
    public ToolMenuButton(Context context) {
        super(context);
        this.setGravity(Gravity.CENTER);
        this.setTextAlignment(TEXT_ALIGNMENT_GRAVITY);

    }

    public ToolMenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolMenuButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
