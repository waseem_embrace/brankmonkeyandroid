package com.embrace.it.beskrivapp.commons;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by waseem on 2/9/17.
 */

public class SimpleTouchListener implements View.OnTouchListener, MyGestureDetector.SimpleGestureListenerCallback {

    private float xDown = 0, yDown = 0;
    private float xDownPrev = 0, yDownPrev = 0;
    SimpleTouchListenerCallback delegate;
    private View touchView;

    public final static int SWIPE_UP = 1;
    public final static int SWIPE_DOWN = 2;
    public final static int SWIPE_LEFT = 3;
    public final static int SWIPE_RIGHT = 4;

    private final static int ACTION_FAKE = -13; //just an unlikely number
    private int swipe_Min_Distance = 50;
    private int swipe_Max_Distance = 1000;
    private int swipe_Min_Velocity = 50;

    private GestureDetector mGestureDetector;
    public final int Max_Swipe_Sensivity = 20;
    public final int Min_Swipe_Sensivity = 1;


    /**
     * @param swipeSensitvity Within range of 1 - 20. 20 is most sensitive and 1 is least sensitive.
     */
    public void setSwipeSensitivity(int swipeSensitvity) {

        if (swipeSensitvity > Max_Swipe_Sensivity) {
            swipeSensitvity = Max_Swipe_Sensivity;
        } else if (swipeSensitvity < Min_Swipe_Sensivity) {
            swipeSensitvity = Min_Swipe_Sensivity;
        } else {
        }
        swipe_Min_Distance = ((Max_Swipe_Sensivity - swipeSensitvity) + 1) * 10;
        swipe_Min_Velocity = ((Max_Swipe_Sensivity - swipeSensitvity) + 1) * 10;

    }

    MyGestureDetector myGestDetector;

    public SimpleTouchListener(Context context, SimpleTouchListenerCallback callback) {
        this.delegate = callback;
        myGestDetector = new MyGestureDetector(this);
        mGestureDetector = new GestureDetector(context, myGestDetector);
    }

    Boolean swipeGestureDetected = false;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        swipeGestureDetected = false;
        touchView = v;
        return mGestureDetector.onTouchEvent(event);
    }


    private static float round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (float) Math.floor(value * scale) / scale;
    }

    @Override
    public void paneGesture(double xNet, double yNet, double xTotal, double yTotal) {
        this.delegate.pane(touchView, xNet, yNet, xTotal, yTotal);
    }

    @Override
    public void clickGesture(float x,float y) {
        this.delegate.click(touchView,x,y);
    }

    @Override
    public void swipeGesture(int direction) {
        if(!swipeGestureDetected) {
            swipeGestureDetected = true;
            this.delegate.swipe(touchView, direction);
        }
    }


    public interface SimpleTouchListenerCallback {
        void pane(View v, double xNet, double yNet, double xTotal, double yTotal);

        void click(View v,float x,float y);

        void swipe(View v, int direction);
    }


}
