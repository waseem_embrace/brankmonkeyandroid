package com.embrace.it.beskrivapp.components.touchbutton;

/**
 * Created by wasimshigri on 1/30/17.
 */

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants.ButtonGroupType;
import com.embrace.it.beskrivapp.commons.Constants.ToolType;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.components.BaseButton;

/**
 * Created by wasimshigri on 1/25/17.
 */

public class ButtonStationView extends ViewGroup implements View.OnClickListener {


    final int icons[] = {R.drawable.menu5_dark, R.drawable.menu2_dark, R.drawable.menu3_dark, R.drawable.menu4_dark, R.drawable.menu1_dark};
    final int groupIcons[] = {R.drawable.menu5, R.drawable.menu2, R.drawable.menu3, R.drawable.menu4, R.drawable.menu1};
    final int subIcons[][] = {
            {R.drawable.menu5_4_dark, R.drawable.menu5_1_dark},
            {R.drawable.menu2_sub_dark, R.drawable.menu2_1_dark, R.drawable.menu2_2_dark},
            {R.drawable.menu3_dark, R.drawable.menu3_1_dark, R.drawable.menu3_2_dark},
            {R.drawable.menu3_3_dark, R.drawable.menu4_2_dark, R.drawable.menu4_3_dark},
            {R.drawable.menu1_dark, R.drawable.menu1_1_dark}};

    final ToolType subButtonsType[][] = {
            {ToolType.Crop_Tool, ToolType.Brightness_Tool},
            {ToolType.Markings_Tool, ToolType.Icon_Tool, ToolType.Sign_Tool},
            {ToolType.Draw_Tool, ToolType.Draw_Area, ToolType.Shape_Tool},
            {ToolType.Brackets_Tool, ToolType.Count_Tool, ToolType.Angle_Tool},
            {ToolType.Text_Area_Tool, ToolType.Symbol_Tool}};

    private float x, y;

    public ButtonStationView(Context context, ButtonStationViewCallback callback) {
        super(context);
        initDimensions();
        orientationType = BaseViewOrientation.UP;
        this.delegate = callback;
        calculateContainerDimensions();
        setBackgroundColor(Color.parseColor("#0099b213"));
        thumbButton = new ThumbButton(context);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        thumbButton.setLayoutParams(params);
        addChildViews(context, ButtonGroupType.group0);
        addView(thumbButton);
    }


    public void refreshViews(float x, float y) {

        calculateContainerDimensions();
        findOrientation(x, y);
        refreshViews(x, y, ButtonGroupType.group0);
        if (thumbButton != null)
            thumbButton.setImageDrawable(null);


    }

    private void refreshViews(ButtonGroupType basebtnType) {
        refreshViews(this.x, this.y, basebtnType);
    }

    private void refreshViews(float x, float y, ButtonGroupType basebtnType) {

        this.x = x;
        this.y = y;
        removeAllViews();
        addChildViews(this.getContext(), basebtnType);
        addView(thumbButton);
        animations();


    }

    public ButtonStationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ButtonStationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ButtonStationView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

void initDimensions(){
    int widhChildFixed = 60,heightChildFixed = 60, widthBaseButtonFixed = 90,heightBaseButtonFixed = 90,paddingFixed = 10;
    paddingLeft = Utils.convertDpToPx(paddingFixed);
    paddingRight = Utils.convertDpToPx(paddingFixed);
    paddingTop = Utils.convertDpToPx(paddingFixed);
    paddingBottom = Utils.convertDpToPx(paddingFixed);

    widthChild = Utils.convertDpToPx(widhChildFixed);
    heightChild = Utils.convertDpToPx(heightChildFixed);
    widthBaseBtn = Utils.convertDpToPx(widthBaseButtonFixed);
    heightBaseBtn = Utils.convertDpToPx(heightBaseButtonFixed);
}
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        for (int i = 0; i < getChildCount(); i++) {
            mTmpChildRect.set(0, 0, widthChild, heightChild);
            final View child = getChildAt(i);

            int left, top, right, bottom = 0;
            mTmpChildRect = getChildRect(thumbButton);
            if (!child.equals(thumbButton)) {
                left = mTmpChildRect.left + widthBaseBtn / 2 - widthChild / 2;
                top = mTmpChildRect.top + heightBaseBtn / 2 - heightChild / 2;
                right = left + widthChild;
                bottom = top + heightChild;
                mTmpChildRect.set(left, top, right, bottom);
            }

//        Gravity.apply(lp.gravity, width, height, mTmpContainerRect, mTmpChildRect);
            child.layout(mTmpChildRect.left, mTmpChildRect.top,
                    mTmpChildRect.right, mTmpChildRect.bottom);
        }
        MyLog.e("", "");
//        animations();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(widthParent, widthSize);
        } else {
            width = widthParent;
        }

        int height;
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(heightParent, heightSize);
        } else {
            height = heightParent;
        }

        int maxHeight = 0;
        int maxWidth = 0;
        int childState = 0;
        for (int i = 0; i < getChildCount(); i++) {

            final View child = getChildAt(i);
//        if (child.getVisibility() != GONE) {
            // Measure the child.
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);

            // Update our size information based on the layout params.  Children
            // that asked to be positioned on the left or right go in those gutters.
//            final LayoutParams lp = (LayoutParams) child.getLayoutParams();
//            {
//                maxWidth = Math.max(maxWidth,
//                        child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);
//            }
//            maxHeight = Math.max(maxHeight,
//                    child.getMeasuredHeight() + lp.topMargin + lp.bottomMargin);
//
//
//            childState = combineMeasuredStates(childState, child.getMeasuredState());
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void measureChildWithMargins(View child, int parentWidthMeasureSpec, int widthUsed, int parentHeightMeasureSpec, int heightUsed) {
        super.measureChildWithMargins(child, parentWidthMeasureSpec, widthUsed, parentHeightMeasureSpec, heightUsed);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
//        animations();

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    //-----------------//
    void addChildViews(Context context, ButtonGroupType baseButtonType) {
        int iconsArray[] = null;
        ToolType subButtonType[] = null;
        thumbButton.setButtonGroupType(baseButtonType);

        switch (baseButtonType) {
            case group0:
                iconsArray = icons;
                break;
            case group1:
                iconsArray = subIcons[0];
                subButtonType = subButtonsType[0];
                break;
            case group2:
                iconsArray = subIcons[1];
                subButtonType = subButtonsType[1];
                break;
            case group3:
                iconsArray = subIcons[2];
                subButtonType = subButtonsType[2];
                break;
            case group4:
                iconsArray = subIcons[3];
                subButtonType = subButtonsType[3];
                break;
            case group5:
                iconsArray = subIcons[4];
                subButtonType = subButtonsType[4];
                break;
        }
        int childCount = iconsArray.length;
        int maxChild = 5;
        int i = 1;
        if (childCount > maxChild)
            childCount = maxChild;
        else if (childCount < maxChild) {
            switch (childCount) {
                case 1:
                    i = 3;
                    childCount = 3;
                    break;
                case 2:
                    i = 2;
                    childCount = 3;
                    break;
                case 3:
                    i = 2;
                    childCount = 4;
                    break;

            }
        }

        int skipedIndex = i;
        for (; i <= childCount; i++) {


            BaseButton btn = null;
//            Shape btn = new Square(context);

            switch (i) {
                case 1:
                    btn = ThumbSubButton.getNewInstance(context);
                    if (baseButtonType == ButtonGroupType.group0) {
                        btn.setButtonGroupType(ButtonGroupType.group1);
                    }
                    break;
                case 2:
                    btn = ThumbSubButton.getNewInstance(context);
                    if (baseButtonType == ButtonGroupType.group0) {
                        btn.setButtonGroupType(ButtonGroupType.group2);
                    }
                    break;
                case 3:
                    btn = ThumbSubButton.getNewInstance(context);
                    if (baseButtonType == ButtonGroupType.group0) {
                        btn.setButtonGroupType(ButtonGroupType.group3);
                    }
                    break;
                case 4:
                    btn = ThumbSubButton.getNewInstance(context);
                    if (baseButtonType == ButtonGroupType.group0) {
                        btn.setButtonGroupType(ButtonGroupType.group4);
                    }
                    break;
                case 5:
                    btn = ThumbSubButton.getNewInstance(context);
                    if (baseButtonType == ButtonGroupType.group0) {
                        btn.setButtonGroupType(ButtonGroupType.group5);
                    }
                    break;

            }
            btn.setImageResource(iconsArray[i - skipedIndex]);
            if (subButtonType != null)
                btn.setButtonType(subButtonType[i - skipedIndex]);
            else
                btn.setButtonType(ToolType.None);

            if (iconsArray.length == 2) {
                btn.buttonLocation = i - skipedIndex + 6;
            }else {
                btn.buttonLocation = i;
            }
            btn.setOnClickListener(this);
            btn.setBackgroundColor(Color.parseColor("#00000000"));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(widthChild, heightChild);
            btn.setLayoutParams(params);
            addView(btn);

        }

    }


    //---------_Variables----------------//
    private final Rect mTmpContainerRect = new Rect();
    private Rect mTmpChildRect = new Rect();
    private int widthChild = 0;
    private int heightChild = 0;
    public static int widthBaseBtn = 0;
    public static int heightBaseBtn = 0;
    private int widthParent = 0;
    private int heightParent = 0;
    private ThumbButton thumbButton;
    private ShapeDrawable mDrawable;
    private int paddingLeft = 0;
    private int paddingRight = 0;
    private int paddingTop = 0;
    private int paddingBottom = 0;

    private BaseViewOrientation orientationType;

    void calculateContainerDimensions() {
        widthParent = widthBaseBtn + (widthChild * 2) + paddingLeft * 3 + paddingRight * 3;
        heightParent = heightBaseBtn + heightChild + paddingTop * 2 + paddingBottom * 2;
    }

    void findOrientation(float touchX, float touchY) {
        orientationType = BaseViewOrientation.UP;
        int screenWidth = Utils.getScreenWidth();
        int screenHeight = Utils.getScreenHeight();
        int centerBaseBtnX = widthParent / 2;
        int centerBaseBtnY = heightParent - heightBaseBtn / 2 - paddingBottom;
        if (((touchX - centerBaseBtnX) >= 0) && ((touchX + centerBaseBtnX) <= screenWidth)) {
            //UP or DOWN
            if ((touchY - centerBaseBtnY) >= 0) {
                //UP
                orientationType = BaseViewOrientation.UP;
                setX(getX() - widthParent / 2);
                setY(getY() - heightParent + heightBaseBtn / 2 + paddingBottom);
            } else {
                //DOWN
                orientationType = BaseViewOrientation.DOWN;
                setX(getX() - widthParent / 2);
                setY(getY() - heightBaseBtn / 2 - paddingTop);
            }
        } else {
            int temp = widthParent;
            widthParent = heightParent;
            heightParent = temp;
            if ((touchX - centerBaseBtnY) >= 0) {
                //LEFT
                orientationType = BaseViewOrientation.LEFT;
                setX(getX() - widthParent + paddingLeft + widthBaseBtn / 2);
                setY(getY() - heightParent / 2);
            } else {
                //RIGHT
                orientationType = BaseViewOrientation.RIGHT;
                setX(getX() - paddingRight - widthBaseBtn / 2);
                setY(getY() - heightParent / 2);
            }
        }

    }

    @Override
    public void onClick(View v) {

        if (v instanceof BaseButton) {
            BaseButton b = (BaseButton) v;
            if (b.getButtonType() == ToolType.None) {
                if((b.buttonLocation -1) >= 0)
                    thumbButton.setImageDrawable(ImageHelper.getdrawable(groupIcons[b.buttonLocation - 1]));
                else
                    thumbButton.setImageDrawable(b.getDrawable());
                refreshViews(b.getButtonGroupType());
            } else {
                //TODO: remove after implementing
                this.delegate.toolSelected(b.getButtonType());
            }
        }
    }


    public enum BaseViewOrientation {
        UP(0), LEFT(1), RIGHT(2), DOWN(3);
        private int value;

        private BaseViewOrientation(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private Rect getChildRect(View child) {

        int left = 0, right = 0, top = 0, bottom = 0;
        switch (orientationType.getValue()) {

            case 0: {//UP
                if (child.equals(thumbButton)) {
                    left = (widthParent / 2) - widthBaseBtn / 2;
                    top = heightParent - heightBaseBtn - paddingBottom;
                    right = left + widthBaseBtn;
                    bottom = top + heightBaseBtn;
                } else {
                    BaseButton s = (BaseButton) child;
                    switch (s.buttonLocation) {
                        case 1:
                            left = (widthParent / 2) - widthBaseBtn / 2 - widthChild - paddingRight - paddingLeft*2;
                            top = heightParent - heightChild - paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 2:
                            left = (widthParent / 2) - widthChild - widthChild / 2 - paddingRight - paddingLeft;
                            top = heightParent - (heightChild * 2) - (paddingBottom * 2) - paddingTop;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 3:
                            left = (widthParent / 2) - widthChild / 2;
                            top = heightParent - heightBaseBtn - heightChild - (paddingBottom * 2) - paddingTop*2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 4:
                            left = (widthParent / 2) + widthChild / 2 + paddingRight + paddingLeft;
                            top = heightParent - (heightChild * 2) - (paddingBottom * 2)  - paddingTop;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 5:
                            left = (widthParent / 2) + widthBaseBtn / 2 + paddingRight + paddingLeft*2;
                            top = heightParent - heightChild - paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;

                        case 6://center Left
                            left = (widthParent / 2) - widthChild - paddingRight;
                            top = heightParent - heightBaseBtn - heightChild - (paddingBottom * 2);
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;

                        case 7://center Right
                            left = (widthParent / 2) + paddingLeft;
                            top = heightParent - heightBaseBtn - heightChild - (paddingBottom * 2);
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        default:
                            left = top = right = bottom = 0;
                            break;
                    }
                }
                break;
            }

            case 1: {//LEFT
                if (child.equals(thumbButton)) {
                    left = widthParent - widthBaseBtn - paddingRight;
                    top = heightParent / 2 - heightBaseBtn / 2;
                    right = left + widthBaseBtn;
                    bottom = top + heightBaseBtn;
                } else {
                    BaseButton s = (BaseButton) child;
                    switch (s.buttonLocation) {
                        case 1:
                            left = widthParent - widthChild - paddingRight;
                            top = heightParent / 2 + heightBaseBtn / 2 + paddingTop + paddingBottom*2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 2:
                            left = widthParent - widthChild * 2 - (paddingRight * 2) - paddingLeft;
                            top = heightParent / 2 + heightChild / 2 + paddingTop + paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 3:
                            left = widthParent - widthBaseBtn - widthChild - paddingRight*2 - paddingLeft*2;
                            top = heightParent / 2 - heightChild / 2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 4:
                            left = widthParent - widthChild * 2 - (paddingRight * 2) - paddingLeft;
                            top = heightParent / 2 - heightChild - heightChild / 2 - paddingTop * 2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 5:
                            left = widthParent - widthChild - paddingRight;
                            top = heightParent / 2 - heightBaseBtn / 2 - heightChild - paddingTop - paddingBottom*2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;

                        case 6://center Left
                            left = widthParent - widthBaseBtn - widthChild - paddingRight * 2 - paddingLeft;
                            top = heightParent / 2 - heightChild - paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;

                        case 7://center Right
                            left = widthParent - widthBaseBtn - widthChild - paddingRight * 2 - paddingLeft;
                            top = heightParent / 2 + paddingTop;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        default:
                            left = top = right = bottom = 0;
                            break;
                    }
                }
                break;
            }

            case 2: {//RIGHT
                if (child.equals(thumbButton)) {
                    left = paddingLeft;
                    top = heightParent / 2 - heightBaseBtn / 2;
                    right = left + widthBaseBtn;
                    bottom = top + heightBaseBtn;
                } else {
                    BaseButton s = (BaseButton) child;
                    switch (s.buttonLocation) {
                        case 5:
                            left = paddingLeft;
                            top = heightParent / 2 + heightBaseBtn / 2 + paddingTop + paddingBottom*2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 4:
                            left = widthChild + (paddingRight * 2) + paddingLeft;
                            top = heightParent / 2 + heightChild / 2 + paddingTop + paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 3:
                            left = widthBaseBtn + paddingRight * 2 + paddingLeft*2;
                            top = heightParent / 2 - heightChild / 2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 2:
                            left = widthChild + (paddingRight * 2) + paddingLeft;
                            top = heightParent / 2 - heightChild / 2 - heightChild - paddingTop - paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 1:
                            left = paddingLeft;
                            top = heightParent / 2 - heightBaseBtn / 2 - heightChild - paddingTop - paddingBottom*2;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 7:
                            left = widthBaseBtn + paddingRight * 2 + paddingLeft;
                            top = heightParent / 2 - heightChild - paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 6:
                            left = widthBaseBtn + paddingRight * 2 + paddingLeft;
                            top = heightParent / 2 + paddingTop;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        default:
                            left = top = right = bottom = 0;
                            break;
                    }
                }
                break;
            }

            case 3: {//DOWN
                if (child.equals(thumbButton)) {
                    left = (widthParent / 2) - widthBaseBtn / 2;
                    top = paddingTop;
                    right = left + widthBaseBtn;
                    bottom = top + heightBaseBtn;
                } else {
                    BaseButton s = (BaseButton) child;
                    switch (s.buttonLocation) {
                        case 5:
                            left = (widthParent / 2) - widthBaseBtn / 2 - widthChild - paddingRight - paddingLeft*2;
                            top = paddingTop;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 4:
                            left = (widthParent / 2) - widthChild - widthChild / 2 - paddingRight - paddingLeft;
                            top = heightChild + paddingBottom + (paddingTop * 2)  ;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 3:
                            left = (widthParent / 2) - widthChild / 2;
                            top = heightBaseBtn + (paddingTop * 2) + paddingBottom*2 ;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 2:
                            left = (widthParent / 2) + widthChild / 2 + paddingRight + paddingLeft;
                            top = heightChild + paddingBottom + (paddingTop * 2)  ;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 1:
                            left = (widthParent / 2) + widthBaseBtn / 2 + paddingRight + paddingLeft*2;
                            top = paddingTop;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 7:
                            left = (widthParent / 2) - widthChild - paddingRight;
                            top = heightBaseBtn + (paddingTop * 2) + paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        case 6:
                            left = (widthParent / 2) + paddingLeft;
                            top = heightBaseBtn + (paddingTop * 2) + paddingBottom;
                            right = left + widthChild;
                            bottom = top + heightChild;
                            break;
                        default:
                            left = top = right = bottom = 0;
                            break;
                    }
                }
                break;
            }
        }

        Rect rect = new Rect();
        rect.set(left, top, right, bottom);
        return rect;
    }


    public void animations() {

        Rect rectBase = getChildRect(thumbButton);
//        rectFrom.left = rectFrom.left + widthBaseBtn / 2 - widthChild/2;
//        rectFrom.top = rectFrom.top + heightBaseBtn / 2 - heightChild/2;
//        rectFrom.right = rectFrom.left + widthChild;
//        rectFrom.bottom = rectFrom.top + heightChild;

        float xFrom = (rectBase.left + widthBaseBtn / 2 - widthChild / 2);
        float yFrom = rectBase.top + heightBaseBtn / 2 - heightChild / 2;

        int ANIM_DURATION = 300;
        int ANIM_DELAY_BTWEEN = 80;
        int animCount = 0;
        AnimatorSet parentAnimSet = new AnimatorSet();

        for (int i = 0; i < getChildCount(); i++) {
            final View v = getChildAt(i);

            Rect mTmpChildRect = getChildRect(v);
            if (v.equals(thumbButton)) {
                continue;
            }
            v.clearAnimation();
//            xFrom = v.getX();
//            yFrom =v.getY();


            final float xTo = mTmpChildRect.left;
            final float yTo = mTmpChildRect.top;
            if (xTo == xFrom && yTo == yFrom)
                return;

            v.setVisibility(View.INVISIBLE);
            ObjectAnimator animX = ObjectAnimator.ofFloat(v, "x", xFrom, xTo);
            ObjectAnimator animY = ObjectAnimator.ofFloat(v, "y", yFrom, yTo);
            ObjectAnimator animAlpha = ObjectAnimator.ofFloat(v, "alpha", 0.0f, 1.0f);
//            ObjectAnimator animScaleX = ObjectAnimator.ofFloat(v, "scaleX", 0.1f, 1.0f);
//            ObjectAnimator animScaleY = ObjectAnimator.ofFloat(v, "scaleY", 0.1f, 1.0f);
            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(ANIM_DURATION);
//            animSet.playTogether(animX, animY, animScaleX, animScaleY);
            animSet.playTogether(animX, animY, animAlpha);
            parentAnimSet.play(animSet).after(ANIM_DELAY_BTWEEN * animCount);
            animSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    v.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {
//                        v.setX(x);
//                        v.setY(y);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animCount++;

        }

        parentAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {
//                        v.setX(x);
//                        v.setY(y);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        parentAnimSet.start();
    }


  /*  void startCircleProgress() {
        handler.postDelayed(mLongPressed, 600);
        this.thumbButton.showProgessForDuration(600);
    }

    void stopCircleProgress() {
        handler.removeCallbacks(mLongPressed);
        this.thumbButton.showProgessForDuration(600);
    }

    final Handler handler = new Handler();
    Runnable mLongPressed = new Runnable() {
        public void run() {

//       refreshViews();
        }
    };*/


    ButtonStationViewCallback delegate;

    public interface ButtonStationViewCallback {
        void toolSelected(ToolType toolType);
    }

}
