package com.embrace.it.beskrivapp.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.helpers.DrawingHelper;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.commons.helpers.MyGoogleLocation;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.commons.observables.EventBus;
import com.embrace.it.beskrivapp.components.tools.ToolMenuSubButton;
import com.embrace.it.beskrivapp.components.touchbutton.ButtonStationView;
import com.embrace.it.beskrivapp.components.touchbutton.ThumbButton;
import com.embrace.it.beskrivapp.database.DBController;
import com.embrace.it.beskrivapp.database.Project;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.ProjectMainItem;
import com.embrace.it.beskrivapp.database.SubProject;
import com.embrace.it.beskrivapp.fragments.CreatProjectFragment;
import com.embrace.it.beskrivapp.fragments.DrawingFragment;
import com.embrace.it.beskrivapp.fragments.HelpFragment;
import com.embrace.it.beskrivapp.fragments.MainFragment;
import com.embrace.it.beskrivapp.fragments.ProjectsFragment;
import com.embrace.it.beskrivapp.fragments.SubProjectsFragment;
import com.embrace.it.beskrivapp.fragments.camera.CameraFragment;
import com.embrace.it.beskrivapp.sticker.Sticker;
import com.embrace.it.beskrivapp.sticker.StickerView;
import com.embrace.it.beskrivapp.sticker.toolframs.EditTextSticker;
import com.embrace.it.beskrivapp.sticker.toolframs.MainImageSticker;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends FragmentActivity implements View.OnTouchListener, ButtonStationView.ButtonStationViewCallback, MainFragment.MainFragmentCallBack, CameraFragment.CameraFragmentCallback, SimpleTouchListener.SimpleTouchListenerCallback, CreatProjectFragment.CreateProjectCallback, DrawingHelper.DrawingHelperCallback {

    CardView contentview;
    ButtonStationView btn;
    ThumbButton touchProgessView;
    SimpleTouchListener mTouchlistener;
    StickerView stickerview;
    StickerView stickerviewImage;
    String projectID = null;
    DrawingHelper drawinghelper;
    CardView cvTitle;
    TextView tvTitle1;
    TextView tvTitle2;
    CameraFragment cameraFragment;
    ProjectsFragment projectsFragment;
    SubProjectsFragment subProjectFragment;
    HelpFragment helpFragment;
    DrawingFragment drawignfragment;
    boolean EdittingMode = false;
    List<Fragment> activeFragments = new ArrayList<Fragment>();
    Project project;
    SubProject subProject;
    ProjectMainItem projectmainItem;

    final String initialBottomLabel = "Hold fingeren nede for at åbne menuen";
    final String initialGuideScreenTopLabel = "Guide og navigation";
    final String initialGuideScreenBottomLabel = "Hvordan navigerer man rundt i appen?";
    final String galleryIsEmptyGoToCamera = "Du har ikke taget nogle billeder, vil du gå til kamera?";
    final String menuIsOpened = "Vælg en funktion";

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initMainActivity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMainActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateValuesInDB(0);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DBController.getInstance().closeRealm();
    }

    void initMainActivity() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_main);
        Bundle b = getIntent().getExtras();
        if (b != null)
            projectID = b.getString("projectId");
        contentview = (CardView) findViewById(R.id.main_contentview);
        stickerview = (StickerView) findViewById(R.id.main_stickerview);
        stickerviewImage = (StickerView) findViewById(R.id.main_imageview_sticker);
        tvTitle1 = (TextView) findViewById(R.id.tv_main_title1);
        tvTitle2 = (TextView) findViewById(R.id.tv_main_title2);
        cvTitle = (CardView) findViewById(R.id.cv_main_title);
        cvTitle.setBackgroundColor(Color.TRANSPARENT);

        drawinghelper = new DrawingHelper(stickerview, this);
        btn = new ButtonStationView(this, this);
        contentview.setOnTouchListener(this);
        touchProgessView = new ThumbButton(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(btn.widthBaseBtn, btn.heightBaseBtn);
        touchProgessView.setLayoutParams(params);
        mTouchlistener = new SimpleTouchListener(getApplicationContext(), this);
        mTouchlistener.setSwipeSensitivity(10);
        drawinghelper.initStickerView(getApplicationContext(), stickerview);
        stickerviewImage.setLocked(false);
        stickerviewImage.zoomOnPinch(true);

        Typeface boldTypeface = Utils.getFontTypeface(getApplicationContext(), Constants.FONT_FrutigerLTStd_Bold);
        Typeface romanTypeface = Utils.getFontTypeface(getApplicationContext(), Constants.FONT_FrutigerLTStd_Roman);
        tvTitle1.setTypeface(boldTypeface);
        tvTitle2.setTypeface(romanTypeface);
        handler.postDelayed(animateTitleHide, 3000);

        stickerview.setOnStickerOperationListener(new StickerOperationListener(stickerview));
        stickerview.setSimpleTouchlistener(mTouchlistener);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 110);
        } else {
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initProject(projectID, null);
                    }
                });
            }
        }, 500);
    }


    void setMainImage(Drawable drawable) {

        stickerviewImage.deleteStickerByTag(Constants.TAG_BG_IMAGE);
        drawinghelper.mainSticker = new MainImageSticker(drawable, ImageView.ScaleType.FIT_XY);
        drawinghelper.mainSticker.setToolType(Constants.ToolType.Main_Image);
        drawinghelper.mainSticker.setTag(Constants.TAG_BG_IMAGE);
        stickerviewImage.addStickerFitImage(drawinghelper.mainSticker);
        stickerviewImage.setOnStickerOperationListener(new StickerOperationListener(stickerviewImage));
        if (projectmainItem != null && projectmainItem.getItem() != null) {
            drawinghelper.mainSticker.setProjectItem(projectmainItem.getItem());
            //TODO:
            drawinghelper.initBrightnessContrast(drawinghelper.mainSticker.getBrightness(), drawinghelper.mainSticker.getContrast());
            updateBrightnessContrast(drawinghelper.calcualteBrightness(), drawinghelper.calcualteContrast(), 0, 0,2);
//            drawinghelper.initBrightnessContrast(0, 5);
            stickerviewImage.invalidate();
        }
    }

    int downX = 0, downY = 0;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = (int) event.getX();
                downY = (int) event.getY();
                contentview.removeView(btn);
                contentview.removeView(touchProgessView);
                if (touchProgessView.getWidth() == 0.0) {
                    touchProgessView.setX(event.getX() - ButtonStationView.widthBaseBtn / 2);
                    touchProgessView.setY(event.getY() - ButtonStationView.heightBaseBtn / 2);
                } else {
                    touchProgessView.setX(event.getX() - touchProgessView.getWidth() / 2);
                    touchProgessView.setY(event.getY() - touchProgessView.getHeight() / 2);
                }
                contentview.addView(touchProgessView);
                handler.postDelayed(mLongPressed, 600);
                touchProgessView.showProgessForDuration(600);
                return true;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_CANCEL:
                contentview.removeView(touchProgessView);
                handler.removeCallbacks(mLongPressed);
                return true;

            case MotionEvent.ACTION_MOVE:
                if (Math.abs(event.getX() - downX) > 1 || Math.abs(event.getY() - downY) > 1) {
                    contentview.removeView(touchProgessView);
                    handler.removeCallbacks(mLongPressed);
                }
                return false;
        }
        return true;
    }

    final Handler handler = new Handler();
    Runnable mLongPressed = new Runnable() {
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showIconsView(touchProgessView.getX() + touchProgessView.getWidth() / 2, touchProgessView.getY() + touchProgessView.getHeight() / 2);
                    contentview.removeView(touchProgessView);
                    updateTitle(menuIsOpened);

                }
            });
        }
    };

    void showIconsView(float x, float y) {
        contentview.removeView(btn);
        contentview.removeView(touchProgessView);
        btn.setX(x);
        btn.setY(y);
        contentview.addView(btn);
        btn.refreshViews(x, y);
    }

    public void createNewProject() {
        EditProject(null);
    }

    public void EditProject(String projectId) {
        CreatProjectFragment f = new CreatProjectFragment();
        f.setDelegate(this);
        if (projectId != null) {
            Bundle bundle = new Bundle();
            bundle.putString("projectId", projectId);
            f.setArguments(bundle);

        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.enter_from_left, R.animator.exit_to_right, R.animator.enter_from_right, R.animator.exit_to_left);
        transaction.replace(R.id.main_contentview, f);
        transaction.addToBackStack(NewProject_Fragment).commit();
    }


    public void showSubProjectsScreen() {
        if (subProjectFragment != null)
            activeFragments.remove(subProjectFragment);

        subProjectFragment = null;
        subProjectFragment = new SubProjectsFragment();
//        f.setDelegate(this);
        if (project != null) {
            Bundle bundle = new Bundle();
            bundle.putString("projectId", project.getProjectId());
            if (subProject != null && subProject.getProjectItems() != null) {
                bundle.putString("subProjectId", subProject.getSubProjectId());
            }
            subProjectFragment.setArguments(bundle);
        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left, R.animator.enter_from_left, R.animator.exit_to_right);
        transaction.replace(R.id.main_contentview, subProjectFragment);
        transaction.addToBackStack(SubProjects_Fragment).commit();
        activeFragments.add(subProjectFragment);
        cvTitle.setVisibility(View.INVISIBLE);
    }


    //-------_MAIN FRAGMENT CallBack------------//
    @Override
    public void showHelpScreen() {
        try {
            if (helpFragment != null)
                activeFragments.remove(helpFragment);
            else
                helpFragment = new HelpFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.animator.enter_from_top, R.animator.exit_to_bottom, R.animator.enter_from_bottom, R.animator.exit_to_top);
            transaction.replace(R.id.main_contentview, helpFragment);
            activeFragments.add(helpFragment);
            transaction.addToBackStack(Help_Fragment).commit();
            cvTitle.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            helpFragment = null;
        }
    }

    @Override
    public void showProjects() {
        if (projectsFragment != null)
            activeFragments.remove(projectsFragment);
        else
            projectsFragment = new ProjectsFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.enter_from_top, R.animator.exit_to_bottom, R.animator.enter_from_bottom, R.animator.exit_to_top);
        transaction.replace(R.id.main_contentview, projectsFragment);
        activeFragments.add(projectsFragment);
        transaction.addToBackStack(Projects_Fragment).commit();
        cvTitle.setVisibility(View.INVISIBLE);

    }


    @Override
    public void showCamera() {
        if (cameraFragment != null)
            activeFragments.remove(cameraFragment);
        else
            cameraFragment = new CameraFragment();

        cameraFragment.setProjectId(projectID);
        cameraFragment.setCallback(this);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.enter_from_left, R.animator.exit_to_right, R.animator.enter_from_right, R.animator.exit_to_left);
        transaction.replace(R.id.main_contentview, cameraFragment);
        activeFragments.add(cameraFragment);
        transaction.addToBackStack(Camera_Fragment).commit();
        cvTitle.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showDrawingPad(Constants.ToolType tooltype) {
        if (drawignfragment != null)
            activeFragments.remove(drawignfragment);
        drawignfragment = new DrawingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("tooltype", tooltype);
        drawignfragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.main_contentview, drawignfragment);
        activeFragments.add(drawignfragment);
        transaction.addToBackStack(Drawing_Fragment).commit();
//        cvTitle.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setBackgorundImageEdittingMode(Constants.ToolType tooltype) {
        Sticker sticker = stickerviewImage.getStickerByTag(Constants.TAG_BG_IMAGE);

        if (tooltype == Constants.ToolType.Crop_Tool) {
            matrixBeforeCrop = new Matrix();
            matrixBeforeCrop.set(sticker.getMatrix());
            stickerviewImage.setLocked(false);
            stickerview.setLocked(true);
            stickerview.setVisibility(View.INVISIBLE);
            stickerviewImage.drawGrid(true);
            stickerviewImage.setHandlingSticker(Constants.TAG_BG_IMAGE);
        } else {
            stickerviewImage.invalidate();
            stickerviewImage.setLocked(true);
            stickerview.setLocked(false);
            bgImageEditted = bgImageOriginal.copy(bgImageOriginal.getConfig(),true);
        }
    }

    @Override
    public void dismissBackgroundImageEdittingMode() {
        stickerviewImage.setLocked(true);
        stickerview.setLocked(false);
        stickerview.setVisibility(View.VISIBLE);
        drawinghelper.removeToolMenu(contentview);
        stickerviewImage.drawGrid(false);
        EdittingMode = false;

        Sticker sticker = stickerviewImage.getStickerByTag(Constants.TAG_BG_IMAGE);

    }

    public static double calculateContrast(float contrast){
        double new_contrast_progress = 0.0;
        if (contrast  <= 5) {
            new_contrast_progress =  ((double)contrast) / 5f;
        } else {
            new_contrast_progress = 1 + (((double)contrast - 5) / 5f) * 9;
        }
       return new_contrast_progress;
    }
    @Override
    public void updateBrightnessContrast(int brightness, float contrast, int brightnessNet, float contrastNet,int mainIndex) {
       Bitmap bmpTemp = null;
        double new_contrast_progress = calculateContrast(contrast);
        if(mainIndex == 0) {
            bmpTemp =ImageHelper.changeBitmapBrightness(bgImageEditted, bgImageEditted, brightness,brightnessNet);
        }else if(mainIndex == 1) {
            bmpTemp = ImageHelper.changeBitmapContrast(bgImageEditted, bgImageEditted, (float)new_contrast_progress,contrastNet);
//            bgImageEditted = ImageHelper.changeBitmapContrastBrightness(bgImageOriginal, bgImageEditted, (float)(new_contrast_progress ), 0);
        }else{
            bmpTemp = ImageHelper.changeBitmapContrast(bgImageEditted, bgImageEditted, (float)new_contrast_progress,contrastNet);
            bmpTemp =ImageHelper.changeBitmapBrightness(bmpTemp, bgImageEditted, brightness,brightnessNet);
        }
//        bgImageEditted = ImageHelper.changeBitmapContrastBrightness(bgImageOriginal, bgImageEditted, contrast, brightness);
        Sticker sticker = stickerviewImage.getStickerByTag(Constants.TAG_BG_IMAGE);
        sticker.setDrawable(ImageHelper.bitmapToDrawable(bmpTemp, getApplicationContext()));
        //TODO:
        drawinghelper.mainSticker.brightnessContrast(brightness,contrast);
        stickerviewImage.invalidate();

    }

    @Override
    public void brightnessToolMenuChanged(int mainIndex,int brightness,float contrast) {
        brightness =  drawinghelper.mainSticker.getBrightness();
        contrast =  drawinghelper.mainSticker.getContrast();
        if(mainIndex == 1) {
            bgImageEditted =ImageHelper.changeBitmapBrightness(bgImageOriginal, bgImageEditted, brightness,0);
        }else if(mainIndex == 0) {
            double new_contrast_progress = calculateContrast(contrast);
            bgImageEditted = ImageHelper.changeBitmapContrast(bgImageOriginal, bgImageEditted, (float)new_contrast_progress,0);
        }
    }

    @Override
    public void undoCrop() {
        Sticker sticker = stickerviewImage.getStickerByTag(Constants.TAG_BG_IMAGE);
        sticker.setMatrix(matrixBeforeCrop);
        stickerviewImage.invalidate();
        dismissBackgroundImageEdittingMode();
    }

    @Override
    public void updateTitle(String title) {
        tvTitle2.setText(title);
        cvTitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void setCrop() {
        dismissBackgroundImageEdittingMode();
        updateValuesInDB(2);
    }

    //------- CameraPreview Callback ---------//
    @Override
    public void cancelCamera() {

    }


    Bitmap bgImageOriginal = null;
    Bitmap bgImageEditted = null;
    Matrix matrixBeforeCrop;

    @Override
    public void pictureCapturedSuccessfuly(final String path, final String fileName) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int count = getFragmentManager().getBackStackEntryCount();
                if (count > 0) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    for (Fragment f : activeFragments) {
                        try {
                            fragmentTransaction.remove(f);
                            getFragmentManager().popBackStack();

                        } catch (Exception e) {
                        }
                    }
                    activeFragments.clear();
                    try {
                        fragmentTransaction.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                cvTitle.setVisibility(View.VISIBLE);
                stickerview.removeAllStickers();
                stickerviewImage.removeAllStickers();

                projectmainItem = null;
                subProject = null;
                stickerviewBgImgBmp = null;
                stickerviewFgImgBmp = null;
                bgImageOriginal = ImageHelper.readImage(path, false);
                bgImageEditted = bgImageOriginal.copy(bgImageOriginal.getConfig(),true);
                final Drawable drawable = ImageHelper.bitmapToDrawable(bgImageOriginal, getApplicationContext());
                setMainImage(drawable);

                ProjectItem projectimtem = drawinghelper.mainSticker.getProjectItem();
                int subProjectCount = 0;
                if (project.getSubProjects() != null)
                    for (SubProject p : project.getSubProjects()) {
                        if (p.equals(subProject))
                            break;
                        subProjectCount++;
                    }
                String subProjectId = "" + project.getProjectId() + subProjectCount;
                subProject = DBController.addSubProject(project, projectimtem, subProjectId, path, fileName);
                String dateString = null;
                String gpsString = null;
                String timeString = null;
                projectmainItem = subProject.getMainItem();
                if(project.getAlbum().getDate()){
                    dateString= Utils.getCurrentDateString();
                }
                if(project.getAlbum().getTime()){
                    timeString= Utils.getCurrentTimeString();
                }
                if(project.getAlbum().getGps()){
                    gpsString= Utils.coordinateString(MyGoogleLocation.sharedInstance().getLongitude(),MyGoogleLocation.sharedInstance().getLatitude());
                }
                DBController.updateSubProject(subProject,timeString,dateString,gpsString);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateValuesInDB(0);
                            }
                        });
                    }
                }, 500);
            }
        });
    }


    private static final String Projects_Fragment = "Projects_Fragment";
    private static final String NewProject_Fragment = "NewProject_Fragment";
    private static final String SubProjects_Fragment = "SubProjects_Fragment";
    private static final String Info_Fragment = "Info_Fragment";
    private static final String Help_Fragment = "Help_Fragment";
    private static final String Drawing_Fragment = "Drawing_Fragment";
    private static final String Camera_Fragment = "Camera_Fragment";

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
            String tag = backEntry.getName();
            if (tag.equals(Help_Fragment)) {
                finish();
            } else {
                getFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();
        }
    }


    //-------_MyGestureDetector Callback-------------//


    @Override
    public void pane(View v, double xNet, double yNet, double xTotal, double yTotal) {
        contentview.removeView(touchProgessView);
        handler.removeCallbacks(mLongPressed);
    }

    @Override
    public void click(View v, float x, float y) {

    }

    @Override
    public void swipe(View v, int direction) {
        contentview.removeView(touchProgessView);
        handler.removeCallbacks(mLongPressed);
        if (!EdittingMode) {

            switch (direction) {
                case SimpleTouchListener.SWIPE_RIGHT:
                    showCamera();
//                    updateValuesInDB(0);
                    break;
                case SimpleTouchListener.SWIPE_DOWN:
                    showProjects();
//                    updateValuesInDB();
                    break;
                case SimpleTouchListener.SWIPE_LEFT:
                    showSubProjectsScreen();
                    break;
                case SimpleTouchListener.SWIPE_UP:
                    break;
            }
        }
    }

    @Override
    public void startDrawing(String projectID) {

        this.projectID = projectID;
        int count = getFragmentManager().getBackStackEntryCount();
        if (count > 0) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            for (Fragment f : activeFragments) {
                try {
                    fragmentTransaction.remove(f);
                    getFragmentManager().popBackStack();
                } catch (Exception e) {
                }
            }
            activeFragments.clear();
            try {
                fragmentTransaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        cvTitle.setVisibility(View.VISIBLE);
        initProject(projectID, null);
        updateValuesInDB(0);

        projectsFragment = null;
    }


    public void selectSubProejct(String subProjectID) {


        if (subProjectID.equals("empty")) {
            initProject(projectID, null);
        } else {
            cvTitle.setVisibility(View.VISIBLE);
            initProject(projectID, subProjectID);
//            updateValuesInDB();
        }
    }

    public void popFragment(Fragment f) {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count > 0) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            try {
                fragmentTransaction.remove(f);
                getFragmentManager().popBackStack();
            } catch (Exception e) {
            }
            activeFragments.remove(f);
            try {
                fragmentTransaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void initProject(String projectID, String subProejctID) {

        //TODO: testiing

//        initProjectTesting("" + 1);
//        if (1 == 1) {
//            return;
//        }
        stickerviewBgImgBmp = null;
        stickerviewFgImgBmp = null;
        if (projectID != null) {
            projectmainItem = null;
            subProject = null;
            try {
                if (bgImageOriginal != null)
                    bgImageOriginal.recycle();
                if (bgImageEditted != null)
                    bgImageEditted.recycle();
            } catch (Exception e) {
            }

            stickerview.removeAllStickers();
            stickerviewImage.removeAllStickers();
            project = DBController.getProject(projectID);
            if (project == null || (project.getSubProjects().size() < 1)) {
                showHelpScreen();
            } else {
                if (subProejctID != null) {
                    for (SubProject s : project.getSubProjects()) {
                        if (s.getSubProjectId().equals(subProejctID)) {
                            subProject = s;
                            break;
                        }
                    }
                }

                if (subProject == null) {
                    subProject = project.getSubProjects().first();
                }

                if (project.getAlbum() != null) {
                    tvTitle1.setText(project.getAlbum().getAlbumTitle());
                }

                if (subProject != null) {
                    projectmainItem = subProject.getMainItem();
                }

                if (projectmainItem != null && projectmainItem.getImagePath() != null && !projectmainItem.getImagePath().equals("")) {
                    bgImageOriginal = ImageHelper.readImage(projectmainItem.getImagePath(), false);
                    bgImageEditted = bgImageOriginal.copy(bgImageOriginal.getConfig(),true);
                    final Drawable drawable = ImageHelper.bitmapToDrawable(bgImageOriginal, getApplicationContext());
                    setMainImage(drawable);
                    stickerview.setProjectItemList(getApplicationContext(), subProject.getProjectItems());
//                    stickerview.setProjectItemList(getApplicationContext(), subProject.getProjectItems());
                } else {
                    showHelpScreen();
                }
            }
        } else

        {
            showHelpScreen();
        }
    }

    void initProjectTesting(String projectID) {
        updateValuesInDB(0);
        if (projectID != null) {
            projectmainItem = null;
            subProject = null;
            project = DBController.getProject(projectID);
            if (project.getSubProjects() != null && project.getSubProjects().size() > 0)
                subProject = project.getSubProjects().first();

            if (subProject != null) {
                projectmainItem = subProject.getMainItem();
            }

            bgImageOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.earth_day);
            final Drawable drawable = ImageHelper.bitmapToDrawable(bgImageOriginal, getApplicationContext());
            setMainImage(drawable);

            if (subProject != null) {
                projectmainItem = subProject.getMainItem();
            } else {
                Sticker sticker = stickerviewImage.getStickerByTag(Constants.TAG_BG_IMAGE);
                ProjectItem projectimtem = sticker.getProjectItem();
                subProject = DBController.addSubProject(project, projectimtem, "11", "", "");
                projectmainItem = subProject.getMainItem();

            }
//          bgImageOriginal = ImageHelper.readImage(projectmainItem.getImagePath());
            stickerview.removeAllStickers();
            stickerview.setProjectItemList(getApplicationContext(), subProject.getProjectItems());
        } else {
            showHelpScreen();
        }
    }

    @Override
    public void toolSelected(Constants.ToolType toolType) {
        drawinghelper.handleAddNewTool(getApplicationContext(), contentview, stickerview, toolType,downX,downY);
        contentview.removeView(btn);
        EdittingMode = true;
        if (toolType == Constants.ToolType.Text_Area_Tool) {
            stickerview.editText();
            stickerview.addEditText(getApplicationContext(),contentview,null);
        }
    }


    private class StickerOperationListener implements StickerView.OnStickerOperationListener {
        String TAG = "Sticker Operation";
        StickerView stickerview;
        Sticker prevStickerSelected;

        public StickerOperationListener(StickerView stickerview) {
            this.stickerview = stickerview;
        }

        @Override
        public void onStickerClicked(Sticker sticker) {
            //stickerView.removeAllSticker();
            if (!stickerview.equals(stickerviewImage)) {
                contentview.removeView(btn);
                contentview.removeView(touchProgessView);
                if (sticker instanceof EditTextSticker) {
//                    ((TextSticker) sticker).setTextColor(Color.RED);
//                    stickerview.replace(sticker);
//                    stickerview.invalidate();
                    if (EdittingMode && prevStickerSelected != null && prevStickerSelected.equals(sticker)) {
                        stickerview.editText();
                        sticker.getInitialFrame();
                        sticker.getFrame();

//                        stickerview.addEditText(getApplicationContext(),contentview,(EditTextSticker)sticker);
                    }

                } else {
                    stickerview.dismissEditText();
//                    stickerview.removeEditText(contentview);
                }
                prevStickerSelected = sticker;
                Log.d(TAG, "onStickerClicked");
                bgImageEditted = null;
                drawinghelper.addToolMenu(contentview, getApplicationContext(), sticker, sticker.getData(), sticker.getToolType());
                EdittingMode = true;

            }
        }

        @Override
        public void onStickerDeleted(Sticker sticker) {
            Log.d(TAG, "onStickerDeleted");
            drawinghelper.removeToolMenu(contentview);
            EdittingMode = false;
            updateValuesInDB(1);
            stickerview.dismissEditText();
            stickerview.removeEditText(contentview);
            updateTitle(initialBottomLabel);
        }

        @Override
        public void onStickerDragFinished(Sticker sticker) {
            Log.d(TAG, "onStickerDragFinished");
        }

        @Override
        public void onStickerZoomFinished(Sticker sticker) {
            Log.d(TAG, "onStickerZoomFinished");
        }

        @Override
        public void onStickerRotateFinished(Sticker sticker) {

        }

        @Override
        public void onStickerFlipped(Sticker sticker) {
            Log.d(TAG, "onStickerFlipped");
        }

        @Override
        public void onStickerDoubleTapped(Sticker sticker) {
            Log.d(TAG, "onDoubleTapped: double tap will be with two click");
        }

        @Override
        public void touchedDown(float x, float y,Boolean overAnySticker) {
            if (!stickerview.equals(stickerviewImage)) {
                downX = (int) x;
                downY = (int) y;
                contentview.removeView(btn);
                contentview.removeView(touchProgessView);
                if (EdittingMode == false) {
                    if (touchProgessView.getWidth() == 0.0) {
                        touchProgessView.setX(x - ButtonStationView.widthBaseBtn / 2);
                        touchProgessView.setY(y - ButtonStationView.heightBaseBtn / 2);
                    } else {
                        touchProgessView.setX(x - touchProgessView.getWidth() / 2);
                        touchProgessView.setY(y - touchProgessView.getHeight() / 2);
                    }
                    contentview.addView(touchProgessView);
                    handler.postDelayed(mLongPressed, 600);
                    updateTitle(initialBottomLabel);
                    touchProgessView.showProgessForDuration(600);
                } else {

                }
            }
        }

        @Override
        public void onStickerViewClicked(float x, float y) {
            if (!stickerview.equals(stickerviewImage)) {
                contentview.removeView(btn);
                contentview.removeView(touchProgessView);
                drawinghelper.removeToolMenu(contentview);
                stickerview.clearEditing();
                if (EdittingMode)
                    updateValuesInDB(1);
                EdittingMode = false;
                stickerview.dismissEditText();
                stickerview.removeEditText(contentview);
                updateTitle(initialBottomLabel);
            }
        }

        @Override
        public void touchedUp() {
            contentview.removeView(touchProgessView);
            handler.removeCallbacks(mLongPressed);
        }

    }

    public void addToolForBitmap(String resourcePath, Constants.ToolType tooltype) {
        drawinghelper.addToolItem(getApplicationContext(), contentview, stickerview, tooltype, resourcePath,0,0);
        EdittingMode = true;

    }

    public void addToolForDrawing(List<PointF> drawingPints, Constants.ToolType tooltype) {
        drawinghelper.addToolItem(getApplicationContext(), contentview, stickerview, drawingPints, tooltype);
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }
        EdittingMode = true;
    }

    Bitmap stickerviewFgImgBmp;
    Bitmap stickerviewBgImgBmp;
/*
value: 0= get both foreGround and backGround stickrView images
        1:get only foreGroundImage
        2: get only backgroundImage
 */
    void setThumbnailImage(int value, final String subProejctId) {
        try {

            Bitmap imageBg;
            Bitmap imageOverlay;
            switch (value) {
                case 0:
                    if (stickerviewFgImgBmp != null)
                        stickerviewFgImgBmp.recycle();
                    if (stickerviewBgImgBmp != null)
                        stickerviewBgImgBmp.recycle();
                    stickerviewFgImgBmp = stickerview.createBitmap();
                    stickerviewBgImgBmp = stickerviewImage.createBitmap();
                    break;

                case 1:
                    if (stickerviewFgImgBmp != null)
                        stickerviewFgImgBmp.recycle();
                    stickerviewFgImgBmp = stickerview.createBitmap();
                    if (stickerviewBgImgBmp == null)
                        stickerviewBgImgBmp = stickerviewImage.createBitmap();

                    break;

                case 2:
                    if (stickerviewBgImgBmp != null)
                        stickerviewBgImgBmp.recycle();
                    stickerviewBgImgBmp = stickerviewImage.createBitmap();
                    if (stickerviewFgImgBmp == null)
                        stickerviewFgImgBmp = stickerview.createBitmap();
                    break;
            }

            imageOverlay = stickerviewFgImgBmp;
            imageBg = stickerviewBgImgBmp;
            Bitmap thumbnail = ImageHelper.overlay(imageBg, imageOverlay);

            final String path = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);
            final String pathBackgorund = ImageHelper.saveBitmapToInternalStorage(imageBg, thumbnailTitle + "background");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DBController.updateThumbnailImage(subProejctId, path);
                    EventBus.getThumbnailSavedObserver().notifyThumbnailSaved(projectID,thumbnailTitle);
                }
            });
//            imageBg.recycle();
//            imageOverlay.recycle();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String thumbnailTitle = "";

    void updateValuesInDB(final int value) {
        try {
            thumbnailTitle = subProject.getSubProjectId();
            new Thread(new Runnable() {
                @Override
                public void run() {
            if (projectmainItem != null) {
                setThumbnailImage(value,thumbnailTitle);
            }
                }
            }).start();

            try {
                DBController.updateProjectItem(projectmainItem, drawinghelper.mainSticker.getProjectItem());
                DBController.updateSubProject(subProject.getSubProjectId(), stickerview.getProjectItemList());
//                DBController.updateProjectItem(subProject, stickerview.getProjectItemList());
                MyLog.emptyLog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //-------Hide first tile with animation---------//
    Runnable animateTitleHide = new Runnable() {
        public void run() {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            animateTitleToHide();
                        } catch (Exception e) {

                        }
                    }
                });
            } catch (Exception e) {
            }
        }
    };

    void animateTitleToHide() {
        View view = (View) tvTitle1.getParent();
        Animator anim1 = ObjectAnimator.ofFloat(view, "y", view.getY(), view.getY() - tvTitle1.getHeight());
        Animator anim2 = ObjectAnimator.ofFloat(tvTitle1, "alpha", 1.0f, 0.0f);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(1000);
        set.playTogether(anim1, anim2);
        set.start();
    }

    public String getNextSubProjectId(Project project){
        int subProjectCount = 0;
        if (project.getSubProjects() != null)
            for (SubProject p : project.getSubProjects()) {
                subProjectCount++;
            }
        String subProjectId = "" + project.getProjectId() + subProjectCount;
        return subProjectId;
    }
    //-----------Export Image--------------//

    public boolean duplicateImage(SubProject sub, String title) {
        ProjectItem projectimtem = MainImageSticker.getDefaultProjectItem();
        String subProjId = sub.getSubProjectId();
        String newSubProjectId = getNextSubProjectId(project);
        String mainImgPath  = sub.getMainItem().getImagePath();
        thumbnailTitle = newSubProjectId;
        SubProject subPro;
        Bitmap thumbnail = null;
        String pathMain = null;

        Bitmap b = ImageHelper.readBitmapFromInternalStorage(subProjId + "background",false);
        if (b != null) {
            thumbnail = ImageHelper.readBitmapFromInternalStorage(subProjId + "background", true);
        } else {
            b = ImageHelper.readBitmapFromInternalStorage(mainImgPath,false);
            thumbnail = ImageHelper.readBitmapFromInternalStorage(mainImgPath, true);
        }

        if(b == null || thumbnail == null)
            return false;

        pathMain = ImageHelper.saveBitmapToInternalStorage(b, newSubProjectId + "background");
        final String pathThumbnail = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);
        subPro = DBController.addSubProject(project, projectimtem, newSubProjectId, pathMain, title);

//        thumbnailTitle = subPro.getSubProjectId();
//        if(thumbnail == null) {
//            thumbnail = ImageHelper.readImage(sub.getMainItem().getImagePath(), true);
//        }
//        final String path = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);
        DBController.updateThumbnailImage(subPro.getSubProjectId(), pathThumbnail);
        return  true;
    }

    public boolean duplicateImage1(SubProject sub, String title) {
        ProjectItem projectimtem = MainImageSticker.getDefaultProjectItem();

        String subProjectId = getNextSubProjectId(project);
        thumbnailTitle = subProjectId;
        SubProject subPro;
        Bitmap thumbnail = null;
        String pathMain = null;
        Bitmap b = ImageHelper.readBitmapFromInternalStorage(sub.getSubProjectId() + "background",false);
        if (b != null) {
            thumbnail = ImageHelper.readBitmapFromInternalStorage(sub.getSubProjectId() + "background", true);
        } else {
            b = ImageHelper.readBitmapFromInternalStorage(sub.getMainItem().getImagePath(),false);
            thumbnail = ImageHelper.readBitmapFromInternalStorage(sub.getMainItem().getImagePath(), true);
        }

        if(b == null || thumbnail == null)
            return false;

        pathMain = ImageHelper.saveBitmapToInternalStorage(b, subProjectId + "background");
        final String pathThumbnail = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);
        subPro = DBController.addSubProject(project, projectimtem, subProjectId, pathMain, title);

//        thumbnailTitle = subPro.getSubProjectId();
//        if(thumbnail == null) {
//            thumbnail = ImageHelper.readImage(sub.getMainItem().getImagePath(), true);
//        }
//        final String path = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);
        DBController.updateThumbnailImage(subPro.getSubProjectId(), pathThumbnail);
        return  true;
    }

    public boolean createProject(Bitmap b) {
        ProjectItem projectimtem = MainImageSticker.getDefaultProjectItem();
        String fileName = "Importeret Billede";
        String subProjectId = getNextSubProjectId(project);
        thumbnailTitle = subProjectId;
        SubProject subPro;
        Bitmap thumbnail = null;
        String pathMain = null;
        String path = ImageHelper.saveBitmapToInternalStorage(b,System.currentTimeMillis() + ".jpeg");
        if (b != null) {
            thumbnail = ImageHelper.readImage(path,true);
        }

        if(b == null || thumbnail == null)
            return false;

        pathMain = ImageHelper.saveBitmapToInternalStorage(b, subProjectId + "background");
        final String pathThumbnail = ImageHelper.saveBitmapToInternalStorage(thumbnail, thumbnailTitle);
        subPro = DBController.addSubProject(project, projectimtem, subProjectId, pathMain, fileName);
        DBController.updateThumbnailImage(subPro.getSubProjectId(), pathThumbnail);
        return  true;
    }

}

