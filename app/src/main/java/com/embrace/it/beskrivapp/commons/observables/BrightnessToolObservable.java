package com.embrace.it.beskrivapp.commons.observables;

import java.util.Observable;

/**
 * Created by waseem on 2/21/17.
 */

public class BrightnessToolObservable extends Observable {


    public void notifyItemClicked(Object obj){
        setChanged();
        notifyObservers(obj);
    }
}
