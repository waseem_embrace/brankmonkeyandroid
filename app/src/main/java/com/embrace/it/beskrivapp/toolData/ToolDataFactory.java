package com.embrace.it.beskrivapp.toolData;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants.ToolType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by waseem on 2/14/17.
 */

public class ToolDataFactory {

    public static ToolData getToolData(ToolType tooltype) {
        ToolData data = new ToolData();

        switch (tooltype) {
            case Crop_Tool: //0,0
                Integer values00[] = {50};
                data.getEditTools().add(new ToolItemData(new ArrayList<Integer>(), Arrays.asList(values00), "Ok"));
                data.getEditTools().add(new ToolItemData(new ArrayList<Integer>(), Arrays.asList(values00), "Annuller"));
                data.setActiveText("Zoom og roter billedet");

                break;
            case Brightness_Tool: //0,1

                Integer values10[] = {50};
                data.getEditTools().add(new ToolItemData(new ArrayList<Integer>(), Arrays.asList(values10), "Lysstyrke","Indstil lysstyrke"));
                data.getEditTools().add(new ToolItemData(new ArrayList<Integer>(), Arrays.asList(values10), "Kontrast","Indstil kontrast"));
                data.getEditTools().add(new ToolItemData(new ArrayList<Integer>(), Arrays.asList(values10), "Annuller"));

                data.setActiveText("Indstil lysstyrke eller kontrast");
//                data.setNonActiveText("Indstil lysstyrke eller kontrast");
                break;
            case Markings_Tool: //1,0

                List<Integer> markers = new ArrayList<Integer>();
                markers.add(R.drawable.menu_markoer_01);
                markers.add(R.drawable.menu_markoer_02);
                markers.add(R.drawable.menu_markoer_03);
                markers.add(R.drawable.menu_markoer_04);
                markers.add(R.drawable.menu_pil_01);
                markers.add(R.drawable.menu_pil_02);
                markers.add(R.drawable.menu_pil_03);
                markers.add(R.drawable.menu_pil_04);
                markers.add(R.drawable.menu_flag_01);
                markers.add(R.drawable.menu_flag_02);
                markers.add(R.drawable.menu_flag_03);
                markers.add(R.drawable.menu_flag_04);
                markers.add(R.drawable.menu_flag_05);
                markers.add(R.drawable.menu_flag_06);

                Integer images[] = {R.drawable.markoer_01,
                        R.drawable.markoer_02,
                        R.drawable.markoer_03,
                        R.drawable.markoer_04,
                        R.drawable.pil_01,
                        R.drawable.pil_02,
                        R.drawable.pil_03,
                        R.drawable.pil_04,
                        R.drawable.flag_01,
                        R.drawable.flag_02,
                        R.drawable.flag_03,
                        R.drawable.flag_04,
                        R.drawable.flag_05,
                        R.drawable.flag_06};

                List<Integer> colorIcons = new ArrayList<Integer>();
                colorIcons.add(R.drawable.color_white);
                colorIcons.add(R.drawable.color_black);
                colorIcons.add(R.drawable.color_grey);
                colorIcons.add(R.drawable.color_blue);
                colorIcons.add(R.drawable.color_red);
                colorIcons.add(R.drawable.color_green);
                colorIcons.add(R.drawable.color_farve_gul);

                Integer colors[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};

                data.getEditTools().add(new ToolItemData(markers, Arrays.asList(images), "Type","Vælg type"));
                data.getEditTools().add(new ToolItemData(colorIcons, Arrays.asList(colors), "Farve","Vælg farve på markøren"));

                data.setActiveText("Placer en markør");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                break;

            case Icon_Tool://1,1
                Integer images12[] = {R.drawable.elektricitet, R.drawable.frost, R.drawable.han, R.drawable.hun, R.drawable.ild, R.drawable.kompas, R.drawable.kvinde, R.drawable.lup, R.drawable.maane, R.drawable.mand, R.drawable.oeje_side, R.drawable.oeje, R.drawable.paere, R.drawable.saks, R.drawable.sol, R.drawable.termometer, R.drawable.vand, R.drawable.fare_01, R.drawable.fare_02, R.drawable.fare_03, R.drawable.fare_04, R.drawable.fare_05, R.drawable.fare_06, R.drawable.fare_07, R.drawable.fare_08, R.drawable.fare_09};
                Integer imageIcons12[] = {R.drawable.menu_elektricitet, R.drawable.menu_frost, R.drawable.menu_han, R.drawable.menu_hun, R.drawable.menu_ild, R.drawable.menu_kompas, R.drawable.menu_kvinde, R.drawable.menu_lup, R.drawable.menu_maane, R.drawable.menu_mand, R.drawable.menu_oeje_side, R.drawable.menu_oeje, R.drawable.menu_paere, R.drawable.menu_saks, R.drawable.menu_sol, R.drawable.menu_termometer, R.drawable.menu_vand, R.drawable.menu_fare_01, R.drawable.menu_fare_02, R.drawable.menu_fare_03, R.drawable.menu_fare_04, R.drawable.menu_fare_05, R.drawable.menu_fare_06, R.drawable.menu_fare_07, R.drawable.menu_fare_08, R.drawable.menu_fare_09};
                Integer colors12[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons12[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                data.getEditTools().add(new ToolItemData(Arrays.asList(imageIcons12), Arrays.asList(images12), "Type","Vælg type"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons12), Arrays.asList(colors12), "Farve","Vælg farve på ikonet"));

                data.setActiveText("Placer dit ikon ?");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                break;
            case Sign_Tool: //1,2
                Integer images1[] = {R.drawable.skilt_01, R.drawable.skilt_02, R.drawable.skilt_03, R.drawable.skilt_04, R.drawable.skilt_05, R.drawable.skilt_06, R.drawable.skilt_07, R.drawable.skilt_08};
                Integer imageIcons1[] = {R.drawable.menu_skilt_01, R.drawable.menu_skilt_02, R.drawable.menu_skilt_03, R.drawable.menu_skilt_04, R.drawable.menu_skilt_05, R.drawable.menu_skilt_06, R.drawable.menu_skilt_07, R.drawable.menu_skilt_08};
                Integer colors1[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons1[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                data.getEditTools().add(new ToolItemData(Arrays.asList(imageIcons1), Arrays.asList(images1), "Type","Vælg type"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons1), Arrays.asList(colors1), "Farve","Vælg farve på ikonet"));

                data.setActiveText("Skift skilt");
                data.setNonActiveText("Skift farve på skiltet");

                break;

            case Draw_Tool://2,0
                Integer colors20[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons20[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                Integer sizeIcons20[] = {R.drawable.draw_stregtyk_00, R.drawable.draw_stregtyk_01, R.drawable.draw_stregtyk_02, R.drawable.draw_stregtyk_03};
                Integer sizes20[] = {1, 3, 5, 7};

                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons20), Arrays.asList(colors20), "Farve","Vælg farve på stregen"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(sizeIcons20), Arrays.asList(sizes20), "Streg","Vælg tykkelse på stregen"));

                data.setActiveText("Tilpas dit lag");
                data.setBeforeFirstClickText("Tryk der hvor du vil starte din streg");
                data.setAfterFirstClickText("Du kan flytte på punkterne");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                data.setFinishText("Rediger din streg");
                break;
            case Draw_Area://2,1

                 Integer sizeIcons21[] = {R.drawable.draw_stregtyk_00, R.drawable.draw_stregtyk_01, R.drawable.draw_stregtyk_02, R.drawable.draw_stregtyk_03, R.drawable.draw_stregtyk_03};
                Integer sizes21[] = {0, 1, 3, 5, 7};

                Integer fill21[] = {0,1,R.drawable.tern, R.drawable.prikker, R.drawable.skravering};
                Integer filltypeIcons21[] = {R.drawable.menu_tom, R.drawable.menu_farve, R.drawable.menu_tern, R.drawable.menu_prikker, R.drawable.menu_skravering};

                Integer colors21[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons21[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};


                data.getEditTools().add(new ToolItemData(Arrays.asList(sizeIcons21), Arrays.asList(sizes21), "Streg","Vælg tykkelse på stregen"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(filltypeIcons21), Arrays.asList(fill21), "Fyld","Vælg fyld"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons21), Arrays.asList(colors21), "Farve","Vælg farve"));

                data.setActiveText("Tilpas dit lag");
                data.setBeforeFirstClickText("Tryk hvor du vil starte dit omrids");
                data.setAfterFirstClickText("Flyt evt. på punkterne. Afslut i 1. punkt");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                data.setFinishText("Customiser din figur");

                break;
            case Shape_Tool://2,2
                Integer imageIcons22[] = {R.drawable.menu_cirkel, R.drawable.menu_firkant ,R.drawable.menu_trekant, R.drawable.menu_rombe, R.drawable.menu_trapez, R.drawable.menu_superellipse};
                Integer images22[] = {  R.drawable.circle,R.drawable.firkant, R.drawable.trekant,R.drawable.rombe, R.drawable.trapez, R.drawable.superellipse};

                Integer colorIcons22[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};
                Integer colors22[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};

                data.getEditTools().add(new ToolItemData(Arrays.asList(imageIcons22), Arrays.asList(images22), "Type","Vælg type"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons22), Arrays.asList(colors22), "Farve","Vælg farve på markøren"));

                data.setActiveText("Placer en figur");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                break;
            case Brackets_Tool://3,0
                Integer colors30[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons30[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                Integer typeIcons30[] = {R.drawable.menu_tuborg, R.drawable.menu_lines_centre, R.drawable.menu_triangle, R.drawable.markering_08, R.drawable.menu_square};
                Integer type30[] = {0,R.drawable.line, R.drawable.triangle, R.drawable.line_pil2, R.drawable.square};

                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons30), Arrays.asList(colors30), "Farve","Vælg farve på stregen"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(typeIcons30), Arrays.asList(type30), "Type","Vælg type"));

                data.setBeforeFirstClickText("Tryk hvor du vil starte linjen");
                data.setAfterFirstClickText("Tryk hvor du vil slutte linjen");
                data.setAfterSecondClickText("Du kan flytte på punkterne");
                data.setActiveText("Tryk på skærmen, der hvor du vil starte dit element");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                data.setFinishText("Customiser din streg");
                break;
            case Count_Tool://3,1
                Integer colors31[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons31[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                Integer sizeIcons31[] = {R.drawable.str1, R.drawable.str2, R.drawable.str3, R.drawable.str4, R.drawable.str5, R.drawable.str6, R.drawable.str7};
                Integer sizes31[] = {12, 18, 24, 30, 36, 42, 52};

                Integer typeIcons31[] = {R.drawable.menu_tal, R.drawable.menu_bogstav, R.drawable.menu_romer};
                Integer types31[] = {0, 1, 2};

                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons31), Arrays.asList(colors31), "Farve","Vælg farve"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(sizeIcons31), Arrays.asList(sizes31), "Størrelse","Vælg størrelse"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(typeIcons31), Arrays.asList(types31), "Type","Vælg type"));

                data.setBeforeFirstClickText("Tryk på de ting, du vil tælle");
                data.setAfterFirstClickText("Tryk på næste ting");
                data.setActiveText("Tryk på de ting, du vil tælle, for at sætte numre");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                data.setFinishText("Juster tallene som du ønsker");
                break;
            case Angle_Tool://3,2
                Integer colors32[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons32[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                Integer sizeIcons32[] = {R.drawable.str1, R.drawable.str2, R.drawable.str3, R.drawable.str4, R.drawable.str5, R.drawable.str6, R.drawable.str7};
                Integer sizes32[] = {12, 18, 24, 30, 36, 42, 52};

                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons32), Arrays.asList(colors32), "Farve","Skift farve på teksten"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(sizeIcons32), Arrays.asList(sizes32), "Størrelse","Skift udseende af teksten (Virker ikke endnu)"));

                data.setActiveText("Tilpas dit lag");
                data.setBeforeFirstClickText("Flyt punkterne for at justere vinklen");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                data.setFinishText("Customiser din figur");
                break;

            case Text_Area_Tool://4,0
                Integer colors40[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons40[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                Integer sizeIcons40[] = {R.drawable.str1, R.drawable.str2, R.drawable.str3, R.drawable.str4, R.drawable.str5, R.drawable.str6, R.drawable.str7};
                Integer sizes40[] = {12, 18, 24, 30, 36, 42, 60};

                data.getEditTools().add(new ToolItemData(Arrays.asList(sizeIcons40), Arrays.asList(sizes40), "Størrelse","Vælg størrelse på teksten"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons40), Arrays.asList(colors40), "Farve","Vælg farve på teksten"));

                data.setActiveText("Beskriv med tekst hvad der er interessant ved det valgte punkt");
                data.setNonActiveText("Skriv hvad du vil");
                data.setFinishText("Hold fingeren inde for at åbne menuen");
                break;
            case Symbol_Tool://4,1
                Integer images41[] = {R.drawable.alpha, R.drawable.beta, R.drawable.delta, R.drawable.delta_stor, R.drawable.epsilon, R.drawable.gamma, R.drawable.lambda, R.drawable.my, R.drawable.ohm_stor, R.drawable.phi_stor, R.drawable.pi, R.drawable.psi, R.drawable.rho, R.drawable.sigma_stor, R.drawable.sigma, R.drawable.tau};
                Integer imageIcons41[] = {R.drawable.menu_alpha, R.drawable.menu_beta, R.drawable.menu_delta, R.drawable.menu_delta_stor, R.drawable.menu_epsilon, R.drawable.menu_gamma, R.drawable.menu_lambda, R.drawable.menu_my, R.drawable.menu_ohm_stor, R.drawable.menu_phi_stor, R.drawable.menu_pi, R.drawable.menu_rho, R.drawable.menu_sigma_stor, R.drawable.menu_sigma, R.drawable.menu_tau};
                Integer colors41[] = {R.color.menuWhite, R.color.menuBlack, R.color.menuGray, R.color.menuBlue, R.color.menuRed, R.color.menuGreen, R.color.farve_gul};
                Integer colorIcons41[] = {R.drawable.color_white, R.drawable.color_black, R.drawable.color_grey, R.drawable.color_blue, R.drawable.color_red, R.drawable.color_green, R.drawable.color_farve_gul};

                data.getEditTools().add(new ToolItemData(Arrays.asList(imageIcons41), Arrays.asList(images41), "Type","Vælg type"));
                data.getEditTools().add(new ToolItemData(Arrays.asList(colorIcons41), Arrays.asList(colors41), "Farve","Vælg farve på symbolet"));

                data.setActiveText("Placer dit symbol");
                data.setNonActiveText("Hold fingeren inden for at åbne menuen");
                break;
        }
        return data;
    }
}
