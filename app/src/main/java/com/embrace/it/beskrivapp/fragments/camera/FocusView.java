package com.embrace.it.beskrivapp.fragments.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;

/**
 * Created by waseem on 4/4/17.
 */

public class FocusView extends SurfaceView {

    private Paint paint;
    private SurfaceHolder mHolder;
    private Context context;
    Bitmap bitmapFocus;

    public FocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FocusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public FocusView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public FocusView(CameraFragment context) {
        super(context.getActivity().getBaseContext());
        init(context.getActivity().getBaseContext());

    }

    void init(Context context) {
        mHolder = getHolder();
        mHolder.setFormat(PixelFormat.TRANSPARENT);
        this.context = context;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);

//        final Canvas canvas = mHolder.lockCanvas();
//        if (canvas != null) {
//            canvas.drawColor(Color.TRANSPARENT);
//            mHolder.unlockCanvasAndPost(canvas);
//        }
       bitmapFocus= ImageHelper.drawableToBitmap(ImageHelper.getdrawable(R.drawable.focus));
        bitmapFocus = ImageHelper.resizeBitmap(bitmapFocus,100,100);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        if (canvas != null) {
//            canvas.drawColor(Color.TRANSPARENT);
//        }
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            invalidate();
//            if (mHolder.getSurface().isValid()) {
//                final Canvas canvas = mHolder.lockCanvas();
//                Log.d("touch", "touchRecieved by camera");
//                if (canvas != null) {
//                    Log.d("touch", "touchRecieved CANVAS STILL Not Null");
//                    canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//                    canvas.drawColor(Color.TRANSPARENT);
//                    canvas.drawBitmap(bitmapFocus,event.getX()-bitmapFocus.getWidth()/2, event.getY()-bitmapFocus.getHeight()/2,new Paint(Paint.ANTI_ALIAS_FLAG));
////                    canvas.drawCircle(event.getX(), event.getY(), 100, paint);
//                    mHolder.unlockCanvasAndPost(canvas);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            Canvas canvas1 = mHolder.lockCanvas();
//                            if (canvas1 != null) {
//                                canvas1.drawColor(0, PorterDuff.Mode.CLEAR);
//                                mHolder.unlockCanvasAndPost(canvas1);
//                            }
//
//                        }
//                    }, 1000);
//
//                }
////                mHolder.unlockCanvasAndPost(canvas);
//
//
//            }
//        }
//
//
//        return false;
//    }


    public void drawFocus(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            invalidate();
            if (mHolder.getSurface().isValid()) {
                final Canvas canvas = mHolder.lockCanvas();
                Log.d("touch", "touchRecieved by camera");
                if (canvas != null) {
                    Log.d("touch", "touchRecieved CANVAS STILL Not Null");
                    canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    canvas.drawColor(Color.TRANSPARENT);
                    canvas.drawBitmap(bitmapFocus,event.getX()-bitmapFocus.getWidth()/2, event.getY()-bitmapFocus.getHeight()/2,new Paint(Paint.ANTI_ALIAS_FLAG));
//                    canvas.drawCircle(event.getX(), event.getY(), 100, paint);
                    mHolder.unlockCanvasAndPost(canvas);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Canvas canvas1 = mHolder.lockCanvas();
                            if (canvas1 != null) {
                                canvas1.drawColor(0, PorterDuff.Mode.CLEAR);
                                mHolder.unlockCanvasAndPost(canvas1);
                            }

                        }
                    }, 1000);

                }
            }
        }
    }
}