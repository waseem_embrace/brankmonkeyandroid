package com.embrace.it.beskrivapp.activity;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.PdfCreator;
import com.embrace.it.beskrivapp.commons.helpers.MyGoogleLocation;
import com.embrace.it.beskrivapp.commons.helpers.MyLog;
import com.embrace.it.beskrivapp.fragments.CreatProjectFragment;
import com.embrace.it.beskrivapp.fragments.InfoFragment;
import com.embrace.it.beskrivapp.fragments.ProjectsFragment;
import com.embrace.it.beskrivapp.fragments.StartFragment;
import com.embrace.it.beskrivapp.fragments.camera.CameraFragment;

/**
 * Created by waseem on 1/31/17.
 */

public class StartActivity extends FragmentActivity implements StartFragment.SplashFragmentCallback, InfoFragment.InfoFragmentCallback, CreatProjectFragment.CreateProjectCallback, ActivityCompat.OnRequestPermissionsResultCallback {

    CameraFragment cameraFragment;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 321;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_STORAGE = 110;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Hide Action Bar
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);


        setContentView(R.layout.activity_start);
        if (savedInstanceState == null) {
/*            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            cameraFragment = CameraFragment.newInstance(true);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.activity_splash_camera, cameraFragment);
                            transaction.addToBackStack(Camera_Fragment).commitAllowingStateLoss();

                            *//*cameraFragment = CameraFragment.newInstance(false);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.activity_splash, cameraFragment);
                            transaction.addToBackStack(Camera_Fragment).commitAllowingStateLoss();*//*
                        }
                    });
                }
            }, 1000);*/

            showStartScreen();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_ACCESS_STORAGE);
        } else {
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
        } else {
            MyGoogleLocation m = MyGoogleLocation.sharedInstance();
            m.initLocationService();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MyGoogleLocation m = MyGoogleLocation.sharedInstance();
                    m.initLocationService();
                } else {
                    // permission denied, boo! Disable the
                }
                return;
            }
        }
    }


    @Override
    public void showInfoScreen() {

        InfoFragment f = new InfoFragment();
        f.setDelegate(this);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.fade_in, R.animator.fade_out, R.animator.fade_in, R.animator.fade_out);
        transaction.replace(R.id.activity_splash, f);
        transaction.addToBackStack(Info_Fragment).commit();
    }


    public void showStartScreen() {
        StartFragment f = new StartFragment();
        f.setDelegate(this);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_splash, f);
        transaction.addToBackStack(Start_Fragment).commit();
    }


    @Override
    public void createNewProject() {
        EditProject(null);
    }

    public void EditProject(String projectId) {
        CreatProjectFragment f = new CreatProjectFragment();
        f.setDelegate(this);
        if (projectId != null) {
            Bundle bundle = new Bundle();
            bundle.putString("projectId", projectId);
            f.setArguments(bundle);
        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.enter_from_bottom, R.animator.fade_out, R.animator.fade_in, R.animator.exit_to_bottom);
        transaction.replace(R.id.activity_splash, f);
        transaction.addToBackStack(NewProject_Fragment).commit();
    }

    @Override
    public void showProjects() {
        ProjectsFragment f = new ProjectsFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.enter_from_top, R.animator.exit_to_bottom, R.animator.enter_from_bottom, R.animator.exit_to_top);
        transaction.replace(R.id.activity_splash, f);
        transaction.addToBackStack(Proejcts_Fragment).commit();
    }

    @Override
    public void startDrawing(String projectId) {
        Intent intent = new Intent(StartActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (projectId != null) {
            intent.putExtra("projectId", projectId);
        }
        startActivity(intent);
        this.finish();
        finish();
    }

    private static String Proejcts_Fragment = "Projects_Fragment";
    private static String Start_Fragment = "Start_Fragment";
    private static String NewProject_Fragment = "NewProject_Fragment";
    private static String Info_Fragment = "Info_Fragment";
    private static String Camera_Fragment = "Camera_Fragment";

    @Override
    public void goBack() {
//        showStartScreen();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";

        @Override
        public boolean onDown(MotionEvent event) {
            MyLog.d(DEBUG_TAG, "onDown: " + event.toString());
            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            MyLog.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            super.onScroll(e1, e2, distanceX, distanceY);

            if (distanceX > 20) {

            }
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1);
            String tag = backEntry.getName();
            if (tag.equals(Start_Fragment) || tag.equals(Camera_Fragment)) {
                finish();
            } else {
                getFragmentManager().popBackStack();

            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        try {
            fragmentTransaction.remove(cameraFragment);
            fragmentTransaction.commit();
        } catch (Exception e) {
        }
        cameraFragment = null;

        super.onDestroy();
    }

}
