package com.embrace.it.beskrivapp.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.adapters.DetailCellViewHolder;
import com.embrace.it.beskrivapp.adapters.JobListAdapter;
import com.embrace.it.beskrivapp.commons.observables.ThumbnailSavedObservable;
import com.embrace.it.beskrivapp.database.DBController;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by waseem on 1/31/17.
 */

public class ProjectsFragment extends Fragment implements JobListAdapter.joblistAdapterCallback {


    RecyclerView recycleview;
    JobListAdapter adapter;
    LinearLayoutManager linearlayoutmanager;

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_projects, container, false);
        recycleview = (RecyclerView) view.findViewById(R.id.joblist_recyclerview);
        adapter = new JobListAdapter(getActivity(), DBController.getProjects(), recycleview);
        adapter.delegate = this;
        linearlayoutmanager = new LinearLayoutManager(getActivity());
        recycleview.setLayoutManager(linearlayoutmanager);
        recycleview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void closeAnimation(final int position) {
        if (position > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DetailCellViewHolder holder = (DetailCellViewHolder) recycleview.findViewHolderForAdapterPosition(position);
                            if(holder != null)
                            holder.closeWithAnimation();
                        }
                    });
                }
            },200);
        }
    }

    JobListAdapter getAdapter(){
        return adapter;
    }
    @Override
    public void closeCell(int position) {
        closeAnimation(position);
    }

    @Override
    public void showDeleteAlert(int position) {
        DeleteDialog dialog = new DeleteDialog();
        dialog.position = position;
        dialog.setTargetFragment(this,00321);
        dialog.show(getChildFragmentManager(),"DeleteDialog");

    }


    public static class DeleteDialog extends DialogFragment implements View.OnClickListener {

        private ProjectsFragment parent;
        TextView tvTitle;
        String title = "";
        Button btnNo;
        Button btnYes;
        public int position;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v = inflater.inflate(
                    R.layout.dialoge_fragment_alert, container, false);
            tvTitle = (TextView) v.findViewById(R.id.tv_dialog_title);
            btnNo = (Button) v.findViewById(R.id.bt_dialog_second);
            btnYes = (Button) v.findViewById(R.id.bt_dialog_first);
            btnNo.setOnClickListener(this);
            btnYes.setOnClickListener(this);
            tvTitle.setText(getString(R.string.sure_you_want_to_delete));
            btnYes.setText(R.string.delete);
            btnNo.setText(R.string.cancel);
            return v;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog d = super.onCreateDialog(savedInstanceState);
            parent = (ProjectsFragment) getParentFragment();
            d.setCancelable(false);
            return d;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationTopToDown;
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        }

        @Override
        public void onClick(View v) {
            if (v.equals(btnNo)) {
                this.dismiss();
            } else if (v.equals(btnYes)) {
                this.dismiss();
                parent.getAdapter().deleteProject(position);
            }
        }
    }


}
