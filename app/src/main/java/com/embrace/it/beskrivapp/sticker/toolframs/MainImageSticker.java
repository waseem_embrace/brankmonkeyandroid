package com.embrace.it.beskrivapp.sticker.toolframs;

import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.DrawableSticker;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

/**
 * @author wupanjie
 */
public class MainImageSticker extends DrawableSticker {

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public float getContrast() {
        return contrast;
    }

    public void setContrast(float contrast) {
        this.contrast = contrast;
    }

    private int brightness;
    private float contrast;
    public MainImageSticker(Drawable drawable, ImageView.ScaleType scaletype) {
        super(drawable, scaletype);
        setUserIntractions(false, false, false);
        brightness = 0; contrast = 5;
    }

    public MainImageSticker(Drawable drawable) {
        super(drawable, null);
        setUserIntractions(false, false, false);
        brightness = 0; contrast = 5;
    }

    private void getMyDrawable() {

    }

    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);
        projectItem.setItemType(toolType.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(this.brightness));
        projectItem.getCharacteristics().add(new RealmFloat(this.contrast));

        //  getFrame();
        return projectItem;
    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }

    @Override
    public void setProjectItem(ProjectItem p) {

        if(p == null)
            return;
        toolType = Utils.getToolType(p.getItemType());

        if(p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
            setFrame(fram);
        }

        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);

        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        if(characterstics.length == 2) {
            this.brightness = (int)characterstics[0];
            this.contrast = characterstics[1];
        }
    }

   public void brightnessContrast(int b,float contrast){
        this.brightness = b;
        this.contrast = contrast;
    }



    static public ProjectItem getDefaultProjectItem() {
        ProjectItem projectItem  = new ProjectItem();
        float[] values = new float[]{1,0,1,0,1,0,0,0,1};
        projectItem.setMatrixValues(values);
        projectItem.setItemType(Constants.ToolType.Main_Image.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(0));
        projectItem.getCharacteristics().add(new RealmFloat(5));

        //  getFrame();
        return projectItem;
    }
}
