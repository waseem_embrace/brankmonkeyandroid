package com.embrace.it.beskrivapp.sticker.toolframs;

import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.embrace.it.beskrivapp.MainApplication;
import com.embrace.it.beskrivapp.commons.helpers.ImageHelper;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.database.ProjectItem;
import com.embrace.it.beskrivapp.database.RealmFloat;
import com.embrace.it.beskrivapp.database.RealmHelper;
import com.embrace.it.beskrivapp.sticker.DrawableSticker;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolItemData;

/**
 * @author wupanjie
 */
public class ShapeSticker extends DrawableSticker {

    private int imageIndex;
    private int colorIndex;

    public ShapeSticker(Drawable drawable) {
        super(drawable);
        imageIndex = 0;colorIndex = 0;
        setUserIntractions(true,true,true);
//        itemUpdated(imageIndex,colorIndex);
    }

    @Override
    public String getInstructions(int mainIndex, int childIndex) {
        ToolItemData dataItem = data.getEditTools().get(mainIndex);
        return dataItem.getDescription();
    }

    @Override
    public void itemUpdated(int mainIndex, int childIndex) {
        if( mainIndex < 0 && childIndex < 0) {
            mainIndex = 0;
            childIndex = 0;
        }
        ToolData data = getData();
        switch (mainIndex) {
            case 0:
                this.imageIndex = childIndex;
                ToolItemData icons = data.getEditTools().get(0);
                Drawable drawable = ImageHelper.getdrawable(icons.getToolItemData().get(childIndex));
                drawable.setTint(getMyColor());
                setDrawable(drawable);

                break;
            case 1:
                this.colorIndex = childIndex;
                Drawable drawable1 = getDrawable();
                drawable1.setTint(getMyColor());
                setDrawable(drawable1);
                break;
        }
    }

    private void getMyDrawable() {

    }

    private int getMyColor() {
        ToolItemData icons = data.getEditTools().get(1);
        return ContextCompat.getColor(MainApplication.getContext(), icons.getToolItemData().get(colorIndex));
    }


    @Override
    public ProjectItem getProjectItem() {
        ProjectItem projectItem = null;

        if (projectItem == null)
            projectItem = new ProjectItem();
        float[] values = new float[9];
        getMatrix().getValues(values);
        projectItem.setMatrixValues(values);
        projectItem.setItemType(toolType.getNumericValue());

        projectItem.getCharacteristics().add(new RealmFloat(this.imageIndex));
        projectItem.getCharacteristics().add(new RealmFloat(this.colorIndex));

        if(getFrame() != null) {
            projectItem.setFrame(getFrame());
        }

        return projectItem;
    }

    @Override
    public void setProjectItem(ProjectItem p) {

        toolType = Utils.getToolType(p.getItemType());

        float[] characterstics = RealmHelper.getArrayF(p.getCharacteristics());
        this.imageIndex = (int)characterstics[0];
        this.colorIndex = (int)characterstics[1];

        if(p.getFrame() != null) {
            RectF fram = RealmHelper.getFrameF(p.getFrame());
            setFrame(fram);
        }

        float[] values = new float[9];
        values = RealmHelper.getArrayF(p.getMatrixValues());
        getMatrix().setValues(values);
        itemUpdated(0,this.imageIndex);

    }
}
