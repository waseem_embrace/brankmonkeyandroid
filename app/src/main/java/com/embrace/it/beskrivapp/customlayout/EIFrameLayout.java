package com.embrace.it.beskrivapp.customlayout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class EIFrameLayout extends FrameLayout {

    public EIFrameLayout(Context context) {
        super(context);
    }

    public EIFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EIFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setYFraction(final float fraction) {
        float translationY = getHeight() * fraction;
        setTranslationY(translationY);
    }

    public float getYFraction() {
        if (getHeight() == 0) {
            return 0;
        }
        return getTranslationY() / getHeight();
    }

    public void setXFraction(final float fraction) {
        float translationX = getWidth() * fraction;
        setTranslationX(translationX);
    }

    public float getXFraction() {
        if (getWidth() == 0) {
            return 0;
        }
        return getTranslationX() / getWidth();
    }
}