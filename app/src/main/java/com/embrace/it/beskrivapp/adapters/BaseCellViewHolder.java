package com.embrace.it.beskrivapp.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.Utils;

/**
 * Created by waseem on 2/1/17.
 */

public class BaseCellViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    ImageButton btnDelete;
    ImageButton btnSettings;
    TextView tvTitle;
    View foregroundView;
    View backgroundView;


    public BaseCellViewHolder(View itemView) {
        super(itemView);
        btnDelete = (ImageButton)itemView.findViewById(R.id.btn_cell_delete);
        btnSettings = (ImageButton)itemView.findViewById(R.id.btn_cell_settings);
        tvTitle = (TextView)itemView.findViewById(R.id.tv_cell_title);
        foregroundView = itemView.findViewById(R.id.cell_foreground_layout);
        backgroundView = itemView.findViewById(R.id.bottom_wrapper);
        btnDelete.setOnClickListener(this);
        btnSettings.setOnClickListener(this);
        itemView.setOnClickListener(this);
        itemView.setEnabled(true);
    }

    public void setValues(Context context){
        Typeface fontBold = Utils.getFontTypeface(context, Constants.FONT_FrutigerLTStd_Black);
        Typeface fontLight = Utils.getFontTypeface(context, Constants.FONT_FrutigerLTStd_Black);
        tvTitle.setTypeface(fontBold);


    }
    @Override
    public void onClick(View v) {
        if(v.equals(btnDelete)){

        }
        else if(v.equals(btnSettings)){

        }else{

        }
    }



    /*
    1.animate foreground to a specific x
    2.animate backgorund to specific point
    3.close all others with animation

    */
    void animateToOpen(){
        ObjectAnimator animX = ObjectAnimator.ofFloat(foregroundView,"x",0,-backgroundView.getWidth());
       animX.setDuration(500).start();
    }

    void animateToClose(){
        ObjectAnimator animX = ObjectAnimator.ofFloat(foregroundView,"x",foregroundView.getX(),0);
        animX.setDuration(200).start();
    }
}
