package com.embrace.it.beskrivapp.database;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by waseem on 2/9/17.
 */

public class Project extends RealmObject {
    public Project(){
        this.subProjects = new RealmList<SubProject>();
    }
    public AlbumInfo getAlbum() {
        return album;
    }

    public void setAlbum(AlbumInfo album) {
        this.album = album;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Boolean getOpened() {
        return isOpened;
    }

    public void setOpened(Boolean opened) {
        isOpened = opened;
    }


    public RealmList<SubProject> getSubProjects() {
        return subProjects;
    }

    public void setSubProjects(RealmList<SubProject> subProjects) {
        this.subProjects = subProjects;
    }

    @PrimaryKey
    private String projectId;
    private String projectTitle;
    private AlbumInfo album;
    private RealmList<SubProject> subProjects;

    @Ignore
    public Boolean isOpened = false;


}
