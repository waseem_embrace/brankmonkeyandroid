package com.embrace.it.beskrivapp.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.commons.Constants;
import com.embrace.it.beskrivapp.commons.SimpleTouchListener;
import com.embrace.it.beskrivapp.commons.Utils;
import com.embrace.it.beskrivapp.sticker.DrawingView;
import com.embrace.it.beskrivapp.toolData.ToolData;
import com.embrace.it.beskrivapp.toolData.ToolDataFactory;

import java.util.List;


public class DrawingFragment extends Fragment implements View.OnClickListener,DrawingView.DrawingViewCallback {

    DrawingView dv;
    private Paint mPaint;

//    public interface InfoFragmentCallback {
//        public void goBack();
//    }

//    public void setDelegate(InfoFragmentCallback delegate) {
//        this.delegate = delegate;
//    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    SimpleTouchListener touchListener;
    Button btnOk;
    Constants.ToolType tooltype;
ToolData toolData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_drawing, container, false);
        dv = (DrawingView) view.findViewById(R.id.drawing);
        dv.setDelegate(this);
        btnOk = (Button) view.findViewById(R.id.btn_drawing_ok);
        btnOk.setOnClickListener(this);
        dv.setTooltype(tooltype);
        btnOk.setText("Færdig");
//        if(tooltype == Constants.ToolType.Brackets_Tool){
//            btnOk.setText("Afslut");
//        }else
            if(tooltype == Constants.ToolType.Draw_Area){
            btnOk.setVisibility(View.INVISIBLE);
        }

        String textTitle = toolData.getBeforeFirstClickText();
        if(textTitle != null && !textTitle.equals("")) {
            ((MainActivity) container.getContext()).updateTitle(toolData.getBeforeFirstClickText());
        }
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        tooltype = (Constants.ToolType) b.get("tooltype");
        toolData = ToolDataFactory.getToolData(tooltype);


    }

    void setFontsAndSizes(Context context) {
        Typeface blackCn = Utils.getFontTypeface(context, Constants.FONT_FrutigerLTStd_BlackCn);
        Typeface roman = Utils.getFontTypeface(context, Constants.FONT_FrutigerLTStd_Roman);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
//        String path = dv.saveAndImagePath();
        ((MainActivity) v.getContext()).addToolForDrawing(dv.getDrawingPoints(),tooltype);
        ((MainActivity) v.getContext()).updateTitle(toolData.getToolItemIntruction(0));
    }

    @Override
    public void doneDrawing(View v) {
        ((MainActivity) v.getContext()).addToolForDrawing(dv.getDrawingPoints(),tooltype);
        ((MainActivity) v.getContext()).updateTitle(toolData.getToolItemIntruction(0));
    }

    @Override
    public void drawingPointsUpdated(View v,List<PointF> drawingPoints) {
        if (drawingPoints != null && drawingPoints.size() >= 1){
            String textTitle = toolData.getAfterFirstClickText();
            if(textTitle != null && !textTitle.equals("")) {
                ((MainActivity) v.getContext()).updateTitle(textTitle);
            }
            textTitle = toolData.getAfterSecondClickText();
            if(drawingPoints.size() >= 2 && textTitle != null && !textTitle.equals("")){
                ((MainActivity) v.getContext()).updateTitle(textTitle);
            }
        }
    }
}
