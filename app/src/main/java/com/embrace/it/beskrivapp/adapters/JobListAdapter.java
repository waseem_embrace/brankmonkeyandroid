package com.embrace.it.beskrivapp.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.embrace.it.beskrivapp.R;
import com.embrace.it.beskrivapp.activity.MainActivity;
import com.embrace.it.beskrivapp.activity.StartActivity;
import com.embrace.it.beskrivapp.commons.observables.EventBus;
import com.embrace.it.beskrivapp.commons.observables.ThumbnailSavedObservable;
import com.embrace.it.beskrivapp.database.DBController;
import com.embrace.it.beskrivapp.database.Project;
import com.embrace.it.beskrivapp.database.SubProject;
import com.embrace.it.beskrivapp.fragments.SubProjectsFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by waseem on 2/1/17.
 */

public class JobListAdapter extends RecyclerView.Adapter implements DetailCellViewHolder.DetailViewholderCallback, Observer {

    List<Project> dataArray = new ArrayList<Project>();
    private LayoutInflater inflater;
    Context context;
    RecyclerView recyclerview;

    private static final int CELL_TYPE_NEW_PROJECT = 0;
    private static final int CELL_TYPE_PROJECT_SIMPLE = 1;
    private static final int CELL_TYPE_PROJECT_DETAIL = 2;

    public JobListAdapter(Context c, List<Project> projects, RecyclerView recyclerview) {
        this.context = c;
        this.dataArray = projects;
        this.recyclerview = recyclerview;
        inflater = LayoutInflater.from(context);
        EventBus.getThumbnailSavedObserver().addObserver(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder = null;
        View view;
        switch (viewType) {
            case CELL_TYPE_NEW_PROJECT:
                view = inflater.inflate(R.layout.cell_project_new, parent, false);
                viewHolder = new NewProjectCellViewHolder(view);
                break;
            case CELL_TYPE_PROJECT_DETAIL:
                view = inflater.inflate(R.layout.cell_project_detail, parent, false);
                DetailCellViewHolder detailViewHolder = new DetailCellViewHolder(view);
                detailViewHolder.setDelegate(this);
                viewHolder = detailViewHolder;
                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Project p = null;
        switch (getItemViewType(position)) {
            case CELL_TYPE_NEW_PROJECT:
                NewProjectCellViewHolder newProjectViewHolder = (NewProjectCellViewHolder) holder;
                newProjectViewHolder.setValues(context);
                break;
            case CELL_TYPE_PROJECT_DETAIL:
                p = dataArray.get(position - 1);
                DetailCellViewHolder detailViewHolder = (DetailCellViewHolder) holder;
                detailViewHolder.setValues(context, p);

                break;
            default:
                break;
        }
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public int getItemCount() {
        if (dataArray != null)
            return dataArray.size() + 1;
        else
            return 1;
    }


    @Override
    public int getItemViewType(int position) {
        int pos = super.getItemViewType(position);
        if (position == 0)
            pos = CELL_TYPE_NEW_PROJECT;
        else {
//            Project p = dataArray.get(position -1);
//            if(p.isOpened != null && p.isOpened)
//            pos = CELL_TYPE_PROJECT_DETAIL;
//            else
            pos = CELL_TYPE_PROJECT_DETAIL;
        }
        return pos;
    }

    DetailCellViewHolder prevExpandedCell;
    private int prevExpendedCellIndex = -1;


/*
   @Override
    public void cellIsOpened(DetailCellViewHolder viewholder, int position) {
        //close all cells
        try {
            if (prevExpandedCell != null && !prevExpandedCell.equals(viewholder)) {
//                for (int i = 1; i <= dataArray.size(); i++) {
//                    if (i != position) {
//                        Project p = dataArray.get(i - 1);
//                        p.isOpened = false;
//                    }
//                }
                prevExpandedCell.closeWithAnimation();
                prevExpandedCell = viewholder;
            } else {
                prevExpandedCell = viewholder;
            }
        } catch (Exception e) {
        }
    }
*/


    @Override
    public void cellIsOpened(DetailCellViewHolder viewholder, int position) {
        //close all cells
        try {
            for (int i = 1; i <= dataArray.size(); i++) {
                Project p = dataArray.get(i - 1);
                if (i == position) {
//                   p.isOpened = p.isOpened?false:true;
                } else if (i == prevExpendedCellIndex) {

                } else {
                    p.isOpened = false;
                }
            }
        } catch (Exception e) {
        }

        if (prevExpendedCellIndex != position) {
            if (dataArray != null && prevExpendedCellIndex >= 0 && prevExpendedCellIndex <= dataArray.size()) {
                notifyItemChanged(prevExpendedCellIndex);
            }
            prevExpendedCellIndex = position;
        }
    }

    @Override
    public void startDrawing(DetailCellViewHolder viewholder, int position, Project project) {
        Project p = dataArray.get(position - 1);
        if (context instanceof StartActivity) {
            ((StartActivity) context).startDrawing(p.getProjectId());
        } else if (context instanceof MainActivity) {
            ((MainActivity) context).startDrawing(p.getProjectId());
        }
    }

    @Override
    public void deleteProject(DetailCellViewHolder viewholder, int position) {
        if (delegate != null)
            delegate.showDeleteAlert(position);
    }

    public void deleteProject(int position) {
        Project p = dataArray.get(position - 1);
        try {
            DBController.getInstance().deleteProject(p.getProjectId());
            dataArray = DBController.getProjects();
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    public interface joblistAdapterCallback {
        void closeCell(int position);

        void showDeleteAlert(int position);
    }

    public joblistAdapterCallback delegate;

    @Override
    public void closeCell(int position) {
        delegate.closeCell(position);
    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ThumbnailSavedObservable) {
            try {
                Bundle bundle = (Bundle) arg;
                String projectId = bundle.getString("projectId");
                int index = getIndexOfProject(projectId);
                if (index >= 0) {
                    notifyItemChanged(index);
                }
            } catch (Exception e) {
            }
        }
    }


    private int getIndexOfProject(String projectId) {
        int index = 1;
        for (Project sub : dataArray) {
            if (sub.getProjectId().equals(projectId)) {
                return index;
            }
            index++;
        }
        return -1;
    }
}
