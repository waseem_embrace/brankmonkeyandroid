package com.embrace.it.beskrivapp.database;

import android.graphics.PointF;

import io.realm.RealmObject;

/**
 * Created by waseem on 3/2/17.
 */

public class RealmPointF extends RealmObject {
    private float x;
    private float y;
    public RealmPointF(){
    }

    public RealmPointF(PointF value) {
        this.x = value.x;
        this.y = value.y;
    }

    public PointF getValue() {
        return new PointF(this.x,this.y);
    }

    public void setValue(PointF value) {
        this.x = value.x;
        this.y = value.y;
    }

    public PointF toPointF(){
        return new PointF(this.x,this.y);
    }
}